FROM node:10 as builder

LABEL maintainer="olivier.dusabimana@vecna.com"

RUN mkdir /usr/src/ideliver-ui
WORKDIR /usr/src/ideliver-ui

COPY . .

ENV PATH /usr/src/ideliver-ui/node_modules/.bin:$PATH

ADD package.json /usr/src/ideliver-ui/package.json
# ADD package-lock.json /usr/src/ideliver-ui/package-lock.json
RUN npm install
RUN npm run build:prod

FROM nginx:stable-alpine

RUN rm -rf /usr/share/nginx/html/*

COPY docker/nginx/default.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /usr/src/ideliver-ui/dist /usr/share/nginx/html

EXPOSE 80

# Initialize environment variables into filesystem
WORKDIR /usr/share/nginx/html
COPY ./env.sh .
COPY --from=builder /usr/src/ideliver-ui/.env .

# Add bash
RUN apk add --no-cache bash

# Run script which initializes env vars to fs
RUN chmod +x env.sh
# RUN ./env.sh

# Start Nginx server
CMD ["/bin/bash", "-c", "/usr/share/nginx/html/env.sh && nginx -g \"daemon off;\""]
