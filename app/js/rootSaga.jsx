import 'regenerator-runtime/runtime';
import { takeLatest, all, takeEvery } from 'redux-saga/effects';
import { FETCH_VISITS_ACTION } from './features/visits/actions/fetchVisitsAction';
import { REGISTER_CLIENT_ACTION } from './features/registration/actions/registerClientAction';
import { START_VISIT_ACTION } from './features/registration/actions/startVisitAction';
import { FETCH_FORM_ACTION, SUBMIT_FORM_ACTION } from './state/ui/form/actions';
import { FETCH_FORM_RESOURCE_ACTION } from './state/ui/formResource/actions';
import fetchVisits from './features/visits/sagas/fetchVisitsSaga';
import registerClient from './features/registration/sagas/registerClientSaga';
import startVisit from './features/registration/sagas/startVisitSaga';
import { fetchForm, submitForm } from './state/ui/form/sagas';
import { fetchFormResource } from './state/ui/formResource/sagas';
import { saveObservation } from './state/ui/obs/sagas';
import { SAVE_OBSERVATION_ACTION } from './state/ui/obs/actions';
import { SAVE_ENCOUNTER_ACTION } from './state/ui/encounter/actions';
import { saveEncounter } from './state/ui/encounter/sagas';
import fetchSession from './features/main/sagas/fetchSessionSaga';
import { FETCH_SESSION_ACTION } from './features/main/actions/fetchSessionAction';

import { FETCH_EPISODES_ACTION } from './state/ui/episode/actions';
import { fetchEpisodes } from './state/ui/episode/sagas';
import {
  SAVE_PATIENT_ATTRIBUTE_ACTION,
  SAVE_PATIENT_IDENTIFIER_ACTION,
  SAVE_PATIENT_IMAGE_ACTION,
} from './state/ui/patient/actions';
import {
  savePatientAttribute,
  savePatientIdentifier,
  savePatientImage,
} from './state/ui/patient/sagas';
import {
  SAVE_VISIT_ACTION,
  SAVE_VISIT_ATTRIBUTE_ACTION,
} from './state/ui/visit/actions';
import { saveVisit, saveVisitAttribute } from './state/ui/visit/sagas';
import { USER_LOGIN_ACTION } from './features/userLogin/actions/userLoginAction';
import userLogin from './features/userLogin/sagas/userLoginSaga';
import { USER_LOGOUT_ACTION } from './features/userLogin/actions/userLogoutAction';
import userLogout from './features/userLogin/sagas/userLogoutSaga';
import { FETCH_LOCATIONS_ACTION } from './features/locations/actions/fetchLocationsAction';
import fetchLocations from './features/locations/sagas/fetchLocationsSaga';

function* rootSaga() {
  yield all([
    takeEvery(FETCH_FORM_ACTION, fetchForm),
    takeEvery(FETCH_FORM_RESOURCE_ACTION, fetchFormResource),
    takeEvery(SAVE_ENCOUNTER_ACTION, saveEncounter),
    takeEvery(SAVE_OBSERVATION_ACTION, saveObservation),
    takeEvery(SAVE_PATIENT_ATTRIBUTE_ACTION, savePatientAttribute),
    takeEvery(SAVE_PATIENT_IDENTIFIER_ACTION, savePatientIdentifier),
    takeEvery(SAVE_PATIENT_IMAGE_ACTION, savePatientImage),
    takeEvery(SAVE_VISIT_ACTION, saveVisit),
    takeEvery(SAVE_VISIT_ATTRIBUTE_ACTION, saveVisitAttribute),
    takeEvery(SUBMIT_FORM_ACTION, submitForm),
    takeLatest(FETCH_EPISODES_ACTION, fetchEpisodes),
    takeLatest(FETCH_VISITS_ACTION, fetchVisits),
    takeLatest(USER_LOGIN_ACTION, userLogin),
    takeLatest(USER_LOGOUT_ACTION, userLogout),
    takeLatest(FETCH_LOCATIONS_ACTION, fetchLocations),
    takeLatest(FETCH_SESSION_ACTION, fetchSession),
    takeLatest(REGISTER_CLIENT_ACTION, registerClient),
    takeLatest(START_VISIT_ACTION, startVisit),
  ]);
}

export default rootSaga;
