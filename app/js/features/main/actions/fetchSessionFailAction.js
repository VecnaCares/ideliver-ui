export const FETCH_SESSION_FAILED = 'FETCH_SESSION_FAILED';

const fetchSessionFailAction = value => ({
  type: FETCH_SESSION_FAILED,
  payload: value,
});

export default fetchSessionFailAction;
