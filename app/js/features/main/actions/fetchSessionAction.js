export const FETCH_SESSION_ACTION = 'FETCH_SESSION_ACTION';

const fetchSessionAction = (url, payload) => ({
  type: FETCH_SESSION_ACTION,
  url,
  payload,
});

export default fetchSessionAction;
