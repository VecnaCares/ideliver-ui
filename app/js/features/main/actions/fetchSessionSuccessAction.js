export const FETCH_SESSION_SUCCEEDED = 'FETCH_SESSION_SUCCEEDED';

const fetchSessionSuccessAction = value => ({
  type: FETCH_SESSION_SUCCEEDED,
  payload: value,
});

export default fetchSessionSuccessAction;
