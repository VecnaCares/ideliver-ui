import 'regenerator-runtime/runtime';
import { call, put } from 'redux-saga/effects';
import fetchSessionSuccessAction from '../actions/fetchSessionSuccessAction';
import fetchSessionFailAction from '../actions/fetchSessionFailAction';
import { postData } from '../../../api';

export const getUrlQuery = action => {
  const { url } = action;
  return `${url}appui/session`;
};

// worker Saga: will be fired on FETCH_SESSION_ACTION actions
export default function* fetchSession(action) {
  const urlQuery = getUrlQuery(action);
  const { apiAuthorization, location } = action.payload;
  try {
    const session = yield call(
      postData,
      urlQuery,
      { location },
      apiAuthorization
    );
    yield put(fetchSessionSuccessAction(session));
  } catch (e) {
    yield put(fetchSessionFailAction(e.message));
  }
}
