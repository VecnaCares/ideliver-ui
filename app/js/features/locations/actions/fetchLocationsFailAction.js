export const FETCH_LOCATIONS_FAILED = 'FETCH_LOCATIONS_FAILED';

const fetchLocationsFailAction = payload => ({
  type: FETCH_LOCATIONS_FAILED,
  payload,
});

export default fetchLocationsFailAction;
