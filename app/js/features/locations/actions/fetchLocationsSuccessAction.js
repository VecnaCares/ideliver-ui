export const FETCH_LOCATIONS_SUCCEEDED = 'FETCH_LOCATIONS_SUCCEEDED';

const fetchLocationsSuccessAction = payload => ({
  type: FETCH_LOCATIONS_SUCCEEDED,
  payload,
});

export default fetchLocationsSuccessAction;
