export const FETCH_LOCATIONS_ACTION = 'FETCH_LOCATIONS_ACTION';

const fetchLocationsAction = (url, payload) => ({
  type: FETCH_LOCATIONS_ACTION,
  url,
  payload,
});

export default fetchLocationsAction;
