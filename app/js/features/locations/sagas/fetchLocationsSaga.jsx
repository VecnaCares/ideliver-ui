import { call, put } from 'redux-saga/effects';
import { get, set } from 'lodash';
import { getData } from '../../../api';
import fetchLocationsFailAction from '../actions/fetchLocationsFailAction';
import fetchLocationsSuccessAction from '../actions/fetchLocationsSuccessAction';

export const getUrlQuery = action => {
  const { url } = action;
  const { tag } = action.payload;

  let query = '';
  if (tag) {
    query = `&tag=${tag}`;
  }

  const customRepresentation =
    '?v=custom:(uuid,display,name,tags,parentLocation:(uuid,display,name,tags),childLocations,retired,links)';

  return `${url}location/${customRepresentation}${query}`;
};

// worker Saga: will be fired on FETCH_LOCATIONS_ACTION actions
export default function* fetchLocations(action) {
  const urlQuery = getUrlQuery(action);

  try {
    const locations = yield call(getData, urlQuery);
    const { results } = locations;
    const resultsByUuids = {};
    const resultsByTags = {};
    results.forEach(result => {
      set(resultsByUuids, [result.uuid], result);
      const { tags } = result;
      if (tags && tags.length) {
        tags.forEach(tag => {
          const currentTags = get(resultsByTags, [tag.uuid], []);
          currentTags.push(result.uuid);
          set(resultsByTags, [tag.uuid], currentTags);
        });
      }
    });

    const payload = {
      locations: resultsByUuids,
      tags: resultsByTags,
      totalCount: results.length,
    };
    yield put(fetchLocationsSuccessAction(payload));
  } catch (e) {
    yield put(fetchLocationsFailAction(e.message));
  }
}
