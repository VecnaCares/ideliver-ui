/**
 * list.locations represent the list of all locations.
 * list.totalCount is the count of locations in list.locations
 * tags is a list of location uuids referenced in the list.locations
 * For example:
 *  if we have 3 locations with uuids uui1, uud2, uuid3, the will be represented like this
 *  list.locations: {uuid1: {}, uuid2: {}, uuid3: {}}
 *  If location uuid1 and uui2 are login locations, they will be represented like this
 *  tags.loginLocationUuid: [uuid1, uuid2]
 *  tags.visitLocationUuid: [uuid1, uuid3]
 *  This representation allows us to store only one reference to the object saving storage
 *  and lookup time
 * @type {{list: {locations: {}, totalCount: number}, tags: {}}}
 */
import { fromJS } from 'immutable';
import { FETCH_LOCATIONS_SUCCEEDED } from './actions/fetchLocationsSuccessAction';
import { FETCH_LOCATIONS_FAILED } from './actions/fetchLocationsFailAction';

export const defaultState = {
  locations: {},
  totalCount: 0,
  tags: {},
  errors: [],
};

const locationsReducer = (state = fromJS(defaultState), action) => {
  switch (action.type) {
    case FETCH_LOCATIONS_SUCCEEDED:
      return fromJS(action.payload).setIn(['errors'], fromJS([]));

    case FETCH_LOCATIONS_FAILED:
      return state.setIn(['errors'], fromJS([action.payload]));
    default:
      return state;
  }
};

export default locationsReducer;
