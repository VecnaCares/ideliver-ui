import { encounterType, form } from '../../uuid';

export const createReferPatientEncounter = (
  { encounterUuid, patientUuid, visitUuid, sessionLocationUuid },
  obs
) => {
  const encounter = {
    uuid: encounterUuid,
    location: sessionLocationUuid,
    encounterType: encounterType.REFERRAL_ENCOUNTER_TYPE_UUID,
    form: form.REFERRAL_FORM_UUID,
    patient: patientUuid,
    visit: visitUuid,
  };

  encounter.obs = [obs];

  return encounter;
};
