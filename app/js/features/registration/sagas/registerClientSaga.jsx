import 'regenerator-runtime/runtime';
import React from 'react';
import { FormattedDate } from 'react-intl';
import { call, put } from 'redux-saga/effects';
import registerClientSuccessAction from '../actions/registerClientSuccessAction';
import registerClientFailAction from '../actions/registerClientFailAction';
import { postData } from '../../../api';
import { concept, identifierType, location } from '../../../uuid';

export default function* registerClient(action) {
  try {
    const idResult = yield call(
      postData,
      `${action.url}idgen/identifiersource/${concept.IDENTIFIER_SOURCE_UUID}/identifier`,
      { identifierSource: concept.IDENTIFIER_SOURCE_UUID }
    );
    const identifiers = [
      {
        identifierType: { uuid: identifierType.OPENMRS_ID_UUID },
        location: { uuid: location.INPATIENT_WARD },
        ...idResult,
      },
    ];
    const patientJson = {
      person: action.payload,
      identifiers,
    };

    const patient = yield call(postData, `${action.url}patient/`, patientJson);

    const result = {
      results: [
        {
          id: patient.uuid,
          mrn: idResult.identifier,
          givenName: action.payload.names[0].givenName,
          middleName: action.payload.names[0].middleName,
          familyName: action.payload.names[0].familyName,
          gender: patient.person.gender,
          age: patient.person.age,
          birthdate: (
            <FormattedDate
              value={patient.person.birthdate}
              month="short"
              day="2-digit"
              year="numeric"
            />
          ),
        },
      ],
      totalCount: 1,
    };

    localStorage.setItem('patient', patient.uuid);
    let registerOrsave = localStorage.getItem('register');
    if (registerOrsave === '0') {
      action.history.push('/visits');
    }
    yield put(registerClientSuccessAction(result));
  } catch (e) {
    yield put(registerClientFailAction(e.message));
  }
}
