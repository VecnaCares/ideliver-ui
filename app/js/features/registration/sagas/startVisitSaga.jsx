import 'regenerator-runtime/runtime';
import { call, put } from 'redux-saga/effects';
import startVisitSuccessAction from '../actions/startVisitSuccessAction';
import startVisitFailAction from '../actions/startVisitFailAction';
import { postData } from '../../../api';

export const getUrlQuery = action => {
  const clientId2 = localStorage.getItem('patient');

  const { url, locationUuid } = action;
  return `${url}emrapi/activevisit?patient=${clientId2}&location=${locationUuid}`;
};

export default function* startVisit(action) {
  const urlQuery = getUrlQuery(action);
  try {
    const result = yield call(postData, urlQuery);
    yield put(startVisitSuccessAction(action.clientId));
  } catch (e) {
    yield put(startVisitFailAction(e.message));
  }
}
