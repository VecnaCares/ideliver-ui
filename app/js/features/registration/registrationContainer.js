import { connect } from 'react-redux';
import Registration from './registration';
import registerClientAction from './actions/registerClientAction';
import { REST_API_PATHNAME, REST_API_VERSION } from '../../paths';
import startVisitAction from './actions/startVisitAction';

const mapStateToProps = (state, props) => {
  return {
    submittingRegistration: state.Register.get('submittingRegistration'),
    success: state.Register.get('success'),
    startVisitSuccess: state.Register.get('startVisitSuccess'),
    sessionLocationUuid: state.User.getIn([
      'session',
      'sessionLocation',
      'uuid',
    ]),
  };
};

const mapDispatchToProps = (dispatch, props) => {
  const path = props.location.pathname.split('/');
  const clientId = path[2];

  return {
    onSubmit: person => {
      dispatch(
        registerClientAction(
          REST_API_PATHNAME + REST_API_VERSION,
          person,
          props.history
        )
      );
    },
    startVisit: locationUuid => {
      dispatch(startVisitAction(REST_API_PATHNAME, clientId, locationUuid));
    },
  };
};

const RegistrationContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Registration);

export default RegistrationContainer;
