export const START_VISIT_SUCCEEDED = 'START_VISIT_SUCCEEDED';

const startVisitSuccessAction = clientId => ({
  type: START_VISIT_SUCCEEDED,
  clientId,
});

export default startVisitSuccessAction;
