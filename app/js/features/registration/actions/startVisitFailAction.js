export const START_VISIT_FAILED = 'START_VISIT_FAILED';

const startVisitFailAction = value => ({
  type: START_VISIT_FAILED,
  payload: value,
});

export default startVisitFailAction;
