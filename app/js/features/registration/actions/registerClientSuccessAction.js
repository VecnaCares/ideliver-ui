export const REGISTER_CLIENT_SUCCEEDED = 'REGISTER_CLIENT_SUCCEEDED';

const registerClientSuccessAction = payload => ({
  type: REGISTER_CLIENT_SUCCEEDED,
  payload,
});

export default registerClientSuccessAction;
