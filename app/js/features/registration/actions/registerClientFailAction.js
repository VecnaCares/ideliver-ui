export const REGISTER_CLIENT_FAILED = 'REGISTER_CLIENT_FAILED';

const registerClientFailAction = payload => ({
  type: REGISTER_CLIENT_FAILED,
  payload,
});

export default registerClientFailAction;
