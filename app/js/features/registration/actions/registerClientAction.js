export const REGISTER_CLIENT_ACTION = 'REGISTER_CLIENT_ACTION';

const registerClientAction = (url, payload, history) => ({
  type: REGISTER_CLIENT_ACTION,
  url,
  payload,
  history,
});

export default registerClientAction;
