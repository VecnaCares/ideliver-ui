export const START_VISIT_ACTION = 'START_VISIT_ACTION';

const startVisitAction = (url, clientId, locationUuid) => ({
  type: START_VISIT_ACTION,
  url,
  clientId,
  locationUuid,
});

export default startVisitAction;
