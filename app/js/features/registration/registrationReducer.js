import { fromJS } from 'immutable';
import { REGISTER_CLIENT_ACTION } from './actions/registerClientAction';
import { REGISTER_CLIENT_FAILED } from './actions/registerClientFailAction';
import { REGISTER_CLIENT_SUCCEEDED } from './actions/registerClientSuccessAction';
import { START_VISIT_ACTION } from './actions/startVisitAction';
import { START_VISIT_FAILED } from './actions/startVisitFailAction';
import { START_VISIT_SUCCEEDED } from './actions/startVisitSuccessAction';

const registerReducer = (
  state = fromJS({
    submittingRegistration: false,
    success: false,
    startVisitSuccess: false,
  }),
  action
) => {
  switch (action.type) {
    case REGISTER_CLIENT_ACTION:
      return state
        .set('submittingRegistration', true)
        .setIn(['startVisitSuccess'], false)
        .setIn(['success'], false);

    case REGISTER_CLIENT_FAILED:
      return state
        .setIn(['submittingRegistration'], false)
        .setIn(['startVisitSuccess'], false)
        .setIn(['success'], false);

    case REGISTER_CLIENT_SUCCEEDED:
      return state
        .setIn(['submittingRegistration'], false)
        .setIn(['startVisitSuccess'], false)
        .setIn(['success'], true);

    case START_VISIT_ACTION:
      return state
        .set('submittingRegistration', true)
        .setIn(['startVisitSuccess'], false)
        .setIn(['success'], false);

    case START_VISIT_FAILED:
      return state
        .setIn(['submittingRegistration'], false)
        .setIn(['startVisitSuccess'], false)
        .setIn(['success'], false);

    case START_VISIT_SUCCEEDED:
      return state
        .setIn(['submittingRegistration'], false)
        .setIn(['startVisitSuccess'], true)
        .setIn(['success'], false);

    default:
      return state;
  }
};

export default registerReducer;
