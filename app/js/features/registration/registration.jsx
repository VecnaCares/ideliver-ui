import React from 'react';
import PropTypes from 'prop-types';
import { set } from 'lodash';
import moment from 'moment';
import { injectIntl } from 'react-intl';
import Paper from '@material-ui/core/Paper';
import Modal from '@material-ui/core/Modal';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import VcHeader from '../../components/vcHeader/vcHeader';
import messages from '../../intl/messages';
import VcTextField from '../../components/vcTextField/vcTextField';
import VcDropDown from '../../components/vcDropDown/vcDropDown';
import VcGenericSwitch from '../../components/vcGenericSwitch/vcGenericSwitch';
import VcButton from '../../components/vcButton/vcButton';
import styles from './registration.scss';
import VcGridRow from '../../components/vcGrid/vcGridRow/vcGridRow';
import { attributeType } from '../../uuid';
import MainSidebar from '../mainSidebar/mainSidebar';
let monthArray = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];
let date;
class Registration extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      person: {
        names: [
          {
            givenName: '',
            middleName: '',
            familyName: '',
          },
        ],
        gender: '',
        age: '',
        birthdate: '',
        birthdateEstimated: '',
        addresses: [
          {
            preferred: true,
            address1: '',
            address2: '',
            cityVillage: '',
            stateProvince: '',
            country: '',
            postalCode: '',
          },
        ],
        attributes: [
          {
            value: '',
            attributeType: {
              uuid: attributeType.PHONE_UUID,
            },
          },
        ],
      },
      birthdateDay: '',
      birthdateMonth: '',
      positionOfMonth: '00',
      birthdateYear: '',
      estimatedMonth: '',
      estimatedYear: '',
      unidentified: '',
      showValidation: false,
      success: false,
    };
  }
  hasRequiredFields = () =>
    this.state.person.names[0].givenName &&
    this.state.person.names[0].familyName &&
    this.state.person.gender &&
    this.state.birthdateDay &&
    this.state.birthdateYear &&
    this.state.birthdateMonth &&
    this.state.person.addresses[0].address1;

  handleChange = (path, value) => {
    if (value.length > 100) {
      this.setState(prevState => set(prevState, path, ''));
    } else {
      this.setState(prevState => set(prevState, path, value));
    }
  };

  handleUnidentified = (path, value) => {
    this.setState({ unidentified: value });
  };

  handleChangeMonth = (path, value) => {
    let position = monthArray.indexOf(value);
    if (value.length > 100) {
      this.setState(prevState => set(prevState, 'person.birthdate', ''));
    } else {
      this.setState({ birthdateMonth: value });
      if (position < 10) {
        this.setState({ positionOfMonth: '0' + (position + 1) });
      } else {
        this.setState({ positionOfMonth: position + 1 });
      }
      this.setState(prevState =>
        set(
          prevState,
          'person.birthdate',
          this.state.birthdateYear +
            '-' +
            position +
            '-' +
            this.state.birthdateDay
        )
      );
    }
  };
  handleChangeDOB(path, value) {
    this.setState({ estimatedYear: '' });
    if (path === 'birthdateDay') {
      if (this.handleNumber(path, value)) {
        if (value > 31) {
          this.setState({ birthdateDay: value });

          this.setState(prevState => set(prevState, 'person.birthdate', ''));
        } else {
          this.setState({ birthdateDay: value });

          this.setState(prevState =>
            set(
              prevState,
              'person.birthdate',
              this.state.birthdateYear +
                '-' +
                this.state.positionOfMonth +
                '-' +
                value
            )
          );
        }
      } else {
        this.setState({ birthdateDay: value });

        this.setState(prevState => set(prevState, 'person.birthdate', ''));
      }
    } else if (path === 'birthdateYear') {
      if (this.handleNumber(path, value)) {
        if (value > 2100 || value < 1900) {
          this.setState({ birthdateYear: '' });

          this.setState(prevState => set(prevState, 'person.birthdate', ''));
        } else {
          this.setState({ birthdateYear: value });
          this.setState(prevState =>
            set(
              prevState,
              'person.birthdate',
              value +
                '-' +
                this.state.positionOfMonth +
                '-' +
                this.state.birthdateDay
            )
          );
          date = new Date();
          let currentYear = date.getFullYear();
          let age = currentYear - value;
          this.setState(prevState => set(prevState, 'person.age', age));
        }
      } else {
        this.setState(prevState => set(prevState, 'person.birthdate', ''));
      }
    }
  }

  handleNumber = (path, value) => {
    let number = /^[0-9\b]+$/;
    let isValidNumber = new RegExp(number).test(value) ? true : false;
    return isValidNumber;
  };

  handleCancel = () => {
    this.props.history.push('/visits');
  };
  handleEstimate(path, value) {
    if (path === 'person.birthdateEstimated') {
      if (this.handleNumber(path, value)) {
        if (value > 119) {
          this.setState(prevState => set(prevState, path, ''));
        } else {
          this.setState({ estimatedYear: value });
          this.setState(prevState => set(prevState, path, true));
          this.setState(prevState => set(prevState, 'person.age', value));
          date = new Date();
          date = moment(date)
            .subtract(value, 'year')
            .format('YYYY-MM-DD');

          let day = moment(date, 'YYYY/MM/DD').date();
          let month = moment(date, 'YYYY/MM/DD').month();
          let year = moment(date, 'YYYY/MM/DD').year();
          this.setState({ birthdateDay: 1 });
          this.setState({ birthdateYear: year });
          if (month < 10) {
            this.setState({ positionOfMonth: '0' + (month + 1) });
          } else {
            this.setState({ positionOfMonth: this.state.month + 1 });
          }

          this.setState(prevState => set(prevState, 'person.birthdate', date));

          const monthtoShow = monthArray[0];
          this.setState({ birthdateMonth: monthtoShow });
        }
      } else {
        this.setState(prevState => set(prevState, path, ''));
      }
    } else if (path === 'person.estimatedMonth') {
      if (this.handleNumber(path, value)) {
        if (value > 12) {
          this.setState({ estimatedMonth: '' });
        } else {
          this.setState({ estimatedMonth: value });
        }
      } else {
        this.setState({ estimatedMonth: '' });
      }
    }
  }

  componentWillReceiveProps(props) {
    if (props.success) {
      this.props.startVisit(this.props.sessionLocationUuid);
    }
    if (props.startVisitSuccess) {
      this.props.history.push('/visits');
    }
  }

  saveAndStartVisit = () => {
    if (this.hasRequiredFields()) {
      this.setState({ showValidation: false });
      localStorage.setItem('register', '1');
      this.props.onSubmit(this.state.person);
    } else {
      this.setState({ showValidation: true });
    }
  };

  isValidInputPhoneNumber = (path, value) => {
    let phoneno = /^[0-9.+-]{9,15}$/;
    let isValidPhoneNumber = false;
    if (path === 'person.attributes[0].value') {
      isValidPhoneNumber = new RegExp(phoneno).test(value) ? true : false;
    }
    return isValidPhoneNumber;
  };

  handleChangePhoneNumber(path, value) {
    if (this.isValidInputPhoneNumber(path, value)) {
      this.setState(prevState => set(prevState, path, value.toString()));
    } else {
      this.setState(prevState => set(prevState, path, ''));
    }
  }
  isValidPostalCode = (path, value) => {
    let postalCode = /^[0-9]{0,15}$/;
    let isValidPostalcode = false;
    if (path === 'person.addresses[0].postalCode') {
      isValidPostalcode = new RegExp(postalCode).test(value) ? true : false;
    }
    return isValidPostalcode;
  };
  handleChangePostalCode = (path, value) => {
    if (this.isValidPostalCode(path, value)) {
      this.setState(prevState => set(prevState, path, value.toString()));
    } else {
      this.setState(prevState => set(prevState, path, ''));
    }
  };

  render() {
    const { formatMessage } = this.props.intl;
    return [
      <MainSidebar
        location={this.props.location.pathname}
        key="registrationMainSidebar"
      />,
      <div>
        <VcHeader
          datatest={formatMessage(messages.dataTestRegistration)}
          title={formatMessage(messages.registration)}
          user={this.props.session ? this.props.session.user : null}
          logout={this.props.logout}
        />
        <div className={styles.registration}>
          <div className={styles.container}>
            <Paper elevation={24} className={styles.paper}>
              <Modal
                open={this.props.submittingRegistration}
                BackdropProps={{ invisible: true }}
                disableAutoFocus
              >
                <div className={styles.alignCenter}>
                  <CircularProgress size={50} className={styles.progress} />
                </div>
              </Modal>
              <div className={styles.section}>
                <div className={styles.textCon}>
                  <Typography variant="h5" gutterBottom>
                    {formatMessage(messages.demographics)}
                  </Typography>
                  <VcGridRow>
                    <VcTextField
                      datatest={formatMessage(messages.dataTestGivenName)}
                      label={formatMessage(messages.givenName)}
                      vType="text"
                      value={this.state.person.names[0].givenName}
                      error={
                        !this.state.person.names[0].givenName &&
                        this.state.showValidation
                      }
                      onChange={value =>
                        this.handleChange(
                          'person.names[0].givenName',
                          value.trim()
                        )
                      }
                      maxLength={50}
                      required
                    />
                    <VcTextField
                      datatest={formatMessage(messages.dataTestMiddleName)}
                      label={formatMessage(messages.middleName)}
                      vType="text"
                      onChange={value =>
                        this.handleChange(
                          'person.names[0].middleName',
                          value.trim()
                        )
                      }
                      value={this.state.person.names[0].middleName}
                      maxLength={50}
                    />
                    <VcTextField
                      datatest={formatMessage(messages.dataTestFamilyName)}
                      label={formatMessage(messages.familyName)}
                      vType="text"
                      value={this.state.person.names[0].familyName}
                      error={
                        !this.state.person.names[0].familyName &&
                        this.state.showValidation
                      }
                      onChange={value =>
                        this.handleChange(
                          'person.names[0].familyName',
                          value.trim()
                        )
                      }
                      maxLength={50}
                      required
                    />
                  </VcGridRow>
                </div>
                <VcGridRow>
                  <VcGenericSwitch
                    datatest={formatMessage(
                      messages.dataTestUnidentifiedPatient
                    )}
                    label={formatMessage(messages.unidentifiedPatient)}
                    vType="text"
                    onChange={value =>
                      this.handleUnidentified('unidentified', value)
                    }
                    value={this.state.unidentified}
                  />
                </VcGridRow>
                <VcGridRow>
                  <VcDropDown
                    datatest={formatMessage(messages.dataTestGender)}
                    label={formatMessage(messages.gender)}
                    value={this.state.person.gender}
                    className={styles.dropDown}
                    onChange={value =>
                      this.handleChange('person.gender', value)
                    }
                    options={[
                      {
                        value: 'M',
                        display: formatMessage(messages.genderM),
                      },
                      {
                        value: 'F',
                        display: formatMessage(messages.genderF),
                      },
                    ]}
                    required
                    error={
                      !this.state.person.gender && this.state.showValidation
                    }
                  />
                </VcGridRow>
                <div className={styles.textCon + ' ' + styles.selectCon}>
                  <VcGridRow className={styles.margingtop}>
                    <VcTextField
                      datatest={formatMessage(messages.dataTestDay)}
                      label={formatMessage(messages.day)}
                      value={this.state.birthdateDay}
                      error={
                        !this.state.birthdateDay && this.state.showValidation
                      }
                      min={1}
                      max={31}
                      maxLength={2}
                      vType="number"
                      onChange={value =>
                        this.handleChangeDOB('birthdateDay', value)
                      }
                    />
                    <VcDropDown
                      datatest={formatMessage(messages.dataTestMonth)}
                      label={formatMessage(messages.month)}
                      vType="text"
                      error={
                        !this.state.birthdateMonth && this.state.showValidation
                      }
                      value={this.state.birthdateMonth}
                      options={[
                        {
                          value: 'January',
                          display: formatMessage(messages.monthJan),
                        },
                        {
                          value: 'February',
                          display: formatMessage(messages.monthFeb),
                        },
                        {
                          value: 'March',
                          display: formatMessage(messages.monthMar),
                        },
                        {
                          value: 'April',
                          display: formatMessage(messages.monthApr),
                        },
                        {
                          value: 'May',
                          display: formatMessage(messages.monthMay),
                        },
                        {
                          value: 'June',
                          display: formatMessage(messages.monthJun),
                        },
                        {
                          value: 'July',
                          display: formatMessage(messages.monthJul),
                        },
                        {
                          value: 'August',
                          display: formatMessage(messages.monthAug),
                        },
                        {
                          value: 'September',
                          display: formatMessage(messages.monthSept),
                        },
                        {
                          value: 'October',
                          display: formatMessage(messages.monthOct),
                        },
                        {
                          value: 'November',
                          display: formatMessage(messages.monthNov),
                        },
                        {
                          value: 'December',
                          display: formatMessage(messages.monthDec),
                        },
                      ]}
                      onChange={value =>
                        this.handleChangeMonth('birthdateMonth', value)
                      }
                    />
                    <VcTextField
                      datatest={formatMessage(messages.dataTestYear)}
                      label={formatMessage(messages.year)}
                      vType="number"
                      maxLength={4}
                      min={1900}
                      value={this.state.birthdateYear}
                      error={
                        !this.state.birthdateYear && this.state.showValidation
                      }
                      onChange={value =>
                        this.handleChangeDOB('birthdateYear', value)
                      }
                    />
                  </VcGridRow>
                </div>
                <div className={styles.textCon}>
                  <VcGridRow>
                    <VcTextField
                      datatest={formatMessage(messages.dataTestEstimatedYears)}
                      label={formatMessage(messages.estimatedYears)}
                      vType="number"
                      className={styles.estimate}
                      value={this.state.estimatedYear}
                      onChange={value =>
                        this.handleEstimate('person.birthdateEstimated', value)
                      }
                      error={
                        !this.state.birthdateYear && this.state.showValidation
                      }
                      disabled={this.state.estimatedMonth}
                      min={1}
                      max={119}
                      maxLength={3}
                    />
                  </VcGridRow>
                </div>
              </div>
              <div className={styles.section}>
                <Typography variant="h5" gutterBottom>
                  {formatMessage(messages.contactInfo)}
                </Typography>
                <VcGridRow>
                  <VcTextField
                    datatest={formatMessage(messages.dataTestAddress)}
                    label={formatMessage(messages.address)}
                    vType="text"
                    onChange={value =>
                      this.handleChange(
                        'person.addresses[0].address1',
                        value.trim()
                      )
                    }
                    value={this.state.person.addresses[0].address1}
                    error={
                      !this.state.person.addresses[0].address1 &&
                      this.state.showValidation
                    }
                    maxLength={100}
                    required
                  />
                </VcGridRow>
                <VcGridRow>
                  <VcTextField
                    datatest={formatMessage(messages.dataTestAddress2)}
                    label={formatMessage(messages.address2)}
                    vType="text"
                    onChange={value =>
                      this.handleChange('person.addresses[0].address2', value)
                    }
                    value={this.state.person.addresses[0].address2}
                    maxLength={100}
                  />
                </VcGridRow>
                <VcGridRow>
                  <VcTextField
                    datatest={formatMessage(messages.dataTestCity)}
                    label={formatMessage(messages.city)}
                    value={this.state.person.addresses[0].cityVillage}
                    onChange={value =>
                      this.handleChange(
                        'person.addresses[0].cityVillage',
                        value
                      )
                    }
                    vType="text"
                    maxLength={100}
                  />
                </VcGridRow>
                <VcGridRow>
                  <VcTextField
                    datatest={formatMessage(messages.dataTestState)}
                    label={formatMessage(messages.state)}
                    value={this.state.person.addresses[0].stateProvince}
                    onChange={value =>
                      this.handleChange(
                        'person.addresses[0].stateProvince',
                        value
                      )
                    }
                    vType="text"
                    maxLength={100}
                  />
                </VcGridRow>
                <VcGridRow>
                  <VcTextField
                    datatest={formatMessage(messages.dataTestCountry)}
                    label={formatMessage(messages.country)}
                    value={this.state.person.addresses[0].country}
                    onChange={value =>
                      this.handleChange('person.addresses[0].country', value)
                    }
                    vType="text"
                    maxLength={50}
                  />
                </VcGridRow>
                <VcGridRow>
                  <VcTextField
                    datatest={formatMessage(messages.dataTestPostalCode)}
                    label={formatMessage(messages.postalCode)}
                    value={this.state.person.addresses[0].postalCode}
                    onChange={value =>
                      this.handleChangePostalCode(
                        'person.addresses[0].postalCode',
                        value
                      )
                    }
                    vType="text"
                    min={0}
                    maxLength={50}
                  />
                </VcGridRow>
                <VcGridRow>
                  <div className={styles.noArrow}>
                    <VcTextField
                      datatest={formatMessage(messages.dataTestPhoneNumber)}
                      label={formatMessage(messages.phoneNumber)}
                      vType="text"
                      value={this.state.person.attributes[0].value}
                      onChange={value =>
                        this.handleChangePhoneNumber(
                          'person.attributes[0].value',
                          value
                        )
                      }
                      maxLength={50}
                    />
                  </div>
                </VcGridRow>
              </div>
              <VcGridRow className={styles.submitButtonContainer}>
                <VcButton
                  datatest={formatMessage(messages.dataTestSaveAndStartVisit)}
                  className={styles.primary}
                  onClick={this.saveAndStartVisit}
                  value={formatMessage(messages.SaveAndStartVisit)}
                  disabled={this.props.success}
                />
                <VcButton
                  datatest={formatMessage(messages.dataTestCancel)}
                  className={styles.red}
                  onClick={this.handleCancel}
                  value={formatMessage(messages.cancel)}
                />
              </VcGridRow>
            </Paper>
          </div>
        </div>
      </div>,
    ];
  }
}

Registration.propTypes = {
  submittingRegistration: PropTypes.bool,
  success: PropTypes.bool,
  startVisitSuccess: PropTypes.bool,
  intl: PropTypes.shape({ formatMessage: PropTypes.func }).isRequired,
  onSubmit: PropTypes.func.isRequired,
  switchValue: PropTypes.bool,
  session: PropTypes.shape({}),
  startVisit: PropTypes.func,
  logout: PropTypes.func,
};

Registration.defaultProps = {
  submittingRegistration: false,
  success: false,
  session: null,
  logout: null,
};

export default injectIntl(Registration);
