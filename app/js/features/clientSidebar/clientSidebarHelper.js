import { form, formField } from '../../uuid';
import { getFormStatus } from '../../state/ui/form/selectors';
export const showAncFormList = (menus, formListData, formStatus) => {
  const index = findIndexData(menus, 'ANC');
  const last = index ? menus[index].childrenMenu[1] : '';
  const { dataLength, data } = dataLengthData(formListData, form.ANC_FORM_UUID);
  const isScheduleNextAppointment = formField.ANC_VISIT_NEXT_APPOINTMENT_DATE;
  const {
    nextAppointmentDateCheck,
    childrenLength,
  } = nextAppointmentDateCheckChildrenLength(
    data,
    dataLength,
    isScheduleNextAppointment,
    menus,
    index
  );
  let menuData = {};
  if (dataLength >= 1 && childrenLength <= 2) {
    for (
      let childIndex = 2;
      childIndex <= nextAppointmentDateCheck + 1 && childIndex <= 10;
      childIndex++
    ) {
      let formIndex = childIndex;
      menuData = {
        display: 'ANC Visit ' + formIndex,
        status: getFormStatus(formStatus, [form.ANC_FORM_UUID, childIndex - 1]),
        formId: form.ANC_FORM_UUID,
        formIndex: childIndex - 1,
      };
      menus[index].childrenMenu.push(menuData);
    }
    menus[index].childrenMenu.push(last);
    return menuData;
  }
};
export const showPncFormList = (menus, formListData, formStatus) => {
  const index = findIndexData(menus, 'PNC');
  const { dataLength, data } = dataLengthData(formListData, form.PNC_FORM_UUID);
  const isScheduleNextAppointment = formField.PNC_SCHEDULE_NEXT_APPOINTMENT;
  const {
    nextAppointmentDateCheck,
    childrenLength,
  } = nextAppointmentDateCheckChildrenLength(
    data,
    dataLength,
    isScheduleNextAppointment,
    menus,
    index
  );
  let menuData = {};
  if (dataLength >= 1 && childrenLength <= 2) {
    for (
      let childIndex = 2;
      childIndex <= nextAppointmentDateCheck + 1 && childIndex <= 10;
      childIndex++
    ) {
      let formIndex = childIndex;
      menuData = {
        display: 'PNC ' + formIndex,
        status: getFormStatus(formStatus, [form.PNC_FORM_UUID, childIndex - 1]),
        formId: form.PNC_FORM_UUID,
        formIndex: childIndex - 1,
      };
      menus[index].childrenMenu.push(menuData);
    }
    return menuData;
  }
};

export const findIndexData = (menus, status) => {
  if (menus) {
    return menus.findIndex(indexId => indexId.display === status)
      ? menus.findIndex(indexId => indexId.display === status)
      : 0;
  }
};
const dataLengthData = (formListData, formUuid) => {
  const dataLength =
    formListData && formListData[formUuid] ? formListData[formUuid].length : 0;
  const data =
    formListData && formListData[formUuid] ? formListData[formUuid] : undefined;
  return { dataLength, data };
};
const nextAppointmentDateCheckChildrenLength = (
  data,
  dataLength,
  isScheduleNextAppointment,
  menus,
  index
) => {
  const nextAppointmentDateCheck =
    data &&
    data[dataLength - 1] &&
    data[dataLength - 1].encounterUuid &&
    data[dataLength - 1][isScheduleNextAppointment] &&
    data[dataLength - 1][isScheduleNextAppointment].value === true
      ? dataLength
      : dataLength - 1;

  const childrenLength =
    index && menus[index] && menus[index].childrenMenu
      ? menus[index].childrenMenu.length
      : 0;
  return { nextAppointmentDateCheck, childrenLength };
};
