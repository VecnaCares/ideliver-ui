import { connect } from 'react-redux';
import { REST_API_PATHNAME, REST_API_VERSION } from '../../paths';
import VcImage from './vcImage';
import { savePatientImageAction } from '../../state/ui/patient/actions';

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch, props) => ({
  onChange: data => {
    const payload = {
      person: props.personId,
      base64EncodedImage: data,
    };
    dispatch(
      savePatientImageAction(
        REST_API_PATHNAME + REST_API_VERSION,
        props.personId,
        payload
      )
    );
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(VcImage);
