import { call, put } from 'redux-saga/effects';
import userLoginFailAction from '../actions/userLoginFailAction';
import { postData } from '../../../api';
import userLoginSuccessAction from '../actions/userLoginSuccessAction';
import {
  API_AUTHORIZATION,
  SELECTED_LOCATION,
} from '../services/userLoginService';

export const getUrlQuery = action => {
  const { url } = action;

  return `${url}appui/session`;
};

// worker Saga: will be fired on USER_LOGIN_ACTION actions
export default function* userLogin(action) {
  const { username, password, location } = action.payload;
  const apiAuthorization = `Basic ${btoa(`${username}:${password}`)}`;
  const urlQuery = getUrlQuery(action);

  try {
    const session = yield call(
      postData,
      urlQuery,
      { location },
      apiAuthorization
    );
    const user = {
      session,
      apiAuthorization,
    };
    if (session.authenticated) {
      localStorage.setItem(API_AUTHORIZATION, apiAuthorization);
      localStorage.setItem(SELECTED_LOCATION, location);
      yield put(userLoginSuccessAction(user));
    } else {
      yield put(userLoginFailAction('loginFailed'));
    }
  } catch (e) {
    console.log(e);
    yield put(userLoginFailAction('loginFailed'));
  }
}
