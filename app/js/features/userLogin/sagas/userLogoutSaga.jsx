import React from 'react';
import { Redirect } from 'react-router-dom';
import { call, put } from 'redux-saga/effects';
import { deleteData } from '../../../api';

import userLogoutFailAction from '../actions/userLogoutFailAction';
import userLogoutSuccessAction from '../actions/userLogoutSuccessAction';
import {
  API_AUTHORIZATION,
  SELECTED_LOCATION,
} from '../services/userLoginService';

export const getUrlQuery = action => {
  const { url } = action;
  return `${url}appui/session`;
};

// worker Saga: will be fired on USER_LOGOUT_ACTION actions
export default function* userLogout(action) {
  const { apiAuthorization } = action.payload;
  const urlQuery = getUrlQuery(action);
  try {
    const session = yield call(deleteData, urlQuery, apiAuthorization);
    const user = {
      session,
      apiAuthorization: null,
    };
    localStorage.removeItem(API_AUTHORIZATION);
    localStorage.removeItem(SELECTED_LOCATION);
    yield put(userLogoutSuccessAction(user));
    yield (<Redirect to="/" />);
  } catch (e) {
    console.log(e);
    yield put(userLogoutFailAction(e.message));
  }
}
