export const API_AUTHORIZATION = 'apiAuthorization';
export const SELECTED_LOCATION = 'selectedLocation';

export const getLogInUrl = url => {
  return `${url}appui/session`;
};

export function getApiAuthorization() {
  return localStorage.getItem(API_AUTHORIZATION);
}

export function getLocation() {
  return localStorage.getItem(SELECTED_LOCATION);
}

export function isAuthenticated() {
  /* Determines if a user is logged in */

  // 1. If the user doesn't have the token set
  const apiAuthorization = getApiAuthorization();
  if (!apiAuthorization) {
    return false;
  }

  if (!getLocation()) {
    return false;
  }

  // 2. Check authorization format matches BASIC
  // 3. Check token expiry

  return true;
}

export function logOut() {
  // just remove the token
  localStorage.removeItem(API_AUTHORIZATION);
  localStorage.removeItem(SELECTED_LOCATION);
}
