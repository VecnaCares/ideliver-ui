import { connect } from 'react-redux';
import { fromJS } from 'immutable';

import UserLogin from './userLogin';
import fetchLocationsAction from '../locations/actions/fetchLocationsAction';
import userLoginAction from './actions/userLoginAction';
import { REST_API_PATHNAME, REST_API_VERSION } from '../../paths';
import { location } from '../../uuid';

export const getLoginLocations = state => {
  const locations = state.Locations.getIn(['locations'], fromJS({})).toJS();
  const loginLocationsUuids = state.Locations.getIn(
    ['tags', location.LOGIN_LOCATION_UUID],
    fromJS([])
  ).toJS();
  return loginLocationsUuids.map(locationUuid => {
    const loginLocation = locations[locationUuid];
    return {
      display: loginLocation.display,
      value: loginLocation.uuid,
    };
  });
};

const mapStateToProps = state => ({
  submitting: state.User.get('submitting'),
  isAuthenticated: state.User.getIn(['session', 'authenticated']),
  loginErrors: state.User.getIn(['errors']).toJS(),
  loginLocations: getLoginLocations(state),
});

const mapDispatchToProps = dispatch => ({
  onSubmit: data =>
    dispatch(userLoginAction(REST_API_PATHNAME + REST_API_VERSION, data)),
  fetchLocations: data =>
    dispatch(fetchLocationsAction(REST_API_PATHNAME + REST_API_VERSION, data)),
});

const UserLoginContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(UserLogin);

export default UserLoginContainer;
