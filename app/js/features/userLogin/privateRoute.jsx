import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import {
  getApiAuthorization,
  getLocation,
  isAuthenticated,
} from './services/userLoginService';
import { REST_API_PATHNAME, REST_API_VERSION } from '../../paths';
import fetchSessionAction from '../main/actions/fetchSessionAction';
import userLogoutAction from './actions/userLogoutAction';

/**
 * Component for rendering private routes that require authentication
 * It checks if the user is authenticated.
 * If they are, it renders the provided component and passes down the user session
 * and fetchSession function as props
 * otherwise redirects to the login page
 *
 * Notes:
 *      Because we want to preserve login after page reloads, we store the user token
 *      in the local storage. If the user reloads the page, the token will still be persisted,
 *      but redux state will be destroyed. That's why we check for the api token in the
 *      local storage first. If there's no corresponding user session in redux store,
 *      we fire an action to fetch the session information and update the store.
 *
 * @param Component the component to render
 * @param rest the properties to pass to the component
 * @returns {*} the rendered component or the login page if the user is not authenticated
 */
export const PrivateRouteRenderer = ({ component: Component, ...rest }) => {
  const renderPrivateRoute = props => {
    const { session } = rest;
    if (!session) {
      // no session in redux store, but the user is logged in. So fetch the user session
      rest.fetchSession();
    }
    return <Component {...props} {...rest} />;
  };
  return (
    <Route
      {...rest}
      render={props =>
        isAuthenticated() ? (
          renderPrivateRoute(props)
        ) : (
          <Redirect
            to={{ pathname: '/login', state: { from: props.location } }}
          />
        )
      }
    />
  );
};

PrivateRouteRenderer.propTypes = {
  /** The session for the logged in user */
  session: PropTypes.shape({}).isRequired,
  /** The function used to fetch the session for logged in user */
  fetchSession: PropTypes.func.isRequired,
  /** Function to log the user out */
  logout: PropTypes.func.isRequired,
  /** The component to render */
  component: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  session: state.User.get('session') ? state.User.get('session').toJS() : null,
});

const mapDispatchToProps = dispatch => {
  const url = REST_API_PATHNAME + REST_API_VERSION;
  const apiAuthorization = getApiAuthorization();
  const location = getLocation();
  const payload = { apiAuthorization, location };
  return {
    fetchSession: () => dispatch(fetchSessionAction(url, payload)),
    logout: () => dispatch(userLogoutAction(url, payload)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PrivateRouteRenderer);
