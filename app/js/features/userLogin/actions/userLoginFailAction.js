export const USER_LOGIN_FAILED = 'USER_LOGIN_FAILED';

const userLoginFailAction = payload => ({
  type: USER_LOGIN_FAILED,
  payload,
});

export default userLoginFailAction;
