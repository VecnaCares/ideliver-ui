export const USER_LOGOUT_ACTION = 'USER_LOGOUT_ACTION';

const userLogoutAction = (url, payload) => ({
  type: USER_LOGOUT_ACTION,
  url,
  payload,
});

export default userLogoutAction;
