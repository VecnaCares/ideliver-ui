export const USER_LOGOUT_SUCCEEDED = 'USER_LOGOUT_SUCCEEDED';

const userLogoutSuccessAction = payload => ({
  type: USER_LOGOUT_SUCCEEDED,
  payload,
});

export default userLogoutSuccessAction;
