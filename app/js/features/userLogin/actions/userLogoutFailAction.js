export const USER_LOGOUT_FAILED = 'USER_LOGOUT_FAILED';

const userLogoutFailAction = payload => ({
  type: USER_LOGOUT_FAILED,
  payload,
});

export default userLogoutFailAction;
