export const USER_LOGIN_ACTION = 'USER_LOGIN_ACTION';

const userLoginAction = (url, payload) => ({
  type: USER_LOGIN_ACTION,
  url,
  payload,
});

export default userLoginAction;
