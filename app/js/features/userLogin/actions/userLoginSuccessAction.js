export const USER_LOGIN_SUCCEEDED = 'USER_LOGIN_SUCCEEDED';

const userLoginSuccessAction = payload => ({
  type: USER_LOGIN_SUCCEEDED,
  payload,
});

export default userLoginSuccessAction;
