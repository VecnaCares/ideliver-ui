import Avatar from '@material-ui/core/Avatar';
import CircularProgress from '@material-ui/core/CircularProgress';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Modal from '@material-ui/core/Modal';
import Paper from '@material-ui/core/Paper';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import Typography from '@material-ui/core/Typography';
import { get } from 'lodash';
import { Redirect } from 'react-router-dom';
import { injectIntl } from 'react-intl';

import VcButton from '../../components/vcButton/vcButton';
import VcDropDown from '../../components/vcDropDown/vcDropDown';
import VcGridRow from '../../components/vcGrid/vcGridRow/vcGridRow';
import { VcNotification } from '@vecnacares/vc-ui';
import VcTextField from '../../components/vcTextField/vcTextField';
import messages from '../../intl/messages';
import styles from './userLogin.scss';

const UserLogin = props => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [location, setLocation] = useState('');
  const [showValidation, setShowValidation] = useState(false);

  const { formatMessage } = props.intl;
  const {
    submitting,
    onSubmit,
    isAuthenticated,
    loginErrors,
    fetchLocations,
    loginLocations,
  } = props;
  const showLoginError = loginErrors && loginErrors.length > 0;

  useEffect(() => {
    fetchLocations({ tag: 'Login Location' });
  }, []);

  const hasRequiredFields = () => username && password && location;

  const handleSubmit = () => {
    if (hasRequiredFields()) {
      setShowValidation(false);
      onSubmit({ username, password, location });
    } else {
      setShowValidation(true);
    }
  };

  if (isAuthenticated) {
    const from = get(props, ['location', 'state', 'from'], '/');
    return <Redirect to={from} />;
  }

  return (
    <div className={styles.container}>
      <VcNotification
        key="login-notification"
        message={
          showLoginError ? formatMessage(messages[loginErrors.join()]) : null
        }
        variant="error"
        open={showLoginError}
        position={{ vertical: 'top', horizontal: 'center' }}
      />
      <Paper elevation={24} className={styles.paper}>
        <Modal
          open={submitting}
          BackdropProps={{ invisible: true }}
          disableAutoFocus
        >
          <CircularProgress size={50} className={styles.progress} />
        </Modal>
        <form autoComplete="off" className={styles.form}>
          <div className={styles.loginHeader}>
            <Avatar className={styles.avatar}>
              <img className={styles.appLogo} src="img/ideliver_logo.png" />
            </Avatar>
            <Typography variant="h5" gutterBottom>
              {formatMessage(messages.login)}
            </Typography>
          </div>
          <div className={styles.section}>
            <VcGridRow>
              <VcTextField
                datatest={formatMessage(messages.dataTestUsername)}
                label={formatMessage(messages.username)}
                vType="text"
                className={styles.fullSizeField}
                required
                error={!username && showValidation}
                onChange={value => setUsername(value)}
                value={username}
              />
            </VcGridRow>
            <VcGridRow>
              <VcTextField
                datatest={formatMessage(messages.dataTestPassword)}
                label={formatMessage(messages.password)}
                vType="password"
                className={styles.fullSizeField}
                required
                error={!password && showValidation}
                onChange={value => setPassword(value)}
                value={password}
              />
            </VcGridRow>
            <VcGridRow>
              <VcDropDown
                datatest={formatMessage(messages.dataTestLocation)}
                label={formatMessage(messages.location)}
                value={location}
                required
                error={!location && showValidation}
                className={styles.dropdown}
                options={loginLocations}
                onChange={value => setLocation(value)}
              />
            </VcGridRow>
          </div>
          <VcGridRow className={styles.submitButtonContainer}>
            <VcButton
              datatest={formatMessage(messages.dataTestLoginButton)}
              color="primary"
              onMouseEnter={() => {}}
              onClick={handleSubmit}
              value={formatMessage(messages.login)}
              disabled={submitting}
            />
          </VcGridRow>
        </form>
      </Paper>
    </div>
  );
};

UserLogin.propTypes = {
  /** Internationalization component */
  intl: PropTypes.shape({}).isRequired,
  /** Indicates that form is being submitted */
  submitting: PropTypes.bool,
  /** Indicates that a user is authenticated */
  isAuthenticated: PropTypes.bool,
  /** Function to call on login form submit */
  onSubmit: PropTypes.func.isRequired,
  /** Login errors */
  loginErrors: PropTypes.arrayOf(PropTypes.string),
  /** Fetch Locations function */
  fetchLocations: PropTypes.func.isRequired,
  loginLocations: PropTypes.arrayOf(
    PropTypes.shape({
      display: PropTypes.string,
      value: PropTypes.string,
    })
  ),
};

UserLogin.defaultProps = {
  submitting: false,
  isAuthenticated: false,
  loginErrors: [],
  loginLocations: [],
};

export default injectIntl(UserLogin);
