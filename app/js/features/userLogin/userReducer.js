import { fromJS } from 'immutable';
import { USER_LOGIN_SUCCEEDED } from './actions/userLoginSuccessAction';
import { USER_LOGIN_FAILED } from './actions/userLoginFailAction';
import { USER_LOGIN_ACTION } from './actions/userLoginAction';
import { FETCH_SESSION_SUCCEEDED } from '../main/actions/fetchSessionSuccessAction';
import { FETCH_SESSION_FAILED } from '../main/actions/fetchSessionFailAction';
import { USER_LOGOUT_SUCCEEDED } from './actions/userLogoutSuccessAction';
import { USER_LOGOUT_FAILED } from './actions/userLogoutFailAction';

export const defaultState = {
  session: null,
  isAuthenticated: false,
  submitting: false,
  errors: [],
};

const userReducer = (state = fromJS(defaultState), action) => {
  switch (action.type) {
    case USER_LOGIN_SUCCEEDED:
      return fromJS(action.payload)
        .setIn(['submitting'], false)
        .setIn(['isAuthenticated'], true)
        .setIn(['errors'], fromJS([]));

    case USER_LOGIN_FAILED:
      return state
        .setIn(['isAuthenticated'], false)
        .setIn(['submitting'], false)
        .setIn(['errors'], fromJS([action.payload]));

    case USER_LOGIN_ACTION:
      return state.setIn(['submitting'], true);

    case USER_LOGOUT_SUCCEEDED:
      return state.setIn(['session'], null).setIn(['isAuthenticated'], false);

    case USER_LOGOUT_FAILED:
      return state
        .setIn(['isAuthenticated'], true)
        .setIn(['errors'], fromJS([action.payload]));

    case FETCH_SESSION_SUCCEEDED:
      return state
        .setIn(['session'], fromJS(action.payload))
        .setIn(['isAuthenticated'], true);

    case FETCH_SESSION_FAILED:
      return state
        .setIn(['isAuthenticated'], false)
        .setIn(['errors'], fromJS([action.payload]));

    default:
      return state;
  }
};

export default userReducer;
