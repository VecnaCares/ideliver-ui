import React from 'react';
import Proptypes from 'prop-types';
import { get } from 'lodash';
import { OrderedMap } from 'immutable';
import { injectIntl, FormattedTime, FormattedDate } from 'react-intl';
import { Typography } from '@material-ui/core';
import moment from 'moment';
import Paper from '@material-ui/core/Paper';
import AppBar from '@material-ui/core/AppBar';
import Divider from '@material-ui/core/Divider';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import VcButton from '../../components/vcButton/vcButton';
import VcFormContainer from '../vcForm/vcFormContainer';
import messages from '../../intl/messages';
import styles from './activeLabour.scss';
import VcGridRow from '../../components/vcGrid/vcGridRow/vcGridRow';
import VcGridColumn from '../../components/vcGrid/vcGridColumn/vcGridColumn';
import VcChart, { chartTypes } from '../../components/vcChart/vcChart';
import { formField, concept } from '../../uuid';
import VcTable from '../../components/vcTable/vcTable';
import Table from '@material-ui/core/Table';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import { sortArrayOnLatestEncounterDate } from './activeLabourHelpers';
import { defaultFormNameSet } from './../vcForm/vcFormHelpers';

let isFormSaved;
class ActiveLabour extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      formDisplayed: false,
      formSelected: false,
      multipleReadingFormDisplayed: false,
      newDataArray: [],
    };
  }
  changeIsFormSaved = () => {
    isFormSaved = true;
  };
  handleAddReading = (index, encounterUuid) => {
    const selectedIndex = index !== this.state.formDisplayed ? index : false;
    const findIndexId = encounterUuid
      ? this.props.metaData.findIndex(
          tabEncounter => tabEncounter.encounterUuid === encounterUuid
        )
      : undefined;
    const finalSelectedIndex =
      encounterUuid && findIndexId !== this.state.formDisplayed
        ? findIndexId
        : false;
    const finalFormDisplayedIndex =
      encounterUuid === undefined && index !== this.state.formDisplayed
        ? index
        : finalSelectedIndex;
    this.setState(prevState => ({
      formDisplayed: finalFormDisplayedIndex,
      multipleReadingFormDisplayed: false,
      formSelected: selectedIndex,
    }));
    this.props.selectForm(this.props.uuid, finalFormDisplayedIndex);
  };

  handleAddMultipleReading = (index, encounterUuid) => {
    this.state.newDataArray = [];
    isFormSaved = true;
    if (this.props.data) {
      for (let i = index; i < index + 6; i++) {
        this.state.newDataArray[i] = this.props.data[i];
      }
    }
    const selectedIndex =
      index !== this.state.multipleReadingFormDisplayed ? index : false;
    const findIndexId = encounterUuid
      ? this.props.metaData.findIndex(
          tabEncounter => tabEncounter.encounterUuid === encounterUuid
        )
      : undefined;
    const finalSelectedIndex =
      encounterUuid && findIndexId !== this.state.multipleReadingFormDisplayed
        ? findIndexId
        : false;
    const finalFormDisplayedIndex =
      encounterUuid === undefined &&
      index !== this.state.multipleReadingFormDisplayed
        ? index
        : finalSelectedIndex;
    this.setState(prevState => ({
      formDisplayed: false,
      multipleReadingFormDisplayed: finalFormDisplayedIndex,
      formSelected: selectedIndex,
    }));

    this.props.selectForm(this.props.uuid, finalFormDisplayedIndex);
  };

  handleMouseEnter = event => {
    event.currentTarget.focus();
  };

  render() {
    const formFieldsMap = this.props.formMetaData
      ? Object.assign([], this.props.formMetaData.formFields)
          .sort((a, b) => a.fieldNumber - b.fieldNumber)
          .reduce((sum, formField) => {
            if (!formField.parent) {
              sum = sum.set(formField.uuid, formField);
            } else if (sum.get(formField.parent.uuid).children) {
              sum.get(formField.parent.uuid).children.push(formField);
            } else {
              sum.get(formField.parent.uuid).children = [formField];
            }
            return sum;
          }, OrderedMap())
      : undefined;

    const sortedFormData =
      this.props.data && this.props.data.length > 1
        ? sortArrayOnLatestEncounterDate(this.props.data)
        : this.props.data;
    const { formatMessage } = this.props.intl;
    const time = formatMessage(messages.time);
    const fetalHeartRate = formatMessage(messages.fetalHeartRate);
    const liquor = formatMessage(messages.liquor);
    const cervix = formatMessage(messages.cervix);
    const descent = formatMessage(messages.descent);
    const moulding = formatMessage(messages.moulding);
    const progression = formatMessage(messages.progression);
    const contractions = formatMessage(messages.contractions);
    const interventions = formatMessage(messages.interventions);
    const drugName = formatMessage(messages.drugName);
    const drugUnit = formatMessage(messages.drugUnit);
    const drugDosage = formatMessage(messages.drugDosage);
    const contractionsStrength = formatMessage(messages.contractionsStrength);
    const vitals = formatMessage(messages.vitals);
    const pulse = formatMessage(messages.pulse);
    const temperature = formatMessage(messages.temperature);
    const urineVolume = formatMessage(messages.urineVolume);
    const systolicBloodPressure = formatMessage(messages.systolicBloodPressure);
    const diastolicBloodPressure = formatMessage(
      messages.diastolicBloodPressure
    );
    const descentUnits = formatMessage(messages.descentUnits);
    const value = sortedFormData
      ? Object.values(sortedFormData)
          .map(item => ({
            [time]: +moment(item.encounterDatetime).format('x'),
            [fetalHeartRate]: get(item, [
              formField.AL_FETAL_HEART_RATE_UUID,
              'value',
            ]),
            [liquor]: get(
              item,
              [formField.AL_LIQUOR_COLOR_UUID, 'value', 'display'],
              ''
            ).charAt(0),
            [moulding]: get(item, [
              formField.AL_DEGREES_OF_MOULDING_UUID,
              'value',
              'display',
            ]),
            [cervix]: get(item, [formField.AL_CERVIX_UUID, 'value', 'display'])
              ? parseInt(
                  get(item, [formField.AL_CERVIX_UUID, 'value', 'display'])
                )
              : get(item, [formField.AL_CERVIX_UUID, 'value']),
            [descent]: get(item, [
              formField.AL_DESCENT_UUID,
              'value',
              'display',
            ])
              ? parseInt(
                  get(item, [formField.AL_DESCENT_UUID, 'value', 'display'])
                )
              : get(item, [formField.AL_DESCENT_UUID, 'value']),
            [descentUnits]: get(item, [
              formField.AL_DESCENT_UNIT_UUID,
              'value',
              'display',
            ]),
            [contractions]: get(item, [
              formField.AL_CONTRACTIONS_UUID,
              'value',
            ]),
            [contractionsStrength]: get(item, [
              formField.AL_CONTRACTIONS_STRENGTH_UUID,
              'value',
              'display',
            ]),
            [interventions]: get(
              item,
              [formField.AL_INTERVENTIONS_UUID, 'value'],
              []
            ).map(intervention => {
              const result = {
                [time]: moment(item.encounterDatetime).format(
                  'DD/MM/YY hh:mm A'
                ),
              };
              intervention.groupMembers.forEach(member => {
                if (member.concept === concept.DRUG_NAME) {
                  result[drugName] = member.value;
                }
                if (member.concept === concept.DRUG_UNIT) {
                  result[drugUnit] = member.value;
                }
                if (member.concept === concept.DRUG_DOSAGE) {
                  result[drugDosage] = member.value;
                }
              });
              return result;
            }),
            [pulse]: get(item, [formField.AL_PULSE_UUID, 'value']),
            [temperature]: get(item, [formField.AL_TEMPERATURE_UUID, 'value']),
            [urineVolume]: get(item, [formField.AL_URINE_VOLUME_UUID, 'value']),
            [systolicBloodPressure]: get(item, [
              formField.AL_Systolic_Blood_Pressure_UUID,
              'value',
            ]),
            [diastolicBloodPressure]: get(item, [
              formField.AL_Diastolic_Blood_Pressure_UUID,
              'value',
            ]),
            const: 1,
          }))
          .reverse()
      : undefined;

    //Change the value for Contractions graph
    // if Contractions Strength & Contractions any one have undefined value.

    value && value.length > 0
      ? value.map(member => {
          member['Contractions Strength'] != undefined &&
          member.Contractions != undefined
            ? value
            : member['Contractions Strength'] != undefined
            ? (member.Contractions = undefined)
            : (member.Contractions = undefined);
        })
      : undefined;

    const tabs =
      sortedFormData && sortedFormData.length
        ? Object.values(sortedFormData)
            .reverse()
            .map((item, index) => (
              <Tab
                key={item.encounterUuid}
                type="primary"
                onClick={() =>
                  this.handleAddReading(
                    sortedFormData.length - 1 - index,
                    item.encounterUuid
                  )
                }
                label={
                  item.encounterDatetime
                    ? [
                        <FormattedTime
                          value={item.encounterDatetime}
                          key="time"
                        />,
                        '   ',
                        <FormattedDate
                          value={item.encounterDatetime}
                          key="date"
                          month="2-digit"
                          day="2-digit"
                        />,
                      ]
                    : ''
                }
              />
            ))
        : undefined;
    return (
      <Paper elevation={24} className={styles.paper}>
        <div className={styles.buttonContainer}>
          <VcButton
            datatest={'add-multiple-reading-btn'}
            color="secondary"
            className={styles.addMultipleReadingButtonStyle}
            onClick={() =>
              this.handleAddMultipleReading(
                this.props.data
                  ? !this.props.data[this.props.data.length - 1].lastUpdated
                    ? this.props.data.length - 1
                    : this.props.data.length
                  : 0,
                undefined
              )
            }
            value="Edit / Add Multiple Readings"
            disabled={this.props.data === undefined}
          />
          <VcButton
            color="primary"
            className={styles.addReadingButtonStyle}
            onClick={() =>
              this.handleAddReading(
                this.props.data && 'lastUpdated' in this.props.data[0] === true
                  ? !this.props.data[this.props.data.length - 1].lastUpdated
                    ? this.props.data.length - 1
                    : this.props.data.length
                  : 0,
                undefined
              )
            }
            value={formatMessage(messages.addReading)}
            datatest={'add-reading-btn'}
          />
        </div>
        {this.props.data && this.props.data.length ? (
          <VcGridRow className={styles.buttonContainer}>
            <div className={styles.tabsscroll}>
              <AppBar position="static" color="default">
                <Tabs
                  classes={{ root: styles.tabs }}
                  value={
                    this.state.formSelected === false
                      ? false
                      : this.props.data.length - 1 - this.state.formSelected
                  }
                  onChange={this.handleChange}
                  variant="scrollable"
                  scrollButtons="on"
                  indicatorColor="primary"
                  textColor="primary"
                >
                  {tabs}
                </Tabs>
              </AppBar>
            </div>
          </VcGridRow>
        ) : null}
        {this.state.multipleReadingFormDisplayed ||
        this.state.multipleReadingFormDisplayed === 0 ? (
          <>
            <VcGridRow className={styles.header} dataTest="ideliver-form-label">
              <Typography
                className={styles.title}
                variant="h5"
                datatest={`${
                  this.props.formMetaData
                    ? defaultFormNameSet(this.props.formMetaData, formatMessage)
                    : null
                }-form-label`}
              >
                {this.props.formMetaData
                  ? defaultFormNameSet(this.props.formMetaData, formatMessage)
                  : null}
              </Typography>
            </VcGridRow>
            <div className={styles.root}>
              <Table>
                <TableRow>
                  <TableCell className={styles.outerCell}>
                    <Table>
                      <TableRow>
                        <TableCell className={styles.sideLabelStyle}>
                          <div className={styles.sideLabelFont}>Time</div>
                        </TableCell>
                      </TableRow>
                      <hr className={styles.hrStyle}></hr>
                      {formFieldsMap &&
                        Object.values(formFieldsMap.toJS()).map(
                          (formField, i) => {
                            return (
                              <>
                                <TableRow>
                                  <TableCell className={styles.sideLabelStyle}>
                                    <div className={styles.sideLabelFont}>
                                      {formField.field.name}
                                    </div>
                                  </TableCell>
                                </TableRow>
                                <hr className={styles.hrStyle}></hr>
                              </>
                            );
                          }
                        )}
                    </Table>
                  </TableCell>
                  {this.state.newDataArray &&
                    this.state.newDataArray.length > 0 &&
                    this.state.newDataArray.map((item, i) => {
                      return (
                        <>
                          <VcFormContainer
                            activeLabourMethod={this.changeIsFormSaved}
                            className={styles.forms}
                            location={this.props.location}
                            uuid={this.props.uuid}
                            index={i}
                            isMultiReadingForm={true}
                          />
                        </>
                      );
                    })}
                </TableRow>
              </Table>
            </div>

            <VcGridRow className={styles.retroButtonSectionGrid}>
              {isFormSaved === true ? (
                <div className={styles.saveButtonStyle}>
                  <VcButton
                    datatest="save-btn"
                    color="primary"
                    onMouseEnter={this.handleMouseEnter}
                    onClick={() => {
                      this.props.onSubmit(
                        this.props.formMetaData.encounterType.uuid,
                        this.props.data,
                        this.state.multipleReadingFormDisplayed
                      );
                      setTimeout(function() {
                        isFormSaved = false;
                      }, 1500);
                    }}
                    value="Save"
                    disabled={this.props.submittingForm}
                  />
                </div>
              ) : (
                <div className={styles.saveButtonStyle}>
                  <img
                    src="img/check-mark-24.png"
                    className={styles.checkImgStyle}
                  />
                  <VcButton
                    className={styles.saveButton}
                    datatest="saved-btn"
                    color="primary"
                    onMouseEnter={this.handleMouseEnter}
                    value="Saved"
                  />
                </div>
              )}

              <div>
                <VcButton
                  datatest="show-partograph-btn"
                  className={styles.savePartographStyle}
                  color="primary"
                  onMouseEnter={this.handleMouseEnter}
                  onClick={() => {
                    this.props.onSubmit(
                      this.props.formMetaData.encounterType.uuid,
                      this.props.data,
                      this.state.multipleReadingFormDisplayed
                    );
                    setTimeout(function() {
                      isFormSaved = false;
                    }, 1500);

                    this.handleAddReading(false);
                  }}
                  value="Show Partograph >"
                />
              </div>
            </VcGridRow>
          </>
        ) : null}
        {this.state.formDisplayed || this.state.formDisplayed === 0 ? (
          <VcFormContainer
            className={styles.forms}
            location={this.props.location}
            uuid={this.props.uuid}
            index={this.state.formDisplayed}
            onSubmit={() => this.handleAddReading(false)}
          />
        ) : this.props.data &&
          this.props.data.length &&
          !this.state.multipleReadingFormDisplayed ? (
          <VcGridColumn>
            {/* Fetal Heart Rate */}
            <div className={styles.chart}>
              <Typography className={styles.label} variant="h3">
                {fetalHeartRate}
              </Typography>
              <VcChart
                type={chartTypes.FETAL_HEART_RATE}
                syncId="anyId"
                width="100%"
                height={500}
                xDataKey={[fetalHeartRate]}
                yDataKey={time}
                xTickFormatter={(value, type) =>
                  !type || type === 'time'
                    ? moment(value).format('DD/MM/YY hh:mm A')
                    : value
                }
                minYValue={
                  value ? +moment(value[0][time], 'x').format('x') : undefined
                }
                maxYValue={
                  value
                    ? +moment(value[value.length - 1][time], 'x').format('x')
                    : undefined
                }
                topLabelDataKey={liquor}
                bottomLabelDataKey={moulding}
                topLabelColors={{
                  C: 'mediumaquamarine',
                  L: 'lightsteelblue',
                  D: 'burlywood',
                  B: 'lightpink',
                }}
                bottomLabelColors={{
                  0: 'mediumaquamarine',
                  '+': 'lightsteelblue',
                  '++': 'burlywood',
                  '+++': 'lightpink',
                }}
                value={value}
                minAlertValue={120}
                maxAlertValue={160}
              />
            </div>
            <Divider />

            {/* Progression */}
            <div className={styles.chart}>
              <Typography className={styles.label} variant="h3">
                {progression}
              </Typography>
              <VcChart
                syncId="anyId"
                width="100%"
                height={500}
                xDataKey={[cervix, descent]}
                xDataColor={['purple', 'gray']}
                yDataKey={time}
                yAxisDomain={[0, 10]}
                xTickFormatter={(value, type) =>
                  !type || type === 'time'
                    ? moment(value).format('DD/MM/YY hh:mm A')
                    : value
                }
                minYValue={
                  value ? +moment(value[0][time], 'x').format('x') : undefined
                }
                maxYValue={
                  value
                    ? +moment(value[value.length - 1][time], 'x').format('x')
                    : undefined
                }
                value={value}
              />
            </div>
            <Divider />

            {/* Contractions */}
            <div className={styles.chart}>
              <Typography className={styles.label} variant="h3">
                {contractions}
              </Typography>
              <VcChart
                type={chartTypes.CONTRACTIONS}
                syncId="anyId"
                width="100%"
                height={500}
                xDataKey={[contractions]}
                xDataColor={['black']}
                yDataKey={time}
                yAxisDomain={[0, 10]}
                xTickFormatter={(value, type) =>
                  !type || type === 'time'
                    ? moment(value).format('DD/MM/YY hh:mm A')
                    : value
                }
                minYValue={
                  value ? +moment(value[0][time], 'x').format('x') : undefined
                }
                maxYValue={
                  value
                    ? +moment(value[value.length - 1][time], 'x').format('x')
                    : undefined
                }
                value={value}
              />
            </div>
            <Divider />

            {/* Interventions */}
            <Typography className={styles.label} variant="h3">
              {interventions}
            </Typography>
            <div className={styles.table}>
              <VcTable
                withPaging={false}
                data={
                  value
                    ? value
                        .reduce((filtered, option) => {
                          if (option[interventions]) {
                            option[interventions].forEach(item => {
                              filtered.push(item);
                              filtered[
                                filtered.indexOf(item)
                              ].id = filtered.indexOf(item);
                            });
                          }
                          return filtered;
                        }, [])
                        .reverse()
                    : undefined
                }
                columnData={[
                  {
                    id: time,
                    sortable: false,
                    numeric: true,
                    disablePadding: false,
                    label: time,
                  },
                  {
                    id: drugDosage,
                    sortable: false,
                    numeric: true,
                    disablePadding: false,
                    label: drugDosage,
                  },
                  {
                    id: drugName,
                    sortable: false,
                    numeric: true,
                    disablePadding: false,
                    label: drugName,
                  },
                  {
                    id: drugUnit,
                    sortable: false,
                    numeric: false,
                    disablePadding: false,
                    label: drugUnit,
                  },
                ]}
              />
            </div>
            <Divider />

            {/* Vitals */}
            <div className={styles.chart}>
              <Typography className={styles.label} variant="h3">
                {vitals}
              </Typography>
              {/* Pulse */}
              <VcChart
                type={chartTypes.VITALS}
                syncId="anyId"
                width="100%"
                height={500}
                xDataKey={[pulse]}
                xDataColor={['black']}
                yDataKey={time}
                xTickFormatter={(value, type) =>
                  !type || type === 'time'
                    ? moment(value).format('DD/MM/YY hh:mm A')
                    : value
                }
                minYValue={
                  value ? +moment(value[0][time], 'x').format('x') : undefined
                }
                maxYValue={
                  value
                    ? +moment(value[value.length - 1][time], 'x').format('x')
                    : undefined
                }
                value={value}
              />
            </div>
            <Divider />

            {/* Temperature */}
            <Typography className={styles.labelVitals} variant="h5">
              {temperature}
            </Typography>
            <VcChart
              noXTicks
              type={chartTypes.BUBBLE_CHART}
              syncId="anyId"
              width="100%"
              height={500}
              xDataKey={[temperature]}
              xDataColor={['red']}
              yDataKey={time}
              xTickFormatter={(value, type) =>
                !type || type === 'time'
                  ? moment(value).format('DD/MM/YY hh:mm A')
                  : value
              }
              minValue={0}
              maxValue={40}
              minYValue={
                value ? +moment(value[0][time], 'x').format('x') : undefined
              }
              maxYValue={
                value
                  ? +moment(value[value.length - 1][time], 'x').format('x')
                  : undefined
              }
              value={value}
            />
            <Divider />

            {/* Urine volume */}
            <Typography className={styles.labelVitals} variant="h5">
              {urineVolume}
            </Typography>
            <VcChart
              type={chartTypes.BUBBLE_CHART}
              syncId="anyId"
              width="100%"
              height={500}
              xDataKey={[urineVolume]}
              xDataColor={['red']}
              yDataKey={time}
              xTickFormatter={(value, type) =>
                !type || type === 'time'
                  ? moment(value).format('DD/MM/YY hh:mm A')
                  : value
              }
              minValue={0}
              maxValue={40}
              minYValue={
                value ? +moment(value[0][time], 'x').format('x') : undefined
              }
              maxYValue={
                value
                  ? +moment(value[value.length - 1][time], 'x').format('x')
                  : undefined
              }
              value={value}
            />

            <Divider />

            {/* systolicBloodPressure */}
            <Typography className={styles.labelVitals} variant="h5">
              {systolicBloodPressure}
            </Typography>
            <VcChart
              type={chartTypes.BUBBLE_CHART}
              syncId="anyId"
              width="100%"
              height={500}
              xDataKey={[systolicBloodPressure]}
              xDataColor={['red']}
              yDataKey={time}
              xTickFormatter={(value, type) =>
                !type || type === 'time'
                  ? moment(value).format('DD/MM/YY hh:mm A')
                  : value
              }
              minValue={0}
              maxValue={240}
              minYValue={
                value ? +moment(value[0][time], 'x').format('x') : undefined
              }
              maxYValue={
                value
                  ? +moment(value[value.length - 1][time], 'x').format('x')
                  : undefined
              }
              value={value}
            />

            <Divider />

            {/* diastolicBloodPressure */}
            <Typography className={styles.labelVitals} variant="h5">
              {diastolicBloodPressure}
            </Typography>
            <VcChart
              type={chartTypes.BUBBLE_CHART}
              syncId="anyId"
              width="100%"
              height={500}
              xDataKey={[diastolicBloodPressure]}
              xDataColor={['red']}
              yDataKey={time}
              xTickFormatter={(value, type) =>
                !type || type === 'time'
                  ? moment(value).format('DD/MM/YY hh:mm A')
                  : value
              }
              minValue={0}
              maxValue={140}
              minYValue={
                value ? +moment(value[0][time], 'x').format('x') : undefined
              }
              maxYValue={
                value
                  ? +moment(value[value.length - 1][time], 'x').format('x')
                  : undefined
              }
              value={value}
            />
          </VcGridColumn>
        ) : null}
      </Paper>
    );
  }
}

ActiveLabour.propTypes = {};

ActiveLabour.defaultProps = {};

export default injectIntl(ActiveLabour, { withRef: true });
