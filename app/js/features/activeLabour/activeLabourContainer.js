import { connect } from 'react-redux';
import withImmutablePropsToJS from 'with-immutable-props-to-js';
import ActiveLabour from './activeLabour';
import {
  getAllSelectedFormData,
  getSelectedFormMetaData,
  getUuidsFromUrl,
  getSelectedFormIndex,
  isSubmitting,
} from '../../state/ui/form/selectors';
import {
  selectFormAction,
  submitFormAction,
} from '../../state/ui/form/actions';
import { REST_API_PATHNAME, REST_API_VERSION } from '../../paths';
const mapStateToProps = state => {
  const formData = getAllSelectedFormData(state);
  const formMetaData = getSelectedFormMetaData(state);
  const formIndex = getSelectedFormIndex(state);
  return {
    data: formData,
    metaData: formData,
    formMetaData: formMetaData,
    formIndex,
    submittingForm: isSubmitting(state),
  };
};

const mapDispatchToProps = (dispatch, props) => {
  const { visitId, formId, formIndex } = getUuidsFromUrl(props);
  const encounterPath = [props.uuid ? props.uuid : formId];
  return {
    onSubmit: (encounterTypeId, data, index) => {
      for (let i = index; i < data.length; i++) {
        encounterPath.push(i);
        if (formIndex !== undefined && formIndex !== null) {
          encounterPath.push(formIndex);
        }
        dispatch(
          submitFormAction(
            REST_API_PATHNAME + REST_API_VERSION,
            visitId,
            encounterPath,
            encounterTypeId
          )
        );
        if (props.onSubmit) {
          props.onSubmit(encounterPath);
        }
        encounterPath.pop();
      }
    },
    selectForm: (formId, formIndex) => {
      dispatch(selectFormAction(formId, formIndex));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withImmutablePropsToJS(ActiveLabour));
