import { getApiAuthorization } from './features/userLogin/services/userLoginService';
/**
 * The openmrs rest api uses Basic Authentication. Each request should have
 * an authorization header containing the authorization token.
 * The authorization token is a base64 encoded string of username and password.
 * @param authorizationToken
 * @returns {{'content-type': string, 'Authorization': string}}
 */
const handleResult = response => {
  return response.json().then(json => {
    if (response.ok) {
      return json;
    }
    return Promise.reject(json);
  });
};

const getHeaders = authorizationToken => {
  const headers = {
    'content-type': 'application/json',
  };

  const localStorageToken = getApiAuthorization();
  const authorization = authorizationToken || localStorageToken;
  if (authorization) {
    headers.Authorization = authorization;
  }
  return headers;
};

export const getImg = (url, authorizationToken) => {
  const headers = getHeaders(authorizationToken);
  delete headers['content-type'];
  return fetch(url, {
    method: 'GET',
    headers,
  }).then(response => {
    if (response.ok) {
      return response.blob();
    }
    throw new Error('Image not found');
  });
};

export const getData = (url, authorizationToken) =>
  fetch(url, {
    method: 'GET',
    headers: getHeaders(authorizationToken),
  }).then(handleResult);

export const postData = (url, data, authorizationToken) =>
  fetch(url, {
    body: JSON.stringify(data), // must match 'Content-Type' header
    headers: getHeaders(authorizationToken),
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
  }).then(handleResult); // parses response to JSON

export const deleteData = (url, authorizationToken) =>
  fetch(url, {
    headers: getHeaders(authorizationToken),
    method: 'DELETE',
  }).then(handleResult);
