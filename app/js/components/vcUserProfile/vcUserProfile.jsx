import AccountCircle from '@material-ui/icons/AccountCircle';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import Typography from '@material-ui/core/Typography';
import { injectIntl, FormattedMessage } from 'react-intl';
import messages from '../../intl/messages';
import styles from './vcUserProfile.scss';

const VcUserProfile = props => {
  const [anchorEl, setAnchorEl] = useState(null);
  const handleClick = event => setAnchorEl(event.currentTarget);
  const handleClose = () => setAnchorEl(null);
  const { user } = props;

  const logout = () => {
    if (props.logout) {
      props.logout();
    }
    setAnchorEl(null);
  };
  const { formatMessage } = props.intl;
  return user ? (
    <div className={styles.userProfile}>
      <Typography color="inherit" variant="inherit" inline>
        <FormattedMessage id={user.display} defaultMessage={user.display} />
      </Typography>
      <IconButton
        color="inherit"
        aria-owns={anchorEl ? 'user-menu' : undefined}
        aria-haspopup="true"
        onClick={handleClick}
      >
        <AccountCircle />
      </IconButton>
      <Menu
        id="user-menu"
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem key="profile" onClick={handleClose}>
          {formatMessage(messages.Profile)}
        </MenuItem>
        <MenuItem key="account" onClick={handleClose}>
          {formatMessage(messages.Myaccount)}
        </MenuItem>
        <MenuItem key="logout" onClick={logout}>
          {formatMessage(messages.Logout)}
        </MenuItem>
      </Menu>
    </div>
  ) : null;
};

VcUserProfile.propTypes = {
  /** The user to display */
  user: PropTypes.shape({
    display: PropTypes.string,
  }),
  /** Function called to logout */
  logout: PropTypes.func,
};

VcUserProfile.defaultProps = {
  user: null,
  logout: () => {},
};

export default injectIntl(VcUserProfile);
