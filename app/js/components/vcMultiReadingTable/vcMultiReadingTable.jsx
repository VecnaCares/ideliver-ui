import React from 'react';
import { fromJS } from 'immutable';
import { injectIntl } from 'react-intl';
import styles from './vcMultiReadingTable.scss';
import cx from 'classnames';
import moment from 'moment';
import Table from '@material-ui/core/Table';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import { get } from 'lodash';
import VcGenericSwitch from '../../components/vcGenericSwitch/vcGenericSwitch';
import VcDropDown from '../../components/vcDropDown/vcDropDown';
import VcFieldsSet from '../../components/vcFieldsSet/vcFieldsSet';
import VcSelectOptions from '../../components/vcSelectOptions/vcSelectOptions';
import VcTextField from '../../components/vcTextField/vcTextField';
import { concept } from '../../uuid';
import { shouldFieldBeShown } from '../../features/vcForm/vcFormHelpers';
import VcDateTime, {
  longDatetimeFormatWithT,
} from '../../components/vcDateTime/vcDateTime';

const VcMultiReadingTable = props => {
  const fieldComponents = {
    VcTextField,
    VcDateTime,
    VcDropDown,
    VcGenericSwitch,
    VcSelectOptions,
    VcFieldsSet,
  };
  const fieldToFormFieldMap = get(
    props.allData,
    ['metadata', 'fieldToFormFields'],
    {}
  );
  const getStyle = (name, compProps) =>
    cx(compProps.className, {
      [styles.textField]:
        name === 'VcTextField' && compProps && compProps.multiline !== true,
      [styles.dateTime]: name === 'VcDateTime',
      [styles.dropDown]: name === 'VcDropDown',
      [styles.genericSwitch]: name === 'VcGenericSwitch',
      [styles.fieldsSet]: name === 'VcFieldsSet',
    });
  const createFields = () => {
    const newformField =
      props.formFieldsMap != undefined ? props.formFieldsMap : null;

    if (newformField != null) {
      return Object.values(fromJS(newformField).toJS()).map((formField, i) => {
        const componentConfig = JSON.parse(formField.field.description);
        if (!componentConfig) {
          return null;
        }

        const Component = fieldComponents[componentConfig.name];
        const answersMap = {};
        if (formField.field.concept) {
          if (formField.field.concept.answers) {
            formField.field.concept.answers.forEach(ans => {
              answersMap[ans.display] = {
                uuid: ans.uuid,
                display: ans.display,
              };
            });
          } else if (
            formField.field.concept.set &&
            formField.field.concept.setMembers
          ) {
            formField.field.concept.setMembers.forEach(ans => {
              answersMap[ans.display] = {
                uuid: ans.uuid,
                display: ans.display,
              };
            });
          }
        }
        return props.hiddenFields &&
          props.hiddenFields.has(formField.field.uuid) ? null : (
          <>
            <TableRow>
              <TableCell className={styles.innerCell}>
                <Component
                  datatest={props.allData.datatest}
                  error={
                    get(
                      props.allData,
                      ['data', 'invalidFormFields'],
                      []
                    ).indexOf(formField.uuid) > -1
                  }
                  onToggle={() => handleToggle(formField.uuid)}
                  disabled={
                    JSON.parse(
                      get(
                        props.allData.data[props.allData.index],
                        [concept.FORM_METADATA, 'value'],
                        '{"disabledFormFields":[]}'
                      )
                    ).disabledFormFields.indexOf(formField.uuid) > -1
                  }
                  value={
                    props.allData.data[props.allData.index] &&
                    props.allData.data[props.allData.index][formField.uuid]
                      ? props.allData.data[props.allData.index][formField.uuid]
                          .value !== undefined
                        ? props.allData.data[props.allData.index][
                            formField.uuid
                          ].value !== null &&
                          props.allData.data[props.allData.index][
                            formField.uuid
                          ].value.display
                          ? props.allData.data[props.allData.index][
                              formField.uuid
                            ].value.display
                          : props.allData.data[props.allData.index][
                              formField.uuid
                            ].value
                        : Array.isArray(
                            props.allData.data[props.allData.index][
                              formField.uuid
                            ]
                          )
                        ? props.allData.data[props.allData.index][
                            formField.uuid
                          ].map(i => i.value)
                        : props.allData.data[props.allData.index][
                            formField.uuid
                          ]
                      : undefined
                  }
                  timestamp={
                    props.allData.data[props.allData.index] &&
                    props.allData.data[props.allData.index][formField.uuid]
                      ? props.allData.data[props.allData.index][formField.uuid]
                          .obsDatetime
                      : undefined
                  }
                  onChange={value => {
                    props.vcFormMethod ? props.vcFormMethod() : '';
                    if (Array.isArray(value)) {
                      const compData = [];
                      value.forEach(val => {
                        compData.push(
                          answersMap[val.value] ? answersMap[val.value] : val
                        );
                      });
                      props.allData.onChange(formField.uuid, {
                        value: compData,
                        obsDatetime: moment().format(longDatetimeFormatWithT),
                        comment: get(
                          props.allData,
                          ['data', formField.uuid, 'comment'],
                          undefined
                        ),
                      });
                    } else {
                      const compData = answersMap[value]
                        ? answersMap[value]
                        : value;
                      props.allData.onChange(
                        formField.uuid,
                        {
                          value: compData,
                          obsDatetime: moment().format(longDatetimeFormatWithT),
                          comment: get(
                            props.allData,
                            ['data', formField.uuid, 'comment'],
                            undefined
                          ),
                        },
                        new Set([
                          ...props.hiddenFields,
                          ...props.getExtraFieldsToHide(
                            formField.field.uuid,
                            props.fieldsHideMap,
                            answersMap[value] ? answersMap[value] : value
                          ),
                        ])
                      );
                    }
                  }}
                  className={getStyle(
                    componentConfig.name,
                    componentConfig.props
                  )}
                  {...componentConfig.props}
                  label=""
                  required={
                    props.allData.metadata.requiredFormFields
                      ? !!props.allData.metadata.requiredFormFields[
                          formField.uuid
                        ]
                      : false
                  }
                  fieldConcept={
                    formField.field.concept
                      ? formField.field.concept.uuid
                      : undefined
                  }
                  options={
                    formField.field.concept
                      ? formField.field.concept.setMembers &&
                        formField.field.concept.setMembers.length > 0
                        ? formField.field.concept.setMembers
                        : formField.field.concept.answers
                      : undefined
                  }
                  defaultDate={componentConfig.props.defaultDate ? true : false}
                >
                  {formField.children
                    ? formField.children.map(childFF => {
                        const childConfig = JSON.parse(
                          childFF.field.description
                        );
                        const ChildComponent =
                          fieldComponents[childConfig.name];
                        const childAnswersMap = {};
                        if (childFF.field.concept) {
                          if (childFF.field.concept.answers) {
                            childFF.field.concept.answers.forEach(ans => {
                              childAnswersMap[ans.display] = {
                                uuid: ans.uuid,
                                display: ans.display,
                              };
                            });
                          } else if (
                            childFF.field.concept.set &&
                            childFF.field.concept.setMembers
                          ) {
                            childFF.field.concept.setMembers.forEach(ans => {
                              childAnswersMap[ans.display] = {
                                uuid: ans.uuid,
                                display: ans.display,
                              };
                            });
                          }
                        }
                        if (
                          !shouldFieldBeShown(
                            childFF,
                            fieldToFormFieldMap,
                            props.allData.data[props.allData.index]
                          )
                        ) {
                          return undefined;
                        }
                        return (
                          <div className={styles.childComponent}>
                            <TableRow>
                              <TableCell className={styles.childCell}>
                                <ChildComponent
                                  datatest={props.allData.datatest}
                                  error={
                                    get(
                                      props.allData,
                                      ['data', 'invalidFormFields'],
                                      []
                                    ).indexOf(childFF.uuid) > -1
                                  }
                                  onToggle={() => handleToggle(childFF.uuid)}
                                  disabled={
                                    JSON.parse(
                                      get(
                                        props.allData.data[props.allData.index],
                                        [concept.FORM_METADATA, 'value'],
                                        '{"disabledFormFields":[]}'
                                      )
                                    ).disabledFormFields.indexOf(childFF.uuid) >
                                    -1
                                  }
                                  value={
                                    props.allData.data[props.allData.index] &&
                                    props.allData.data[props.allData.index][
                                      childFF.uuid
                                    ]
                                      ? props.allData.data[props.allData.index][
                                          childFF.uuid
                                        ].value !== undefined
                                        ? props.allData.data[
                                            props.allData.index
                                          ][childFF.uuid].value !== null &&
                                          props.allData.data[
                                            props.allData.index
                                          ][childFF.uuid].value.display
                                          ? props.allData.data[
                                              props.allData.index
                                            ][childFF.uuid].value.display
                                          : props.allData.data[
                                              props.allData.index
                                            ][childFF.uuid].value
                                        : Array.isArray(
                                            props.allData.data[
                                              props.allData.index
                                            ][childFF.uuid]
                                          )
                                        ? props.allData.data[
                                            props.allData.index
                                          ][childFF.uuid].map(i => i.value)
                                        : props.allData.data[
                                            props.allData.index
                                          ][childFF.uuid]
                                      : undefined
                                  }
                                  timestamp={
                                    props.allData.data[props.allData.index] &&
                                    props.allData.data[props.allData.index][
                                      childFF.uuid
                                    ]
                                      ? props.allData.data[props.allData.index][
                                          childFF.uuid
                                        ].obsDatetime
                                      : undefined
                                  }
                                  onChange={value => {
                                    let childData;
                                    if (Array.isArray(value)) {
                                      childData = [];
                                      value.forEach(val => {
                                        childData.push(
                                          childAnswersMap[val.value]
                                            ? childAnswersMap[val.value]
                                            : val
                                        );
                                      });
                                    } else {
                                      childData = childAnswersMap[value]
                                        ? childAnswersMap[value]
                                        : value;
                                    }
                                    props.allData.onChange(
                                      childFF.uuid,
                                      {
                                        value: childData,
                                        obsDatetime: moment().format(
                                          longDatetimeFormatWithT
                                        ),
                                        comment: get(
                                          props.allData,
                                          ['data', childFF.uuid, 'comment'],
                                          undefined
                                        ),
                                      },
                                      new Set([
                                        ...props.hiddenFields,
                                        ...props.getExtraFieldsToHide(
                                          childFF.field.uuid,
                                          props.fieldsHideMap,
                                          childAnswersMap[value]
                                            ? childAnswersMap[value]
                                            : value
                                        ),
                                      ])
                                    );
                                  }}
                                  className={getStyle(
                                    childConfig.name,
                                    childConfig.props
                                  )}
                                  {...childConfig.props}
                                  label={
                                    componentConfig.name === 'VcMessage' ||
                                    componentConfig.name === 'VcTaskWithDismiss'
                                      ? childFF.field.concept.description
                                          .display
                                      : childFF.field.name
                                  }
                                  required={
                                    props.allData.metadata.requiredFormFields
                                      ? !!props.allData.metadata
                                          .requiredFormFields[childFF.uuid]
                                      : false
                                  }
                                  key={childFF.uuid}
                                  options={
                                    childFF.field.concept.setMembers &&
                                    childFF.field.concept.setMembers.length > 0
                                      ? childFF.field.concept.setMembers
                                      : childFF.field.concept.answers
                                  }
                                />
                              </TableCell>
                            </TableRow>
                          </div>
                        );
                      })
                    : []}
                </Component>
              </TableCell>
            </TableRow>
            <hr className={styles.hrStyle}></hr>
          </>
        );
      });
    }
  };
  return (
    <TableCell className={styles.outerCell}>
      <Table>
        <TableRow>
          <TableCell className={styles.innerCell}>
            {props.allData != undefined ? (
              <VcDateTime
                datatest={`encounter`}
                className={styles.encounterDatetime}
                label=""
                value={
                  props.allData.data != undefined
                    ? get(
                        props.allData.data[props.allData.index],
                        ['encounterDatetime'],
                        undefined
                      )
                    : null
                }
                onChange={value =>
                  props.allData.onChange(
                    'encounterDatetime',
                    moment(value).format(longDatetimeFormatWithT)
                  )
                }
              />
            ) : (
              'Time'
            )}
          </TableCell>
        </TableRow>
        <hr className={styles.hrStyle}></hr>
        {createFields()}
      </Table>
    </TableCell>
  );
};

export default injectIntl(VcMultiReadingTable);
