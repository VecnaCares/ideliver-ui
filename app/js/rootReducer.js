import { combineReducers } from 'redux';
import reduceReducers from 'reduce-reducers';
import visitsReducer from './features/visits/visitsReducer';
import registerReducer from './features/registration/registrationReducer';

import entitiesReducer from './state/entities/entitiesReducer';
import errorsReducer from './state/error/errorReducer';
import uiReducer from './state/ui/uiReducer';
import crossSliceReducer from './state/crossSliceReducer';
import userReducer from './features/userLogin/userReducer';
import locationsReducer from './features/locations/locationsReducer';

// Use ES6 object literal shorthand syntax to define the object shape
const sliceReducer = combineReducers({
  Visits: visitsReducer,
  Register: registerReducer,
  entities: entitiesReducer,
  ui: uiReducer,
  errors: errorsReducer,
  User: userReducer,
  Locations: locationsReducer,
});

const rootReducer = reduceReducers(sliceReducer, crossSliceReducer);

export default rootReducer;
