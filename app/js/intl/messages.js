import { defineMessages } from 'react-intl';

const Messages = defineMessages({
  activeLabour: {
    id: 'activeLabour',
    defaultMessage: 'Active Labour',
  },
  acuity: {
    id: 'acuity',
    defaultMessage: 'Acuity',
  },
  addReading: {
    id: 'addReading',
    defaultMessage: 'Add Reading',
  },
  admission: {
    id: 'admission',
    defaultMessage: 'Admission',
  },
  admit: {
    id: 'admit',
    defaultMessage: 'Admit',
  },
  admitted: {
    id: 'admitted',
    defaultMessage: 'Admitted',
  },
  age: {
    id: 'age',
    defaultMessage: 'Age',
  },
  alert: {
    id: 'alert',
    defaultMessage: 'Alert',
  },
  alertIrregular: {
    id: 'alertIrregular',
    defaultMessage: 'Alert and Irregular',
  },
  allPatientNotes: {
    id: 'allPatientNotes',
    defaultMessage: 'All patient notes',
  },
  ancRisk: {
    id: 'ancRisk',
    defaultMessage: 'ANC Risk factors',
  },
  assessment: {
    id: 'assessment',
    defaultMessage: 'Assessment',
  },
  bloodOxygenError: {
    id: 'bloodOxygenError',
    defaultMessage:
      'The value should be between 1 and 100 (inclusive) and not start with 0',
  },
  calcGestationalAge: {
    id: 'calcGestationalAge',
    defaultMessage: 'The calculated Gestational age is:',
  },
  cancel: {
    id: 'cancel',
    defaultMessage: 'Cancel',
  },
  cardContentDiagnosis: {
    id: 'content',
    defaultMessage: 'No diagnosis assigned.',
  },
  cardContentPossibleDiagnosis: {
    id: 'cardContentPossibleDiagnosis',
    defaultMessage: 'No possible diagnosis assigned.',
  },
  cardContentSummary: {
    id: 'cardContentSummary',
    defaultMessage: 'Summary missing',
  },
  cardHeaderDiagnosis: {
    id: 'cardHeaderDiagnosis',
    defaultMessage: 'Diagnosis',
  },
  cardHeaderPossibleDiagnosis: {
    id: 'cardHeaderPossibleDiagnosis',
    defaultMessage: 'Possible diagnosis',
  },
  cardHeaderSummary: {
    id: 'cardHeaderSummary',
    defaultMessage: 'Visit summary',
  },
  cervix: {
    id: 'cervix',
    defaultMessage: 'Cervix (cm)',
  },
  checkABCDE: {
    id: 'checkABCDE',
    defaultMessage: 'CHECK (ABCDE)',
  },
  clients: {
    id: 'clients',
    defaultMessage: 'Clients',
  },
  close: {
    id: 'close',
    defaultMessage: 'Close',
  },
  companion: {
    id: 'companion',
    defaultMessage: 'Companion',
  },
  complaintsAssessment: {
    id: 'complaintsAssessment',
    defaultMessage: 'General Assessment',
  },
  completedLaborAndDelivery: {
    id: 'completedLaborAndDelivery',
    defaultMessage: 'Has patient completed labour and delivery?',
  },
  confirmedDiagnosisSectionHeader: {
    id: 'confirmedDiagnosisSectionHeader',
    defaultMessage: 'Confirmed',
  },
  contractions: {
    id: 'contractions',
    defaultMessage: 'Contractions',
  },
  contractionsStrength: {
    id: 'contractionsStrength',
    defaultMessage: 'Contractions Strength',
  },
  date: {
    id: 'date',
    defaultMessage: 'date',
  },
  datetime: {
    id: 'datetime',
    defaultMessage: 'Datetime',
  },
  demographics: {
    id: 'demographics',
    defaultMessage: 'Demographics',
  },
  descent: {
    id: 'descent',
    defaultMessage: 'Descent',
  },
  descentUnits: {
    id: 'descentUnits',
    defaultMessage: 'Descent Units',
  },
  diagnosis: {
    id: 'diagnosis',
    defaultMessage: 'Diagnosis',
  },
  dialogHeaderDiagnosis: {
    id: 'dialogHeaderDiagnosis',
    defaultMessage: 'Diagnosis',
  },
  dischargeButton: {
    id: 'dischargeButton',
    defaultMessage: 'DISCHARGE',
  },
  addDiagnoses: {
    id: 'addDiagnoses',
    defaultMessage: 'Add diagnoses',
  },
  dialogHeaderSummary: {
    id: 'dialogHeaderSummary',
    defaultMessage: 'Add visit summary',
  },
  diastolic: {
    id: 'diastolic',
    defaultMessage: 'Diastolic',
  },
  diastolicBloodPressure: {
    id: 'diastolicBloodPressure',
    defaultMessage: 'Diastolic blood pressure',
  },
  discharge: {
    id: 'discharge',
    defaultMessage: 'Discharge',
  },
  discharged: {
    id: 'discharged',
    defaultMessage: 'Discharged',
  },
  dismiss: {
    id: 'dismiss',
    defaultMessage: 'Dismiss',
  },
  dismissMessage: {
    id: 'dismissMessage',
    defaultMessage:
      'Are you sure you want to dismiss this task from Management plan?',
  },
  dismissTitle: {
    id: 'dismissTitle',
    defaultMessage: 'Dismiss the task?',
  },
  doctorNotes: {
    id: 'doctorNotes',
    defaultMessage: 'Doctor`s notes',
  },
  done: {
    id: 'done',
    defaultMessage: 'Done',
  },
  drugDosage: {
    id: 'drugDosage',
    defaultMessage: 'Drug Dosage',
  },
  drugName: {
    id: 'drugName',
    defaultMessage: 'Drug Name',
  },
  drugUnit: {
    id: 'drugUnit',
    defaultMessage: 'Drug Unit',
  },
  editButton: {
    id: 'editButton',
    defaultMessage: 'EDIT',
  },
  education: {
    id: 'education',
    defaultMessage: 'Education',
  },
  encounter: {
    id: 'encounter',
    defaultMessage: 'Encounter',
  },
  fetalHeartRate: {
    id: 'fetalHeartRate',
    defaultMessage: 'Fetal Heart Rate',
  },
  fetus: {
    id: 'fetus',
    defaultMessage: 'Fetus',
  },
  generalManagement: {
    id: 'generalManagement',
    defaultMessage: 'General Management',
  },
  generalManagementMsg: {
    id: 'generalManagementMsg',
    defaultMessage:
      '{genMng} {br} SHOUT FOR HELP and start an IV line {br} {check} {br} - A-Airways {br} - B-Breathing {br} - C-Circulation {br} - D-Disability {br} - E-Establish Management Plan {br} {br} Obtain history from the woman, her relatives or chart but do not restrain her. Provide constant supervision.',
  },
  gravidityAbrev: {
    id: 'gravidityAbrev',
    defaultMessage: 'G',
  },
  h12: {
    id: '12h',
    defaultMessage: '12h',
  },
  h24: {
    id: '24h',
    defaultMessage: '24h',
  },
  heartRate: {
    id: 'heartRate',
    defaultMessage: 'Heart rate (bpm)',
  },
  inlineAlerts: {
    id: 'inlineAlerts',
    defaultMessage: 'Alerts',
  },
  invalidDateAfter: {
    id: 'invalidDateAfter',
    defaultMessage: 'The date should not be before: ',
  },
  invalidFormatErrorMessage: {
    id: 'invalidFormatErrorMessage',
    defaultMessage: 'Error: Invalid format',
  },
  interventions: {
    id: 'interventions',
    defaultMessage: 'Interventions',
  },
  invalidDateBefore: {
    id: 'invalidDateBefore',
    defaultMessage: 'The date should not be after: ',
  },
  invalidValueLess: {
    id: 'invalidValueLess',
    defaultMessage: 'The value should be less than or equal to: ',
  },
  invalidValueMore: {
    id: 'invalidValueMore',
    defaultMessage: 'The value should be more than or equal to: ',
  },
  irregular: {
    id: 'irregular',
    defaultMessage: 'Irregular',
  },
  labResults: {
    id: 'labResults',
    defaultMessage: 'Lab Results',
  },
  labourSigns: {
    id: 'labourSigns',
    defaultMessage: 'Labour Signs',
  },
  labourSummary: {
    id: 'labourSummary',
    defaultMessage: 'Labour Summary',
  },
  laborSummaryLink: {
    id: 'laborSummaryLink',
    defaultMessage: 'COMPLETE LABOUR SUMMARY',
  },
  lastUpdated: {
    id: 'lastUpdated',
    defaultMessage: 'Last Updated',
  },
  liquor: {
    id: 'liquor',
    defaultMessage: 'Liquor',
  },
  livingChildrenAbrev: {
    id: 'livingChildrenAbrev',
    defaultMessage: 'L',
  },
  managementPlanFor: {
    id: 'managementPlanFor',
    defaultMessage: 'Management plan for',
  },
  managementPlans: {
    id: 'managementPlans',
    defaultMessage: 'Management Plans',
  },
  medicalHistory: {
    id: 'medicalHistory',
    defaultMessage: 'Medical History',
  },
  moulding: {
    id: 'moulding',
    defaultMessage: 'Moulding',
  },
  na: {
    id: 'na',
    defaultMessage: 'N/A',
  },
  nextCheckUp: {
    id: 'nextCheckUp',
    defaultMessage: 'Next check-up',
  },
  no: {
    id: 'no',
    defaultMessage: 'No',
  },
  none: {
    id: 'none',
    defaultMessage: 'None',
  },
  normal: {
    id: 'normal',
    defaultMessage: 'Normal',
  },
  notAdmitted: {
    id: 'notAdmitted',
    defaultMessage: 'Not Admitted',
  },
  nursingCardex: {
    id: 'nursingCardex',
    defaultMessage: 'Nursing cardex',
  },
  obHistory: {
    id: 'obHistory',
    defaultMessage: 'History',
  },
  orders: {
    id: 'orders',
    defaultMessage: 'Orders',
  },
  outOfRangeErrorMessage: {
    id: 'outOfRangeErrorMessage',
    defaultMessage: 'Error: Out of range',
  },
  overdue: {
    id: 'overdue',
    defaultMessage: 'Overdue',
  },
  otherDiagnosisSectionHeader: {
    id: 'otherDiagnosisSectionHeader',
    defaultMessage: 'Add',
  },
  parity: {
    id: 'parity',
    defaultMessage: 'Parity',
  },
  patientInfo: {
    id: 'patientInfo',
    defaultMessage: 'Patient Information',
  },
  patientNotes: {
    id: 'patientNotes',
    defaultMessage: 'Patient Notes',
  },
  plateletError: {
    id: 'plateletError',
    defaultMessage: 'Normal range for platelet count is 130 - 400',
  },
  pncVisit: {
    id: 'pncVisit',
    defaultMessage: 'PNC Visit',
  },
  possibleDiagnosisSectionHeader: {
    id: 'possibleDiagnosisSectionHeader',
    defaultMessage: 'Possible',
  },
  postpartum: {
    id: 'postpartum',
    defaultMessage: 'Postpartum',
  },
  presentingComplaints: {
    id: 'presentingComplaints',
    defaultMessage: 'Presenting Complaints',
  },
  pretermBirthsAbrev: {
    id: 'pretermBirthsAbrev',
    defaultMessage: 'P',
  },
  progression: {
    id: 'progression',
    defaultMessage: 'Progression',
  },
  pulse: {
    id: 'pulse',
    defaultMessage: 'Pulse',
  },
  reading: {
    id: 'reading',
    defaultMessage: 'Reading',
  },
  refer: {
    id: 'refer',
    defaultMessage: 'Refer',
  },
  referButton: {
    id: 'referButton',
    defaultMessage: 'REFER',
  },
  referral: {
    id: 'referral',
    defaultMessage: 'Referral',
  },
  regular: {
    id: 'regular',
    defaultMessage: 'Regular',
  },
  required: {
    id: 'required',
    defaultMessage: 'Required Field',
  },
  respiratoryRateError: {
    id: 'respiratoryRateError',
    defaultMessage:
      'The value should be between 1 and 140 (inclusive) and not start with 0',
  },
  restore: {
    id: 'restore',
    defaultMessage: 'Restore',
  },
  restoreMessage: {
    id: 'restoreMessage',
    defaultMessage:
      'Are you sure you want to restore this task to Management plan?',
  },
  restoreTitle: {
    id: 'restoreTitle',
    defaultMessage: 'Restore the task?',
  },
  saveNotesButton: {
    id: 'saveNotesButton',
    defaultMessage: 'SAVE NOTES',
  },
  secondStageLabour: {
    id: 'secondStageLabour',
    defaultMessage: 'Second Stage Labour',
  },
  selectAll: {
    id: 'selectAll',
    defaultMessage: 'Select All',
  },
  startMngPlan: {
    id: 'startMngPlan',
    defaultMessage: 'Start Management Plan',
  },
  surveyTaken: {
    id: 'surveyTaken',
    defaultMessage: ' taken',
  },
  systolicBloodPressure: {
    id: 'systolicBloodPressure',
    defaultMessage: 'Systolic blood pressure',
  },
  task: {
    id: 'task',
    defaultMessage: 'TASK',
  },
  tasks: {
    id: 'tasks',
    defaultMessage: 'Tasks',
  },
  temperature: {
    id: 'temperature',
    defaultMessage: 'Temperature(°C)',
  },
  termBirthsAbrev: {
    id: 'termBirthsAbrev',
    defaultMessage: 'T',
  },
  time: {
    id: 'time',
    defaultMessage: 'time',
  },
  toggleNo: {
    id: 'toggleNo',
    defaultMessage: 'No',
  },
  toggleUnsure: {
    id: 'toggleUnsure',
    defaultMessage: 'Unsure',
  },
  toggleYes: {
    id: 'toggleYes',
    defaultMessage: 'Yes',
  },
  urinalysisDetected: {
    id: 'urinalysisDetected',
    defaultMessage: 'Urinalysis Detected',
  },
  urineProtein: {
    id: 'urineProtein',
    defaultMessage: 'Urine Protein',
  },
  urineAcetone: {
    id: 'urineAcetone',
    defaultMessage: 'Urine Acetone',
  },
  urineVolume: {
    id: 'urineVolume',
    defaultMessage: 'Urine Volume',
  },
  visitStarted: {
    id: 'visitStarted',
    defaultMessage: 'Visit started',
  },
  vitals: {
    id: 'vitals',
    defaultMessage: 'Vitals',
  },
  waiting: {
    id: 'waiting',
    defaultMessage: 'Waiting',
  },
  wardAlerts: {
    id: 'wardAlerts',
    defaultMessage: 'Ward Alerts',
  },
  wardHeader: {
    id: 'wardHeader',
    defaultMessage: 'Ward Dashboard',
  },
  wardSearchPlaceholder: {
    id: 'searchPlaceholder',
    defaultMessage: 'Search by Client First Name, Last Name, or MRN',
  },
  wardVisitsNoResult: {
    id: 'visitsNoResult',
    defaultMessage: 'No results',
  },
  wbcError: {
    id: 'wbcError',
    defaultMessage: 'Normal range for WBC is 3.5 - 10',
  },
  week: {
    id: 'week',
    defaultMessage: 'Week',
  },
  yes: {
    id: 'yes',
    defaultMessage: 'Yes',
  },
  referralSummary: {
    id: 'referralSummary',
    defaultMessage: 'Referral summary',
  },
  dischargeSummary: {
    id: 'dischargeSummary',
    defaultMessage: 'Discharge summary',
  },
  returnToCardex: {
    id: 'returnToCardex',
    defaultMessage: '< RETURN TO CARDEX',
  },
  facilityReferredTo: {
    id: 'facilityReferredTo',
    defaultMessage: 'Facility referred to',
  },
  reasonForReferral: {
    id: 'reasonForReferral',
    defaultMessage: 'Reason for referral',
  },
  referPatient: {
    id: 'referPatient',
    defaultMessage: 'Refer Patient',
  },
  dischargePatient: {
    id: 'dischargePatient',
    defaultMessage: 'Discharge Patient',
  },
  notesButton: {
    id: 'notesButton',
    defaultMessage: 'NOTES',
  },
  intake: {
    id: 'intake',
    defaultMessage: 'Intake',
  },
  pnc: {
    id: 'pnc',
    defaultMessage: 'PNC',
  },
  anc: {
    id: 'anc',
    defaultMessage: 'ANC',
  },
  labor: {
    id: 'labor',
    defaultMessage: 'Labor',
  },
  Save: {
    id: 'Save',
    defaultMessage: 'Save',
  },
  SaveAndContinue: {
    id: 'SaveAndContinue',
    defaultMessage: 'Save and Continue',
  },
  invalidCurrentYear: {
    id: 'invalidCurrentYear',
    defaultMessage: 'The value should be less than or equal to current year',
  },
  invalidCurrentYear: {
    id: 'invalidCurrentYear',
    defaultMessage: 'The value should be less than or equal to current year',
  },
  recordedVitals: {
    id: 'recordedVitals',
    defaultMessage: 'Recorded Vitals',
  },
  showMore: {
    id: 'showMore',
    defaultMessage: 'Show More',
  },
  postDelivery: {
    id: 'postDelivery',
    defaultMessage: 'Post Delivery',
  },
  showLess: {
    id: 'showLess',
    defaultMessage: 'Show Less',
  },
  activePolicyForCovid: {
    id: 'activePolicyForCovid',
    defaultMessage:
      'Activate hospital policy for isolation/quarantine and for COVID-19 testing',
  },
  presentingSymptoms: {
    id: 'activePolicyForCovid',
    defaultMessage: 'Presenting symptoms',
  },
  additionalPresentingSymptoms: {
    id: 'additionalPresentingSymptoms',
    defaultMessage: 'Additional Presenting Symptoms',
  },
  feverDegree: {
    id: 'feverDegree',
    defaultMessage: 'Fever greater than equal to 38 degrees',
  },
  login: {
    id: 'login',
    defaultMessage: 'LOG IN',
  },
  username: {
    id: 'username',
    defaultMessage: 'Username',
  },
  location: {
    id: 'location',
    defaultMessage: 'Location',
  },
  password: {
    id: 'password',
    defaultMessage: 'Password',
  },
  loginFailed: {
    id: 'loginFailed',
    defaultMessage: 'Login Failed! Check your username and/or password',
  },
  dataTestUsername: {
    id: 'dataTestUsername',
    defaultMessage: 'username-input-field',
  },
  dataTestPassword: {
    id: 'dataTestPassword',
    defaultMessage: 'password-input-field',
  },
  dataTestLocation: {
    id: 'dataTestLocation',
    defaultMessage: 'location-drop-down-field',
  },
  dataTestLoginButton: {
    id: 'dataTestLoginButton',
    defaultMessage: 'login-btn',
  },
  registration: {
    id: 'registration',
    defaultMessage: 'Registration',
  },
  givenName: {
    id: 'givenName',
    defaultMessage: 'Given Name',
  },
  middleName: {
    id: 'middleName',
    defaultMessage: 'Middle Name',
  },
  familyName: {
    id: 'familyName',
    defaultMessage: 'Family Name',
  },
  gender: {
    id: 'gender',
    defaultMessage: 'Gender',
  },
  genderM: {
    id: 'genderM',
    defaultMessage: 'Male',
  },
  genderF: {
    id: 'genderF',
    defaultMessage: 'Female',
  },
  day: {
    id: 'day',
    defaultMessage: 'Birthdate- Day *',
  },
  month: {
    id: 'month',
    defaultMessage: 'Birthdate- Month *',
  },
  year: {
    id: 'year',
    defaultMessage: 'Birthdate- Year *',
  },
  estimatedYears: {
    id: 'estimatedYears',
    defaultMessage: 'Estimated Years',
  },
  estimatedMonths: {
    id: 'estimatedYears',
    defaultMessage: 'Estimated Months',
  },
  contactInfo: {
    id: 'contactInfo',
    defaultMessage: 'Contact Info',
  },
  address: {
    id: 'address',
    defaultMessage: 'Address',
  },
  address2: {
    id: 'address2',
    defaultMessage: 'Address 2',
  },
  city: {
    id: 'city',
    defaultMessage: 'City/Village',
  },
  state: {
    id: 'state',
    defaultMessage: 'State/Province',
  },
  country: {
    id: 'country',
    defaultMessage: 'Country',
  },
  postalCode: {
    id: 'postalCode',
    defaultMessage: 'Postal Code',
  },
  phoneNumber: {
    id: 'phoneNumber',
    defaultMessage: 'Phone Number',
  },
  personName: {
    id: 'personName',
    defaultMessage: 'Person Name',
  },
  SaveAndStartVisit: {
    id: 'SaveAndStartVisit',
    defaultMessage: 'Save and Start Visit',
  },
  unidentifiedPatient: {
    id: 'unidentifiedPatient',
    defaultMessage: 'Unidentified Patient',
  },
  relationships: {
    id: 'relationships',
    defaultMessage: 'Relationships',
  },
  monthJan: {
    id: 'monthJan',
    defaultMessage: 'January',
  },
  monthFeb: {
    id: 'monthFeb',
    defaultMessage: 'February',
  },
  monthMar: {
    id: 'monthMar',
    defaultMessage: 'March',
  },
  monthApr: {
    id: 'monthApr',
    defaultMessage: 'April',
  },
  monthMay: {
    id: 'monthMay',
    defaultMessage: 'May',
  },
  monthJun: {
    id: 'monthJun',
    defaultMessage: 'June',
  },
  monthJul: {
    id: 'monthJul',
    defaultMessage: 'July',
  },
  monthAug: {
    id: 'monthAug',
    defaultMessage: 'August',
  },
  monthSept: {
    id: 'monthSept',
    defaultMessage: 'September',
  },
  monthOct: {
    id: 'monthOct',
    defaultMessage: 'October',
  },
  monthNov: {
    id: 'monthNov',
    defaultMessage: 'November',
  },
  monthDec: {
    id: 'monthDec',
    defaultMessage: 'December',
  },
  selectrelationship: {
    id: 'relationship',
    defaultMessage: 'Select Relationship',
  },
  doctor: {
    id: 'doctor',
    defaultMessage: 'Doctor',
  },
  sibling: {
    id: 'sibling',
    defaultMessage: 'Sibling',
  },
  parent: {
    id: 'parent',
    defaultMessage: 'Parent',
  },
  uncleAunt: {
    id: 'uncleAunt',
    defaultMessage: 'Uncle / Aunt',
  },
  supervisor: {
    id: 'supervisor',
    defaultMessage: 'Supervisor',
  },
  spouse: {
    id: 'spouse',
    defaultMessage: 'Spouse',
  },
  dataTestRegistration: {
    id: 'dataTestRegistration',
    defaultMessage: 'Registration',
  },
  dataTestRegistrationSideBar: {
    id: 'dataTestRegistrationSideBar',
    defaultMessage: 'sideBar-registration-btn',
  },
  dataTestGivenName: {
    id: 'dataTestGivenName',
    defaultMessage: 'givenName-input-field',
  },

  dataTestMiddleName: {
    id: 'dataTestMiddleName',
    defaultMessage: 'middleName-input-field',
  },
  dataTestFamilyName: {
    id: 'dataTestFamilyName',
    defaultMessage: 'familyName-input-field',
  },
  dataTestGender: {
    id: 'dataTestGender',
    defaultMessage: 'gender-drop-down-field',
  },
  dataTestDay: {
    id: 'dataTestDay',
    defaultMessage: 'birthdateDay-input-field',
  },
  dataTestMonth: {
    id: 'dataTestMonth',
    defaultMessage: 'birthdateMonth-drop-down-field',
  },
  dataTestYear: {
    id: 'dataTestYear',
    defaultMessage: 'birthdateYear-input-field',
  },
  dataTestEstimatedYears: {
    id: 'dataTestEstimatedYears',
    defaultMessage: 'estimatedYears-input-field',
  },
  dataTestEstimatedMonths: {
    id: 'dataTestEstimatedYears',
    defaultMessage: 'estimatedMonths-input-field',
  },
  dataTestAddress: {
    id: 'dataTestAddress',
    defaultMessage: 'address-input-field',
  },
  dataTestAddress2: {
    id: 'dataTestAddress2',
    defaultMessage: 'address2-input-field',
  },
  dataTestCity: {
    id: 'dataTestCity',
    defaultMessage: 'city/village-input-field',
  },
  dataTestState: {
    id: 'dataTestState',
    defaultMessage: 'state/province-input-field',
  },
  dataTestCountry: {
    id: 'dataTestCountry',
    defaultMessage: 'country-input-field',
  },
  dataTestPostalCode: {
    id: 'dataTestPostalCode',
    defaultMessage: 'postalCode-input-field',
  },
  dataTestPhoneNumber: {
    id: 'dataTestPhoneNumber',
    defaultMessage: 'phoneNumber-input-field',
  },
  dataTestPersonName: {
    id: 'dataTestPersonName',
    defaultMessage: 'personName-input-field',
  },
  dataTestSaveAndStartVisit: {
    id: 'dataTestSaveAndStartVisit',
    defaultMessage: 'saveandStartVisit-btn',
  },
  dataTestRelationships: {
    id: 'dataTestRelationships',
    defaultMessage: 'relationships-drop-down-field',
  },
  dataTestSaveAndContinue: {
    id: 'dataTestSaveAndContinue',
    defaultMessage: 'saveandContinue-btn',
  },
  dataTestCancel: {
    id: 'dataTestCancel',
    defaultMessage: 'cancel-btn',
  },
  dataTestAdd: {
    id: 'dataTestAdd',
    defaultMessage: 'relationships-add-btn-img',
  },
  dataTestMinus: {
    id: 'dataTestMinus',
    defaultMessage: 'relationships-delete-btn-img',
  },
  dataTestUnidentifiedPatient: {
    id: 'dataTestunidentifiedPatient',
    defaultMessage: 'unidentifiedPatient-generic-switch-field',
  },
  dataTestinterventionDelete: {
    id: 'dataTestinterventionDelete',
    defaultMessage: 'interventions-delete-btn',
  },
  dataTestinterventionAdd: {
    id: 'dataTestinterventionAdd',
    defaultMessage: 'interventions-add-btn',
  },
  Logout: {
    id: 'Logout',
    defaultMessage: 'Logout',
  },
  Profile: {
    id: 'Profile',
    defaultMessage: 'Profile',
  },
  Myaccount: {
    id: 'Myaccount',
    defaultMessage: 'My account',
  },
  SaveAndContinuetoWardDashboard: {
    id: 'SaveAndContinuetoWardDashboard',
    defaultMessage: 'Save And Continue to Ward Dashboard',
  },
});
export default Messages;
