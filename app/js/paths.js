export const APP_PATHNAME = '/openmrs/owa/ideliver.ui/index.html';

const DEFAULT_API_HOST = 'http://localhost:8080';
// eslint-disable-next-line no-underscore-dangle
export const { API_HOST = DEFAULT_API_HOST } = window._env_ || {};
export const REST_API_PATHNAME = `${API_HOST}/openmrs/ws/rest/`;
export const REST_API_VERSION = 'v1/';
