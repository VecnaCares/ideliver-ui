import { fromJS } from 'immutable';
import {
  allAncFormDataSorting,
  allPncFormDataSorting,
  allVitalFormDataSorting,
  allActiveLabourFormDataSorting,
} from '../../../../js/state/ui/form/formCrossSiteReducer';

describe('Form Cross Site Reducer', () => {
  it('formcrossreducer function test case', () => {
    expect(allAncFormDataSorting([])).toStrictEqual([]);
    expect(allPncFormDataSorting([])).toStrictEqual([]);
    expect(allVitalFormDataSorting([])).toStrictEqual([]);
  });
  it('should populate all the Active Labour form data in sorted way', () => {
    let activeLabourUnSortedData = [
      {
        '23b2cab3-da98-4469-b7c9-3b2e77869e1b': [
          {
            0: {
              '7feb5905-f4d4-42f8-a2d9-9b40f7ca9cbd': {
                comment: '{"formField":"7feb5905-f4d4-42f8-a2d9-9b40f7ca9cbd"}',
                obsDatetime: '2020-03-16T15:38:13.000+0530',
                uuid: '7efd662d-a483-4bfb-a025-9f5fee8f638b',
                value: 'pelvic Exam 1',
              },
              encounterDatetime: '2020-03-16T15:38:51.000+0530',
              encounterUuid: 'c65b96bd-a4b4-4680-bcac-19d8ed040e78',
              lastUpdated: '2020-03-16T15:38:51.000+0530',
            },
            1: {
              '7feb5905-f4d4-42f8-a2d9-9b40f7ca9cbd': {
                comment: '{"formField":"7feb5905-f4d4-42f8-a2d9-9b40f7ca9cbd"}',
                obsDatetime: '2020-03-16T15:46:55.000+0530',
                uuid: '07a18074-abd1-499f-af1c-d686996ad3fa',
                value: 'Pelvic Exam 2',
              },
              encounterDatetime: '2020-03-16T15:47:23.000+0530',
              encounterUuid: '030c282c-56ec-4bfa-b295-48b9a2430d4d',
              lastUpdated: '2020-03-16T15:47:23.000+0530',
            },
          },
        ],
      },
    ];
    let activeLabourSortData = [
      {
        '23b2cab3-da98-4469-b7c9-3b2e77869e1b': [
          {
            0: {
              '7feb5905-f4d4-42f8-a2d9-9b40f7ca9cbd': {
                value: 'pelvic Exam 1',
                obsDatetime: '2020-03-16T15:38:13.000+0530',
                comment: '{"formField":"7feb5905-f4d4-42f8-a2d9-9b40f7ca9cbd"}',
                uuid: '7efd662d-a483-4bfb-a025-9f5fee8f638b',
              },
              lastUpdated: '2020-03-16T15:38:51.000+0530',
              encounterDatetime: '2020-03-16T15:38:51.000+0530',
              encounterUuid: 'c65b96bd-a4b4-4680-bcac-19d8ed040e78',
            },
            1: {
              '7feb5905-f4d4-42f8-a2d9-9b40f7ca9cbd': {
                value: 'Pelvic Exam 2',
                obsDatetime: '2020-03-16T15:46:55.000+0530',
                comment: '{"formField":"7feb5905-f4d4-42f8-a2d9-9b40f7ca9cbd"}',
                uuid: '07a18074-abd1-499f-af1c-d686996ad3fa',
              },
              lastUpdated: '2020-03-16T15:47:23.000+0530',
              encounterUuid: '030c282c-56ec-4bfa-b295-48b9a2430d4d',
              encounterDatetime: '2020-03-16T15:47:23.000+0530',
            },
          },
        ],
      },
    ];
    expect(
      allActiveLabourFormDataSorting(activeLabourUnSortedData.sort())
    ).toEqual(activeLabourSortData);
  });
});
