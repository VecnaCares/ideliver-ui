import { put, call } from 'redux-saga/effects';
import { cloneableGenerator } from '@redux-saga/testing-utils';
import startVisitSaga, {
  getUrlQuery,
} from '../../../../js/features/registration/sagas/startVisitSaga';
import startVisitSuccessAction from '../../../../js/features/registration/actions/startVisitSuccessAction';
import startVisitFailAction from '../../../../js/features/registration/actions/startVisitFailAction';
import startVisitAction from '../../../../js/features/registration/actions/startVisitAction';
import { postData } from '../../../../js/api';

describe('start visit flow', () => {
  const action = startVisitAction(
    'mockUrl',
    '0595be24-d811-402f-8c52-cfded5555a15',
    'b34cc51e-a1a4-4813-bb62-13d696007d0e'
  );

  const generator = cloneableGenerator(startVisitSaga)(action);

  test('start visit success', () => {
    const clone = generator.clone();
    expect(clone.next().value).toEqual(call(postData, getUrlQuery(action)));

    expect(clone.next().value).toEqual(
      put(startVisitSuccessAction('0595be24-d811-402f-8c52-cfded5555a15'))
    );
    expect(clone.next().done).toEqual(true);
    expect(clone.next().done).toEqual(true);
  });

  test('start visit error', () => {
    const clone = generator.clone();
    clone.next();
    expect(clone.throw(new Error('error')).value).toEqual(
      put(startVisitFailAction('error'))
    );
    expect(clone.next().done).toEqual(true);
  });
});
