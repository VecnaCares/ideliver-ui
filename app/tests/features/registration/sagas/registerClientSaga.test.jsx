import React from 'react';
import { FormattedDate } from 'react-intl';
import { put, call } from 'redux-saga/effects';
import { cloneableGenerator } from '@redux-saga/testing-utils';
import registerClientSaga from '../../../../js/features/registration/sagas/registerClientSaga';
import registerClientSuccessAction from '../../../../js/features/registration/actions/registerClientSuccessAction';
import registerClientFailAction from '../../../../js/features/registration/actions/registerClientFailAction';
import registerClientAction from '../../../../js/features/registration/actions/registerClientAction';
import { concept, identifierType, location } from '../../../../js/uuid';
import { postData } from '../../../../js/api';

describe('register client flow', () => {
  const mockFunc = jest.fn();

  const action = registerClientAction(
    'mockUrl',
    {
      names: [
        {
          givenName: 'givenName',
          middleName: 'middleName',
          familyName: 'familyName',
        },
      ],
    },
    { push: mockFunc }
  );

  const generator = cloneableGenerator(registerClientSaga)(action);

  test('register client success', () => {
    const clone = generator.clone();
    expect(clone.next().value).toEqual(
      call(
        postData,
        `${action.url}idgen/identifiersource/${concept.IDENTIFIER_SOURCE_UUID}/identifier`,
        { identifierSource: concept.IDENTIFIER_SOURCE_UUID }
      )
    );
    const identifiers = [
      {
        identifierType: { uuid: identifierType.OPENMRS_ID_UUID },
        location: { uuid: location.INPATIENT_WARD },
        identifier: 'idResult',
      },
    ];
    const patientJson = {
      person: action.payload,
      identifiers,
    };
    expect(clone.next({ identifier: 'idResult' }).value).toEqual(
      call(postData, `${action.url}patient/`, patientJson)
    );
    const result = {
      results: [
        {
          id: 'uuid',
          mrn: 'idResult',
          givenName: 'givenName',
          middleName: 'middleName',
          familyName: 'familyName',
          gender: 'Female',
          age: 0,
          birthdate: (
            <FormattedDate
              day="2-digit"
              month="short"
              value="someDate"
              year="numeric"
            />
          ),
        },
      ],
      totalCount: 1,
    };
    expect(
      clone.next({
        uuid: 'uuid',
        person: { gender: 'Female', age: 0, birthdate: 'someDate' },
      }).value
    ).toEqual(put(registerClientSuccessAction(result)));

    expect(mockFunc.mock.calls.length).toBe(0);
    expect(clone.next().done).toEqual(true);
  });

  test('register error', () => {
    const clone = generator.clone();
    clone.next();
    clone.next();
    expect(clone.throw(new Error('error')).value).toEqual(
      put(registerClientFailAction('error'))
    );
    expect(clone.next().done).toEqual(true);
  });
});
