import { fromJS } from 'immutable';
import registerReducer from '../../../js/features/registration/registrationReducer';
import registerClientAction from '../../../js/features/registration/actions/registerClientAction';
import registerClientFailAction from '../../../js/features/registration/actions/registerClientFailAction';
import registerClientSuccessAction from '../../../js/features/registration/actions/registerClientSuccessAction';

import startVisitAction from '../../../js/features/registration/actions/startVisitAction';
import startVisitFailAction from '../../../js/features/registration/actions/startVisitFailAction';
import startVisitSuccessAction from '../../../js/features/registration/actions/startVisitSuccessAction';

describe('RegisterReducer', () => {
  it('should return the initial state', () => {
    expect(registerReducer(undefined, {})).toEqual(
      fromJS({
        submittingRegistration: false,
        success: false,
        startVisitSuccess: false,
      })
    );
  });

  it('should handle registerClientAction', () => {
    expect(registerReducer(fromJS({}), registerClientAction('data'))).toEqual(
      fromJS({
        submittingRegistration: true,
        startVisitSuccess: false,
        success: false,
      })
    );
  });

  it('should handle registerClientFailAction', () => {
    expect(
      registerReducer(fromJS({}), registerClientFailAction('errors'))
    ).toEqual(
      fromJS({
        submittingRegistration: false,
        startVisitSuccess: false,
        success: false,
      })
    );
  });

  it('should handle registerClientSuccessAction', () => {
    expect(
      registerReducer(fromJS({}), registerClientSuccessAction('data'))
    ).toEqual(
      fromJS({
        submittingRegistration: false,
        success: true,
        startVisitSuccess: false,
      })
    );
  });

  it('should handle startVisitAction', () => {
    expect(registerReducer(fromJS({}), startVisitAction('data'))).toEqual(
      fromJS({
        submittingRegistration: true,
        startVisitSuccess: false,
        success: false,
      })
    );
  });
  it('should handle startVisitFailAction', () => {
    expect(registerReducer(fromJS({}), startVisitFailAction('error'))).toEqual(
      fromJS({
        submittingRegistration: false,
        startVisitSuccess: false,
        success: false,
      })
    );
  });

  it('should handle startVisitSuccessAction', () => {
    expect(
      registerReducer(fromJS({}), startVisitSuccessAction('data'))
    ).toEqual(
      fromJS({
        submittingRegistration: false,
        startVisitSuccess: true,
        success: false,
      })
    );
  });
});
