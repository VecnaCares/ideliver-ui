import React from 'react';
import lolex from 'lolex';
import { IntlProvider } from 'react-intl';
import { act } from 'react-dom/test-utils';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import { createMockStore } from 'redux-test-utils';
import Registration from '../../../js/features/registration/registration';
import VcDropDown from '../../../js/components/vcDropDown/vcDropDown';
import { mountWithRouterAndIntl } from '../../enzyme-test-helpers';
import VcGenericSwitch from '../../../js//components/vcGenericSwitch/vcGenericSwitch';
import { attributeType } from '../../../js/uuid';

describe('Registration', () => {
  // Construct a new `IntlProvider` instance by passing `props` and
  // `context` as React would, then call `getChildContext()` to get the
  // React Intl API, complete with the `format*()` functions.
  const intlProvider = new IntlProvider({ locale: 'en' }, {});
  const { intl } = intlProvider.getChildContext();
  let clock;
  let mockFunc1, mockFunc2;
  const teststateProvince = {};
  let store;
  let component;

  beforeEach(() => {
    clock = lolex.install();
    mockFunc1 = jest.fn();
    mockFunc2 = jest.fn();

    store = createMockStore(teststateProvince);

    component = mountWithRouterAndIntl(
      <Provider store={store}>
        <MemoryRouter initialEntries={['/registration']}>
          <Registration.WrappedComponent
            intl={intl}
            history={{ push: jest.fn() }}
            location={{ location: { pathname: '/registration' } }}
            onSubmit={mockFunc1}
          />
        </MemoryRouter>
        <MemoryRouter initialEntries={['/registration']}>
          <Registration.WrappedComponent
            intl={intl}
            history={{ push: jest.fn() }}
            location={{ location: { pathname: '/registration' } }}
            startVisit={mockFunc2}
          />
        </MemoryRouter>
      </Provider>
    )
      .find('Registration')
      .at(0);
  });

  it('should render', () => {
    expect(component).toBeDefined();

    expect(mockFunc1.mock.calls.length).toBe(0);
    expect(mockFunc2.mock.calls.length).toBe(0);

    const textFields = component.find('VcTextField');

    const givenName = textFields.at(0);
    const familyName = textFields.at(1);
    const middleName = textFields.at(2);

    const gender = component.find(VcDropDown).at(0);
    const birthdateMonth = component.find(VcDropDown).at(1);
    const unidentified = component.find(VcGenericSwitch).at(0);

    const birthdateDay = textFields.at(3);
    const birthdateYear = textFields.at(4);

    const estimatedYear = textFields.at(5);
    const estimatedMonth = textFields.at(6);
    const address1 = textFields.at(7);
    const address2 = textFields.at(8);
    const cityVillage = textFields.at(9);
    const stateProvince = textFields.at(10);
    const country = textFields.at(11);

    const postalcode = textFields.at(12);
    const noPhone = textFields.at(13);

    familyName.props().onChange('');
    middleName.props().onChange('');
    givenName.props().onChange('');

    birthdateDay.props().onChange('');
    birthdateYear.props().onChange('');
    estimatedYear.props().onChange('');
    estimatedMonth.props().onChange('');

    gender.props().onChange('');

    address1.props().onChange('');
    address2.props().onChange('');
    cityVillage.props().onChange('');
    stateProvince.props().onChange('');
    country.props().onChange('');
    postalcode.props().onChange('');
    birthdateMonth.props().onChange('');
    unidentified.props().onChange('');

    expect(component.state()).toEqual({
      person: {
        names: [
          {
            givenName: '',
            middleName: '',
            familyName: '',
          },
        ],
        gender: '',
        age: '',
        birthdate: '--1-',
        birthdateEstimated: '',
        addresses: [
          {
            preferred: true,
            address1: '',
            address2: '',
            cityVillage: '',
            stateProvince: '',
            country: '',
            postalCode: '',
          },
        ],
        attributes: [
          {
            value: '',
            attributeType: {
              uuid: attributeType.PHONE_UUID,
            },
          },
        ],
      },
      birthdateDay: '',
      birthdateMonth: '',
      positionOfMonth: '00',
      birthdateYear: '',
      estimatedMonth: '',
      estimatedYear: '',
      unidentified: '',
      showValidation: false,
      success: false,
    });

    estimatedYear.props().onChange('');

    expect(component.state().person.birthdateEstimated).toEqual('');
    expect(component.state().person.age).toEqual('');
    expect(component.state().birthdateDay).toEqual('');
    expect(component.state().birthdateYear).toEqual('');

    expect(component.state().person.addresses[0].address1).toEqual('');
    expect(component.state().person.addresses[0].address2).toEqual('');
    expect(component.state().person.addresses[0].cityVillage).toEqual('');
    expect(component.state().person.addresses[0].stateProvince).toEqual('');
    expect(component.state().person.addresses[0].country).toEqual('');
    expect(component.state().person.addresses[0].postalCode).toEqual('');

    estimatedMonth.props().onChange('');
    expect(component.state().unidentified).toEqual('');

    expect(component.state().person.addresses[0].preferred).toEqual(true);

    const button = component.find('VcButton').at(0);
    button.simulate('click');

    const button1 = component.find('VcButton').at(1);
    button1.simulate('click');

    expect(mockFunc1.mock.calls.length).toBe(0);
    expect(mockFunc2.mock.calls.length).toBe(0);

    const genericSwitch = component.find('VcGenericSwitch').getElements()[0];
    genericSwitch.props.onChange(true);

    expect(mockFunc1.mock.calls.length).toBe(0);
    expect(mockFunc2.mock.calls.length).toBe(0);

    givenName.props().onChange('');
    familyName.props().onChange('');
    middleName.props().onChange('');

    button.simulate('click');
    button1.simulate('click');

    expect(mockFunc1.mock.calls.length).toBe(0);
    expect(mockFunc2.mock.calls.length).toBe(0);

    expect(component.state().success).toEqual(false);
    expect(component.state().showValidation).toEqual(true);

    clock.uninstall();
  });
  it('should validate required fields', () => {
    expect(component).toBeDefined();
    const button = component.find('VcButton').at(0);
    button.simulate('click');

    const textFields = component.find('VcTextField');

    const familyName = textFields.at(2);
    const givenName = textFields.at(0);
    const gender = component.find(VcDropDown).at(0);
    const birthdateDay = textFields.at(3);
    const address1 = textFields.at(7);

    expect(givenName).toBeDefined();
    expect(familyName).toBeDefined();
    expect(gender).toBeDefined();
    expect(birthdateDay).toBeDefined();
    expect(address1).toBeDefined();

    expect(givenName.props().error).toEqual(false);
    expect(familyName.props().error).toEqual(false);
    expect(gender.props().error).toEqual(false);
    expect(birthdateDay.props().error).toEqual(false);
    expect(mockFunc1.mock.calls.length).toEqual(0);
  });

  it('should call onSubmit if validation passes', () => {
    expect(component).toBeDefined();
    const button = component.find('VcButton').at(0);

    const textFields = component.find('VcTextField');
    const givenName = textFields.at(0);
    const familyName = textFields.at(2);
    const gender = component.find(VcDropDown).at(0);
    const birthdateDay = textFields.at(3);
    const address1 = textFields.at(7);

    act(() => {
      givenName.props().onChange('testGivenName');
      familyName.props().onChange('testFamilyName');
      gender.props().onChange('testgender');
      birthdateDay.props().onChange('testBirthdateDay');
      address1.props().onChange('testAddress1');
    });
    button.simulate('click');

    expect(mockFunc1.mock.calls.length).toEqual(0);
  });
});
