import React from 'react';
import { IntlProvider } from 'react-intl';
import { Provider } from 'react-redux';
import { fromJS } from 'immutable';
import { createMockStore } from 'redux-test-utils';
import { MemoryRouter } from 'react-router-dom';
import { mountWithRouterAndIntl } from '../../enzyme-test-helpers';
import { REST_API_PATHNAME, REST_API_VERSION } from '../../../js/paths';
import { REGISTER_CLIENT_ACTION } from '../../../js/features/registration/actions/registerClientAction';
import { START_VISIT_ACTION } from '../../../js/features/registration/actions/startVisitAction';
import RegistrationContainer from '../../../js/features/registration/registrationContainer';
import Registration from '../../../js/features/registration/registration';

describe('RegistrationContainer', () => {
  // Construct a new `IntlProvider` instance by passing `props` and
  // `context` as React would, then call `getChildContext()` to get the
  // React Intl API, complete with the `format*()` functions.
  const intlProvider = new IntlProvider({ locale: 'en' }, {});
  const { intl } = intlProvider.getChildContext();
  let store, component, testState, mockFunc;

  beforeEach(() => {
    mockFunc = jest.fn();

    testState = {
      Register: fromJS({}),
      User: fromJS({}),
    };
    store = createMockStore(testState);
    component = mountWithRouterAndIntl(
      <Provider store={store}>
        <MemoryRouter initialEntries={['/registration']}>
          <RegistrationContainer
            intl={intl}
            history={{}}
            location={{
              pathname: '01234567DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD89',
            }}
            onSubmit={mockFunc}
          />
        </MemoryRouter>
      </Provider>
    )
      .find(Registration)
      .get(0);
  });

  it('should render without throwing an error', () => {
    expect(component).toBeDefined();
  });

  it('submit action should get dispatched', () => {
    component.props.onSubmit('mockEncounterTypeId');

    const actions = store.getActions();
    expect(actions).toEqual([
      {
        history: {},
        url: REST_API_PATHNAME + REST_API_VERSION,
        payload: 'mockEncounterTypeId',
        type: REGISTER_CLIENT_ACTION,
      },
    ]);
  });

  it('start visit action should get dispatched', () => {
    component.props.startVisit('b34cc51e-a1a4-4813-bb62-13d696007d0e');

    const actions = store.getActions();
    expect(actions).toEqual([
      {
        url: REST_API_PATHNAME,
        locationUuid: 'b34cc51e-a1a4-4813-bb62-13d696007d0e',
        type: START_VISIT_ACTION,
      },
    ]);
  });
});
