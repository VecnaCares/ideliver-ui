import registerClientFailAction, {
  REGISTER_CLIENT_FAILED,
} from '../../../../js/features/registration/actions/registerClientFailAction';

describe('registerClientFailAction', () => {
  it('should return the correct action', () => {
    const result = registerClientFailAction('payload');
    expect(result).toEqual({
      type: REGISTER_CLIENT_FAILED,
      payload: 'payload',
    });
  });
});
