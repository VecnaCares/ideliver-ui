import startVisitFailAction, {
  START_VISIT_FAILED,
} from '../../../../js/features/registration/actions/startVisitFailAction';

describe('startVisitFailAction', () => {
  it('should return the correct action', () => {
    const result = startVisitFailAction('test');
    expect(result).toEqual({
      type: START_VISIT_FAILED,
      payload: 'test',
    });
  });
});
