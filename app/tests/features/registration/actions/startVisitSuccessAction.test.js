import startVisitSuccessAction, {
  START_VISIT_SUCCEEDED,
} from '../../../../js/features/registration/actions/startVisitSuccessAction';

describe('startVisitSuccessAction', () => {
  it('should return the correct action', () => {
    const result = startVisitSuccessAction('clientId');
    expect(result).toEqual({
      type: START_VISIT_SUCCEEDED,
      clientId: 'clientId',
    });
  });
});
