import registerClientSuccessAction, {
  REGISTER_CLIENT_SUCCEEDED,
} from '../../../../js/features/registration/actions/registerClientSuccessAction';

describe('registerClientSuccessAction', () => {
  it('should return the correct action', () => {
    const result = registerClientSuccessAction('payload');
    expect(result).toEqual({
      type: REGISTER_CLIENT_SUCCEEDED,
      payload: 'payload',
    });
  });
});
