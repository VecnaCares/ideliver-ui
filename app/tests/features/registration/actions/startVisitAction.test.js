import startVisitAction, {
  START_VISIT_ACTION,
} from '../../../../js/features/registration/actions/startVisitAction';

describe('startVisitAction', () => {
  it('should return the correct action', () => {
    const result = startVisitAction('url', 'clientId', 'locationUuid');
    expect(result).toEqual({
      type: START_VISIT_ACTION,
      url: 'url',
      clientId: 'clientId',
      locationUuid: 'locationUuid',
    });
  });
});
