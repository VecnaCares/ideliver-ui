import registerClientAction, {
  REGISTER_CLIENT_ACTION,
} from '../../../../js/features/registration/actions/registerClientAction';

describe('registerClientAction', () => {
  it('should return the correct action', () => {
    const result = registerClientAction('url', 'payload', 'history');
    expect(result).toEqual({
      type: REGISTER_CLIENT_ACTION,
      url: 'url',
      payload: 'payload',
      history: 'history',
    });
  });
});
