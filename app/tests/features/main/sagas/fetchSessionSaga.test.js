import { put, call } from 'redux-saga/effects';
import { cloneableGenerator } from '@redux-saga/testing-utils';
import fetchSessionSaga, {
  getUrlQuery,
} from '../../../../js/features/main/sagas/fetchSessionSaga';
import fetchSessionSuccessAction from '../../../../js/features/main/actions/fetchSessionSuccessAction';
import fetchSessionFailAction from '../../../../js/features/main/actions/fetchSessionFailAction';
import fetchSessionAction from '../../../../js/features/main/actions/fetchSessionAction';
import { postData } from '../../../../js/api';

describe('start visit flow', () => {
  const payload = {
    apiAuthorization: 'mockApiAuthorization',
    location: 'mockLocation',
  };
  const action = fetchSessionAction('mockUrl', payload);

  const generator = cloneableGenerator(fetchSessionSaga)(action);

  test('start session success', () => {
    const clone = generator.clone();
    expect(clone.next().value).toEqual(
      call(
        postData,
        getUrlQuery(action),
        { location: payload.location },
        payload.apiAuthorization
      )
    );
    expect(clone.next(payload).value).toEqual(
      put(fetchSessionSuccessAction(payload))
    );
    expect(clone.next().done).toEqual(true);
  });

  test('start session error', () => {
    const clone = generator.clone();
    clone.next();
    expect(clone.throw(new Error('error')).value).toEqual(
      put(fetchSessionFailAction('error'))
    );
    expect(clone.next().done).toEqual(true);
  });
});
