import { fromJS } from 'immutable';
import locationsReducer, {
  defaultState,
} from '../../../js/features/locations/locationsReducer';
import fetchLocationsSuccessAction from '../../../js/features/locations/actions/fetchLocationsSuccessAction';
import { successPayload } from './actions/fetchLocationsSuccessAction.test';
import fetchLocationsFailAction from '../../../js/features/locations/actions/fetchLocationsFailAction';

describe('LocationsReducer', () => {
  it('should return the initial state', () => {
    expect(locationsReducer(undefined, {})).toEqual(fromJS(defaultState));
  });

  it('should handle fetchLocationSuccessAction', () => {
    expect(
      locationsReducer(
        fromJS(defaultState),
        fetchLocationsSuccessAction(successPayload)
      )
    ).toEqual(
      fromJS({
        locations: successPayload.locations,
        totalCount: Object.keys(successPayload.locations).length,
        tags: successPayload.tags,
        errors: [],
      })
    );
  });

  it('should handle fetchLocationFailAction', () => {
    expect(
      locationsReducer(fromJS({}), fetchLocationsFailAction('errorMsg'))
    ).toEqual(
      fromJS({
        errors: ['errorMsg'],
      })
    );
  });
});
