import { call, put } from 'redux-saga/effects';
import { cloneableGenerator } from '@redux-saga/testing-utils';
import fetchLocationsAction from '../../../../js/features/locations/actions/fetchLocationsAction';
import fetchLocationsSaga, {
  getUrlQuery,
} from '../../../../js/features/locations/sagas/fetchLocationsSaga';
import { getData } from '../../../../js/api';
import fetchLocationsSuccessAction from '../../../../js/features/locations/actions/fetchLocationsSuccessAction';
import fetchLocationsFailAction from '../../../../js/features/locations/actions/fetchLocationsFailAction';

export const apiMockResults = {
  results: [
    {
      uuid: 'b1a8b05e-3542-4037-bbd3-998ee9c40574',
      display: 'Inpatient Ward',
      name: 'Inpatient Ward',
      tags: [
        {
          uuid: '8d4626ca-7abd-42ad-be48-56767bbcf272',
          display: 'Transfer Location',
          name: 'Transfer Location',
          description:
            'Patients may only be transferred (within the hospital) to a location with this tag',
          retired: false,
          links: [
            {
              rel: 'self',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/8d4626ca-7abd-42ad-be48-56767bbcf272',
            },
            {
              rel: 'full',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/8d4626ca-7abd-42ad-be48-56767bbcf272?v=full',
            },
          ],
          resourceVersion: '1.8',
        },
        {
          uuid: 'b8bbf83e-645f-451f-8efe-a0db56f09676',
          display: 'Login Location',
          name: 'Login Location',
          description:
            'When a user logs in and chooses a session location, they may only choose one with this tag',
          retired: false,
          links: [
            {
              rel: 'self',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/b8bbf83e-645f-451f-8efe-a0db56f09676',
            },
            {
              rel: 'full',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/b8bbf83e-645f-451f-8efe-a0db56f09676?v=full',
            },
          ],
          resourceVersion: '1.8',
        },
        {
          uuid: '1c783dca-fd54-4ea8-a0fc-2875374e9cb6',
          display: 'Admission Location',
          name: 'Admission Location',
          description:
            'Patients may only be admitted to inpatient care in a location with this tag',
          retired: false,
          links: [
            {
              rel: 'self',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/1c783dca-fd54-4ea8-a0fc-2875374e9cb6',
            },
            {
              rel: 'full',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/1c783dca-fd54-4ea8-a0fc-2875374e9cb6?v=full',
            },
          ],
          resourceVersion: '1.8',
        },
      ],
      parentLocation: {
        uuid: 'aff27d58-a15c-49a6-9beb-d30dcfc0c66e',
        display: 'Amani Hospital',
        name: 'Amani Hospital',
        tags: [
          {
            uuid: '37dd4458-dc9e-4ae6-a1f1-789c1162d37b',
            display: 'Visit Location',
            name: 'Visit Location',
            description:
              'Visits are only allowed to happen at locations tagged with this location tag or at locations that descend from a location tagged with this tag.',
            retired: false,
            links: [
              {
                rel: 'self',
                uri:
                  'http://localhost:8080/openmrs/ws/rest/v1/locationtag/37dd4458-dc9e-4ae6-a1f1-789c1162d37b',
              },
              {
                rel: 'full',
                uri:
                  'http://localhost:8080/openmrs/ws/rest/v1/locationtag/37dd4458-dc9e-4ae6-a1f1-789c1162d37b?v=full',
              },
            ],
            resourceVersion: '1.8',
          },
        ],
      },
      childLocations: [],
      retired: false,
      links: [
        {
          rel: 'self',
          uri:
            'http://localhost:8080/openmrs/ws/rest/v1/location/b1a8b05e-3542-4037-bbd3-998ee9c40574',
        },
        {
          rel: 'default',
          uri:
            'http://localhost:8080/openmrs/ws/rest/v1/location/b1a8b05e-3542-4037-bbd3-998ee9c40574?v=default',
        },
      ],
    },
    {
      uuid: '2131aff8-2e2a-480a-b7ab-4ac53250262b',
      display: 'Isolation Ward',
      name: 'Isolation Ward',
      tags: [
        {
          uuid: '8d4626ca-7abd-42ad-be48-56767bbcf272',
          display: 'Transfer Location',
          name: 'Transfer Location',
          description:
            'Patients may only be transferred (within the hospital) to a location with this tag',
          retired: false,
          links: [
            {
              rel: 'self',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/8d4626ca-7abd-42ad-be48-56767bbcf272',
            },
            {
              rel: 'full',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/8d4626ca-7abd-42ad-be48-56767bbcf272?v=full',
            },
          ],
          resourceVersion: '1.8',
        },
        {
          uuid: 'b8bbf83e-645f-451f-8efe-a0db56f09676',
          display: 'Login Location',
          name: 'Login Location',
          description:
            'When a user logs in and chooses a session location, they may only choose one with this tag',
          retired: false,
          links: [
            {
              rel: 'self',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/b8bbf83e-645f-451f-8efe-a0db56f09676',
            },
            {
              rel: 'full',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/b8bbf83e-645f-451f-8efe-a0db56f09676?v=full',
            },
          ],
          resourceVersion: '1.8',
        },
        {
          uuid: '1c783dca-fd54-4ea8-a0fc-2875374e9cb6',
          display: 'Admission Location',
          name: 'Admission Location',
          description:
            'Patients may only be admitted to inpatient care in a location with this tag',
          retired: false,
          links: [
            {
              rel: 'self',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/1c783dca-fd54-4ea8-a0fc-2875374e9cb6',
            },
            {
              rel: 'full',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/1c783dca-fd54-4ea8-a0fc-2875374e9cb6?v=full',
            },
          ],
          resourceVersion: '1.8',
        },
      ],
      parentLocation: {
        uuid: 'aff27d58-a15c-49a6-9beb-d30dcfc0c66e',
        display: 'Amani Hospital',
        name: 'Amani Hospital',
        tags: [
          {
            uuid: '37dd4458-dc9e-4ae6-a1f1-789c1162d37b',
            display: 'Visit Location',
            name: 'Visit Location',
            description:
              'Visits are only allowed to happen at locations tagged with this location tag or at locations that descend from a location tagged with this tag.',
            retired: false,
            links: [
              {
                rel: 'self',
                uri:
                  'http://localhost:8080/openmrs/ws/rest/v1/locationtag/37dd4458-dc9e-4ae6-a1f1-789c1162d37b',
              },
              {
                rel: 'full',
                uri:
                  'http://localhost:8080/openmrs/ws/rest/v1/locationtag/37dd4458-dc9e-4ae6-a1f1-789c1162d37b?v=full',
              },
            ],
            resourceVersion: '1.8',
          },
        ],
      },
      childLocations: [],
      retired: false,
      links: [
        {
          rel: 'self',
          uri:
            'http://localhost:8080/openmrs/ws/rest/v1/location/2131aff8-2e2a-480a-b7ab-4ac53250262b',
        },
        {
          rel: 'default',
          uri:
            'http://localhost:8080/openmrs/ws/rest/v1/location/2131aff8-2e2a-480a-b7ab-4ac53250262b?v=default',
        },
      ],
    },
    {
      uuid: '2436bff8-2e2b-480b-b7cb-4ac53250262c',
      display: 'Consultation Ward',
      name: 'Consultation Ward',
      tags: [],
      parentLocation: {
        uuid: 'aff27d58-a15c-49a6-9beb-d30dcfc0c66e',
        display: 'Amani Hospital',
        name: 'Amani Hospital',
        tags: [
          {
            uuid: '37dd4458-dc9e-4ae6-a1f1-789c1162d37b',
            display: 'Visit Location',
            name: 'Visit Location',
            description:
              'Visits are only allowed to happen at locations tagged with this location tag or at locations that descend from a location tagged with this tag.',
            retired: false,
            links: [
              {
                rel: 'self',
                uri:
                  'http://localhost:8080/openmrs/ws/rest/v1/locationtag/37dd4458-dc9e-4ae6-a1f1-789c1162d37b',
              },
              {
                rel: 'full',
                uri:
                  'http://localhost:8080/openmrs/ws/rest/v1/locationtag/37dd4458-dc9e-4ae6-a1f1-789c1162d37b?v=full',
              },
            ],
            resourceVersion: '1.8',
          },
        ],
      },
      childLocations: [],
      retired: false,
      links: [
        {
          rel: 'self',
          uri:
            'http://localhost:8080/openmrs/ws/rest/v1/location/2131aff8-2e2a-480a-b7ab-4ac53250262b',
        },
        {
          rel: 'default',
          uri:
            'http://localhost:8080/openmrs/ws/rest/v1/location/2131aff8-2e2a-480a-b7ab-4ac53250262b?v=default',
        },
      ],
    },
  ],
};

export const successPayload = {
  locations: {
    'b1a8b05e-3542-4037-bbd3-998ee9c40574': {
      uuid: 'b1a8b05e-3542-4037-bbd3-998ee9c40574',
      display: 'Inpatient Ward',
      name: 'Inpatient Ward',
      tags: [
        {
          uuid: '8d4626ca-7abd-42ad-be48-56767bbcf272',
          display: 'Transfer Location',
          name: 'Transfer Location',
          description:
            'Patients may only be transferred (within the hospital) to a location with this tag',
          retired: false,
          links: [
            {
              rel: 'self',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/8d4626ca-7abd-42ad-be48-56767bbcf272',
            },
            {
              rel: 'full',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/8d4626ca-7abd-42ad-be48-56767bbcf272?v=full',
            },
          ],
          resourceVersion: '1.8',
        },
        {
          uuid: 'b8bbf83e-645f-451f-8efe-a0db56f09676',
          display: 'Login Location',
          name: 'Login Location',
          description:
            'When a user logs in and chooses a session location, they may only choose one with this tag',
          retired: false,
          links: [
            {
              rel: 'self',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/b8bbf83e-645f-451f-8efe-a0db56f09676',
            },
            {
              rel: 'full',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/b8bbf83e-645f-451f-8efe-a0db56f09676?v=full',
            },
          ],
          resourceVersion: '1.8',
        },
        {
          uuid: '1c783dca-fd54-4ea8-a0fc-2875374e9cb6',
          display: 'Admission Location',
          name: 'Admission Location',
          description:
            'Patients may only be admitted to inpatient care in a location with this tag',
          retired: false,
          links: [
            {
              rel: 'self',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/1c783dca-fd54-4ea8-a0fc-2875374e9cb6',
            },
            {
              rel: 'full',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/1c783dca-fd54-4ea8-a0fc-2875374e9cb6?v=full',
            },
          ],
          resourceVersion: '1.8',
        },
      ],
      parentLocation: {
        uuid: 'aff27d58-a15c-49a6-9beb-d30dcfc0c66e',
        display: 'Amani Hospital',
        name: 'Amani Hospital',
        tags: [
          {
            uuid: '37dd4458-dc9e-4ae6-a1f1-789c1162d37b',
            display: 'Visit Location',
            name: 'Visit Location',
            description:
              'Visits are only allowed to happen at locations tagged with this location tag or at locations that descend from a location tagged with this tag.',
            retired: false,
            links: [
              {
                rel: 'self',
                uri:
                  'http://localhost:8080/openmrs/ws/rest/v1/locationtag/37dd4458-dc9e-4ae6-a1f1-789c1162d37b',
              },
              {
                rel: 'full',
                uri:
                  'http://localhost:8080/openmrs/ws/rest/v1/locationtag/37dd4458-dc9e-4ae6-a1f1-789c1162d37b?v=full',
              },
            ],
            resourceVersion: '1.8',
          },
        ],
      },
      childLocations: [],
      retired: false,
      links: [
        {
          rel: 'self',
          uri:
            'http://localhost:8080/openmrs/ws/rest/v1/location/b1a8b05e-3542-4037-bbd3-998ee9c40574',
        },
        {
          rel: 'default',
          uri:
            'http://localhost:8080/openmrs/ws/rest/v1/location/b1a8b05e-3542-4037-bbd3-998ee9c40574?v=default',
        },
      ],
    },
    '2131aff8-2e2a-480a-b7ab-4ac53250262b': {
      uuid: '2131aff8-2e2a-480a-b7ab-4ac53250262b',
      display: 'Isolation Ward',
      name: 'Isolation Ward',
      tags: [
        {
          uuid: '8d4626ca-7abd-42ad-be48-56767bbcf272',
          display: 'Transfer Location',
          name: 'Transfer Location',
          description:
            'Patients may only be transferred (within the hospital) to a location with this tag',
          retired: false,
          links: [
            {
              rel: 'self',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/8d4626ca-7abd-42ad-be48-56767bbcf272',
            },
            {
              rel: 'full',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/8d4626ca-7abd-42ad-be48-56767bbcf272?v=full',
            },
          ],
          resourceVersion: '1.8',
        },
        {
          uuid: 'b8bbf83e-645f-451f-8efe-a0db56f09676',
          display: 'Login Location',
          name: 'Login Location',
          description:
            'When a user logs in and chooses a session location, they may only choose one with this tag',
          retired: false,
          links: [
            {
              rel: 'self',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/b8bbf83e-645f-451f-8efe-a0db56f09676',
            },
            {
              rel: 'full',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/b8bbf83e-645f-451f-8efe-a0db56f09676?v=full',
            },
          ],
          resourceVersion: '1.8',
        },
        {
          uuid: '1c783dca-fd54-4ea8-a0fc-2875374e9cb6',
          display: 'Admission Location',
          name: 'Admission Location',
          description:
            'Patients may only be admitted to inpatient care in a location with this tag',
          retired: false,
          links: [
            {
              rel: 'self',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/1c783dca-fd54-4ea8-a0fc-2875374e9cb6',
            },
            {
              rel: 'full',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/1c783dca-fd54-4ea8-a0fc-2875374e9cb6?v=full',
            },
          ],
          resourceVersion: '1.8',
        },
      ],
      parentLocation: {
        uuid: 'aff27d58-a15c-49a6-9beb-d30dcfc0c66e',
        display: 'Amani Hospital',
        name: 'Amani Hospital',
        tags: [
          {
            uuid: '37dd4458-dc9e-4ae6-a1f1-789c1162d37b',
            display: 'Visit Location',
            name: 'Visit Location',
            description:
              'Visits are only allowed to happen at locations tagged with this location tag or at locations that descend from a location tagged with this tag.',
            retired: false,
            links: [
              {
                rel: 'self',
                uri:
                  'http://localhost:8080/openmrs/ws/rest/v1/locationtag/37dd4458-dc9e-4ae6-a1f1-789c1162d37b',
              },
              {
                rel: 'full',
                uri:
                  'http://localhost:8080/openmrs/ws/rest/v1/locationtag/37dd4458-dc9e-4ae6-a1f1-789c1162d37b?v=full',
              },
            ],
            resourceVersion: '1.8',
          },
        ],
      },
      childLocations: [],
      retired: false,
      links: [
        {
          rel: 'self',
          uri:
            'http://localhost:8080/openmrs/ws/rest/v1/location/2131aff8-2e2a-480a-b7ab-4ac53250262b',
        },
        {
          rel: 'default',
          uri:
            'http://localhost:8080/openmrs/ws/rest/v1/location/2131aff8-2e2a-480a-b7ab-4ac53250262b?v=default',
        },
      ],
    },
    '2436bff8-2e2b-480b-b7cb-4ac53250262c': {
      uuid: '2436bff8-2e2b-480b-b7cb-4ac53250262c',
      display: 'Consultation Ward',
      name: 'Consultation Ward',
      tags: [],
      parentLocation: {
        uuid: 'aff27d58-a15c-49a6-9beb-d30dcfc0c66e',
        display: 'Amani Hospital',
        name: 'Amani Hospital',
        tags: [
          {
            uuid: '37dd4458-dc9e-4ae6-a1f1-789c1162d37b',
            display: 'Visit Location',
            name: 'Visit Location',
            description:
              'Visits are only allowed to happen at locations tagged with this location tag or at locations that descend from a location tagged with this tag.',
            retired: false,
            links: [
              {
                rel: 'self',
                uri:
                  'http://localhost:8080/openmrs/ws/rest/v1/locationtag/37dd4458-dc9e-4ae6-a1f1-789c1162d37b',
              },
              {
                rel: 'full',
                uri:
                  'http://localhost:8080/openmrs/ws/rest/v1/locationtag/37dd4458-dc9e-4ae6-a1f1-789c1162d37b?v=full',
              },
            ],
            resourceVersion: '1.8',
          },
        ],
      },
      childLocations: [],
      retired: false,
      links: [
        {
          rel: 'self',
          uri:
            'http://localhost:8080/openmrs/ws/rest/v1/location/2131aff8-2e2a-480a-b7ab-4ac53250262b',
        },
        {
          rel: 'default',
          uri:
            'http://localhost:8080/openmrs/ws/rest/v1/location/2131aff8-2e2a-480a-b7ab-4ac53250262b?v=default',
        },
      ],
    },
  },
  tags: {
    '8d4626ca-7abd-42ad-be48-56767bbcf272': [
      'b1a8b05e-3542-4037-bbd3-998ee9c40574',
      '2131aff8-2e2a-480a-b7ab-4ac53250262b',
    ],
    'b8bbf83e-645f-451f-8efe-a0db56f09676': [
      'b1a8b05e-3542-4037-bbd3-998ee9c40574',
      '2131aff8-2e2a-480a-b7ab-4ac53250262b',
    ],
    '1c783dca-fd54-4ea8-a0fc-2875374e9cb6': [
      'b1a8b05e-3542-4037-bbd3-998ee9c40574',
      '2131aff8-2e2a-480a-b7ab-4ac53250262b',
    ],
  },
  totalCount: 3,
};

describe('Fetch clients', () => {
  const url = '/locations';
  const payload = {};
  const action = fetchLocationsAction(url, payload);

  const generator = cloneableGenerator(fetchLocationsSaga)(action);

  test('fetch all locations success', () => {
    const clone = generator.clone();
    const urlQuery = getUrlQuery(action);
    expect(clone.next().value).toEqual(call(getData, urlQuery));

    expect(clone.next(apiMockResults).value).toEqual(
      put(fetchLocationsSuccessAction(successPayload))
    );

    expect(clone.next().done).toEqual(true);
  });

  test('fetch locations with tag success', () => {
    const tagPayload = { tag: 'Login Location' };
    const actionTag = fetchLocationsAction(url, tagPayload);
    const generatorTag = cloneableGenerator(fetchLocationsSaga)(actionTag);
    const clone = generatorTag.clone();
    const urlQuery = getUrlQuery(actionTag);
    expect(clone.next().value).toEqual(call(getData, urlQuery));

    expect(clone.next(apiMockResults).value).toEqual(
      put(fetchLocationsSuccessAction(successPayload))
    );

    expect(clone.next().done).toEqual(true);
  });

  test('fetch locations fail', () => {
    const clone = generator.clone();
    clone.next();
    expect(clone.throw(new Error('errorMessage')).value).toEqual(
      put(fetchLocationsFailAction('errorMessage'))
    );
  });
});
