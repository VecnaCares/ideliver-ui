import fetchLocationsAction, {
  FETCH_LOCATIONS_ACTION,
} from '../../../../js/features/locations/actions/fetchLocationsAction';

describe('fetchLocationsAction', () => {
  it('should return the correct action', () => {
    const payload = { tag: 'Login locations' };
    const url = '/locations';
    const result = fetchLocationsAction(url, payload);
    const expected = Object.assign(
      {},
      { payload, url, type: FETCH_LOCATIONS_ACTION }
    );
    expect(result).toEqual(expected);
  });
});
