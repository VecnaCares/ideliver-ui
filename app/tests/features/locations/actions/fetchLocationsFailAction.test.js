import fetchLocationsFailAction, {
  FETCH_LOCATIONS_FAILED,
} from '../../../../js/features/locations/actions/fetchLocationsFailAction';

describe('fetchLocationsFailAction', () => {
  it('should return the correct action', () => {
    const payload = 'Error loading locations';
    const result = fetchLocationsFailAction(payload);
    const expected = Object.assign(
      {},
      { payload, type: FETCH_LOCATIONS_FAILED }
    );
    expect(result).toEqual(expected);
  });
});
