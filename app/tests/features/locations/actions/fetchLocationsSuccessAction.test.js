import fetchLocationsSuccessAction, {
  FETCH_LOCATIONS_SUCCEEDED,
} from '../../../../js/features/locations/actions/fetchLocationsSuccessAction';

export const successPayload = {
  locations: {
    'b1a8b05e-3542-4037-bbd3-998ee9c40574': {
      uuid: 'b1a8b05e-3542-4037-bbd3-998ee9c40574',
      display: 'Inpatient Ward',
      name: 'Inpatient Ward',
      tags: [
        {
          uuid: '8d4626ca-7abd-42ad-be48-56767bbcf272',
          display: 'Transfer Location',
          name: 'Transfer Location',
          description:
            'Patients may only be transferred (within the hospital) to a location with this tag',
          retired: false,
          links: [
            {
              rel: 'self',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/8d4626ca-7abd-42ad-be48-56767bbcf272',
            },
            {
              rel: 'full',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/8d4626ca-7abd-42ad-be48-56767bbcf272?v=full',
            },
          ],
          resourceVersion: '1.8',
        },
        {
          uuid: 'b8bbf83e-645f-451f-8efe-a0db56f09676',
          display: 'Login Location',
          name: 'Login Location',
          description:
            'When a user logs in and chooses a session location, they may only choose one with this tag',
          retired: false,
          links: [
            {
              rel: 'self',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/b8bbf83e-645f-451f-8efe-a0db56f09676',
            },
            {
              rel: 'full',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/b8bbf83e-645f-451f-8efe-a0db56f09676?v=full',
            },
          ],
          resourceVersion: '1.8',
        },
        {
          uuid: '1c783dca-fd54-4ea8-a0fc-2875374e9cb6',
          display: 'Admission Location',
          name: 'Admission Location',
          description:
            'Patients may only be admitted to inpatient care in a location with this tag',
          retired: false,
          links: [
            {
              rel: 'self',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/1c783dca-fd54-4ea8-a0fc-2875374e9cb6',
            },
            {
              rel: 'full',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/1c783dca-fd54-4ea8-a0fc-2875374e9cb6?v=full',
            },
          ],
          resourceVersion: '1.8',
        },
      ],
      parentLocation: {
        uuid: 'aff27d58-a15c-49a6-9beb-d30dcfc0c66e',
        display: 'Amani Hospital',
        name: 'Amani Hospital',
        tags: [
          {
            uuid: '37dd4458-dc9e-4ae6-a1f1-789c1162d37b',
            display: 'Visit Location',
            name: 'Visit Location',
            description:
              'Visits are only allowed to happen at locations tagged with this location tag or at locations that descend from a location tagged with this tag.',
            retired: false,
            links: [
              {
                rel: 'self',
                uri:
                  'http://localhost:8080/openmrs/ws/rest/v1/locationtag/37dd4458-dc9e-4ae6-a1f1-789c1162d37b',
              },
              {
                rel: 'full',
                uri:
                  'http://localhost:8080/openmrs/ws/rest/v1/locationtag/37dd4458-dc9e-4ae6-a1f1-789c1162d37b?v=full',
              },
            ],
            resourceVersion: '1.8',
          },
        ],
      },
      childLocations: [],
      retired: false,
      links: [
        {
          rel: 'self',
          uri:
            'http://localhost:8080/openmrs/ws/rest/v1/location/b1a8b05e-3542-4037-bbd3-998ee9c40574',
        },
        {
          rel: 'default',
          uri:
            'http://localhost:8080/openmrs/ws/rest/v1/location/b1a8b05e-3542-4037-bbd3-998ee9c40574?v=default',
        },
      ],
    },
    '2131aff8-2e2a-480a-b7ab-4ac53250262b': {
      uuid: '2131aff8-2e2a-480a-b7ab-4ac53250262b',
      display: 'Isolation Ward',
      name: 'Isolation Ward',
      tags: [
        {
          uuid: '8d4626ca-7abd-42ad-be48-56767bbcf272',
          display: 'Transfer Location',
          name: 'Transfer Location',
          description:
            'Patients may only be transferred (within the hospital) to a location with this tag',
          retired: false,
          links: [
            {
              rel: 'self',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/8d4626ca-7abd-42ad-be48-56767bbcf272',
            },
            {
              rel: 'full',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/8d4626ca-7abd-42ad-be48-56767bbcf272?v=full',
            },
          ],
          resourceVersion: '1.8',
        },
        {
          uuid: 'b8bbf83e-645f-451f-8efe-a0db56f09676',
          display: 'Login Location',
          name: 'Login Location',
          description:
            'When a user logs in and chooses a session location, they may only choose one with this tag',
          retired: false,
          links: [
            {
              rel: 'self',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/b8bbf83e-645f-451f-8efe-a0db56f09676',
            },
            {
              rel: 'full',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/b8bbf83e-645f-451f-8efe-a0db56f09676?v=full',
            },
          ],
          resourceVersion: '1.8',
        },
        {
          uuid: '1c783dca-fd54-4ea8-a0fc-2875374e9cb6',
          display: 'Admission Location',
          name: 'Admission Location',
          description:
            'Patients may only be admitted to inpatient care in a location with this tag',
          retired: false,
          links: [
            {
              rel: 'self',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/1c783dca-fd54-4ea8-a0fc-2875374e9cb6',
            },
            {
              rel: 'full',
              uri:
                'http://localhost:8080/openmrs/ws/rest/v1/locationtag/1c783dca-fd54-4ea8-a0fc-2875374e9cb6?v=full',
            },
          ],
          resourceVersion: '1.8',
        },
      ],
      parentLocation: {
        uuid: 'aff27d58-a15c-49a6-9beb-d30dcfc0c66e',
        display: 'Amani Hospital',
        name: 'Amani Hospital',
        tags: [
          {
            uuid: '37dd4458-dc9e-4ae6-a1f1-789c1162d37b',
            display: 'Visit Location',
            name: 'Visit Location',
            description:
              'Visits are only allowed to happen at locations tagged with this location tag or at locations that descend from a location tagged with this tag.',
            retired: false,
            links: [
              {
                rel: 'self',
                uri:
                  'http://localhost:8080/openmrs/ws/rest/v1/locationtag/37dd4458-dc9e-4ae6-a1f1-789c1162d37b',
              },
              {
                rel: 'full',
                uri:
                  'http://localhost:8080/openmrs/ws/rest/v1/locationtag/37dd4458-dc9e-4ae6-a1f1-789c1162d37b?v=full',
              },
            ],
            resourceVersion: '1.8',
          },
        ],
      },
      childLocations: [],
      retired: false,
      links: [
        {
          rel: 'self',
          uri:
            'http://localhost:8080/openmrs/ws/rest/v1/location/2131aff8-2e2a-480a-b7ab-4ac53250262b',
        },
        {
          rel: 'default',
          uri:
            'http://localhost:8080/openmrs/ws/rest/v1/location/2131aff8-2e2a-480a-b7ab-4ac53250262b?v=default',
        },
      ],
    },
  },
  tags: {
    '8d4626ca-7abd-42ad-be48-56767bbcf272': [
      'b1a8b05e-3542-4037-bbd3-998ee9c40574',
      '2131aff8-2e2a-480a-b7ab-4ac53250262b',
    ],
    'b8bbf83e-645f-451f-8efe-a0db56f09676': [
      'b1a8b05e-3542-4037-bbd3-998ee9c40574',
      '2131aff8-2e2a-480a-b7ab-4ac53250262b',
    ],
    '1c783dca-fd54-4ea8-a0fc-2875374e9cb6': [
      'b1a8b05e-3542-4037-bbd3-998ee9c40574',
      '2131aff8-2e2a-480a-b7ab-4ac53250262b',
    ],
  },
  totalCount: 2,
};

describe('userLoginSuccessAction', () => {
  it('should return the correct action', () => {
    const result = fetchLocationsSuccessAction(successPayload);
    const expected = Object.assign(
      {},
      { payload: successPayload, type: FETCH_LOCATIONS_SUCCEEDED }
    );
    expect(result).toEqual(expected);
  });
});
