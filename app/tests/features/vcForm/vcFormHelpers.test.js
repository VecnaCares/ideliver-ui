import {
  getFormFieldUuidOrRawValue,
  getFormFieldValue,
  shouldFieldBeShown,
  getDerivedValue,
  isDerived,
  evaluateCondition,
  populatedAddress,
  populatedTelephone,
  populateLabourData,
} from '../../../js/features/vcForm/vcFormHelpers';
import { fromJS } from 'immutable';

describe('vcFormHelper tests', () => {
  it('should return form field value', () => {
    let returnedValue = getFormFieldValue(null);
    expect(returnedValue).toBeFalsy();

    returnedValue = getFormFieldValue('abc');
    expect(returnedValue).toEqual('abc');
    returnedValue = getFormFieldValue({ value: '123' });
    expect(returnedValue).toEqual('123');
    returnedValue = getFormFieldValue({ value: false });
    expect(returnedValue).toEqual(false);
    returnedValue = getFormFieldValue({ value: true });
    expect(returnedValue).toEqual(true);
    returnedValue = getFormFieldValue([{ value: '123' }, { value: '124' }]);
    expect(returnedValue).toEqual(['123', '124']);
  });

  it('should return form field uuid or row value', () => {
    let returnedValue = getFormFieldUuidOrRawValue(null);
    expect(returnedValue).toBeFalsy();

    returnedValue = getFormFieldUuidOrRawValue('abc');
    expect(returnedValue).toEqual('abc');

    returnedValue = getFormFieldUuidOrRawValue(true);
    expect(returnedValue).toEqual(true);

    returnedValue = getFormFieldUuidOrRawValue(false);
    expect(returnedValue).toEqual(false);

    returnedValue = getFormFieldUuidOrRawValue({ uuid: '123' });
    expect(returnedValue).toEqual('123');

    returnedValue = getFormFieldUuidOrRawValue([{ uuid: '123' }, false, true]);
    expect(returnedValue).toEqual(['123', false, true]);
  });

  it('should evaluate condition', () => {
    const condition = {
      showIf: {
        field: 'c72ce931-4814-451b-b734-1a1c0a500ddd',
        operator: 'EQUALS',
        value: '572552a7-856c-419b-861b-2794bc5f279b',
      },
    };
    const field = {
      field: { description: JSON.stringify(condition) },
    };
    const fieldToFormFieldMap = {
      'c72ce931-4814-451b-b734-1a1c0a500ddd':
        '96b30d65-5991-40f2-8def-7f89f4bc8846',
      '91100748-f9d0-4165-aad1-82340b2fd8c4':
        'ffdefead-7c1d-4ad9-8e2c-c405dfdd2a55',
    };
    const formData = {
      '96b30d65-5991-40f2-8def-7f89f4bc8846': {
        value: {
          display: 'Normal',
          uuid: 'abead76e-86af-4480-9fd8-74f65c5c0494',
        },
        obsDatetime: '2019-09-05T14:46:55.000-0400',
        uuid: 'e18bbe85-bd2a-4ddf-b795-e3e8fa40290c',
      },
    };

    let result = shouldFieldBeShown(field, fieldToFormFieldMap, formData);
    expect(result).toBe(false);

    const formData2 = {
      '96b30d65-5991-40f2-8def-7f89f4bc8846': {
        value: {
          display: 'Normal',
          uuid: '572552a7-856c-419b-861b-2794bc5f279b',
        },
        obsDatetime: '2019-09-05T14:46:55.000-0400',
        uuid: 'e18bbe85-bd2a-4ddf-b795-e3e8fa40290c',
      },
    };
    result = shouldFieldBeShown(field, fieldToFormFieldMap, formData2);
    expect(result).toBe(true);

    const condition2 = {
      showIf: {
        field: 'c72ce931-4814-451b-b734-1a1c0a500ddd',
        operator: 'NOT_EQUALS',
        value: '572552a7-856c-419b-861b-2794bc5f279b',
      },
    };

    field.field.description = JSON.stringify(condition2);

    result = shouldFieldBeShown(field, fieldToFormFieldMap, formData2);
    expect(result).toBe(false);

    const condition3 = {
      showIf: {
        field: '91100748-f9d0-4165-aad1-82340b2fd8c4',
        operator: 'IN',
        value: [
          '395b8472-bb81-4aa9-8745-37a72948191a',
          '4967bbc9-c975-453c-a192-c9c9db8b5531',
        ],
      },
    };
    field.field.description = JSON.stringify(condition3);

    const formData3 = {
      'ffdefead-7c1d-4ad9-8e2c-c405dfdd2a55': {
        value: [
          {
            display: 'Other modern method',
            uuid: '4967bbc9-c975-453c-a192-c9c9db8b5531',
            voided: false,
          },
        ],
        obsDatetime: '2019-09-05T15:46:19.751-0400',
      },
    };
    result = shouldFieldBeShown(field, fieldToFormFieldMap, formData3);
    expect(result).toBe(true);

    const condition4 = {
      showIf: {
        field: '91100748-f9d0-4165-aad1-82340b2fd8c4',
        operator: 'NOT_IN',
        value: [
          '395b8472-bb81-4aa9-8745-37a72948191a',
          '4967bbc9-c975-453c-a192-c9c9db8b5531',
        ],
      },
    };
    field.field.description = JSON.stringify(condition4);

    result = shouldFieldBeShown(field, fieldToFormFieldMap, formData3);
    expect(result).toBe(false);

    field.field.description = JSON.stringify(condition3);
    result = shouldFieldBeShown(field, fieldToFormFieldMap, {});
    expect(result).toBe(false);
    const condition5 = {
      showIf: {
        field: '91100748-f9d0-4165-aad1-82340b2fd8c4',
        operator: 'NO_EXISTING_OPERATOR',
        value: [
          '395b8472-bb81-4aa9-8745-37a72948191a',
          '4967bbc9-c975-453c-a192-c9c9db8b5531',
        ],
      },
    };
    field.field.description = JSON.stringify(condition5);
    result = shouldFieldBeShown(field, fieldToFormFieldMap, {});
    expect(result).toBe(true);

    const condition6 = {
      showIf: {
        field: 'dedf7c5e-ee6e-4429-ae2b-6a9f27d1e684',
        operator: 'GREATER_THAN',
        value: '28',
      },
    };

    field.field.description = JSON.stringify(condition6);
    result = shouldFieldBeShown(field, fieldToFormFieldMap, {});
    expect(result).toBe(false);
  });

  it('should populate address', () => {
    const addressData = {
      persons: {
        'b36434f6-3276-4d91-8a68-2fbf7f6f3143': {
          preferredAddress: {
            cityVillage: 'CDE',
            stateProvince: 'FGH',
            address1: '123 ABC Street',
            address2: 'GHI Road',
          },
        },
      },
    };
    const client_id = 'b36434f6-3276-4d91-8a68-2fbf7f6f3143';
    const showAddress = populatedAddress(addressData, client_id);
    expect(showAddress).toEqual('');
  });

  it('should populate phoneNumber', () => {
    const phoneData = {
      persons: {
        'b36434f6-3276-4d91-8a68-2fbf7f6f3143': {
          attributes: [
            {
              uuid: '455a8716-e5d7-41c3-9d4c-f383eb446b40',
              display: 'Telephone Number = 9999300804',
              value: '9999300804',
              attributeType: '14d4f066-15f5-102d-96e4-000c29c2a5d7',
              voided: false,
            },
          ],
        },
      },
    };
    const client_id = 'b36434f6-3276-4d91-8a68-2fbf7f6f3143';
    const showPhoneNumber = populatedTelephone(phoneData, client_id);
    expect(showPhoneNumber).toEqual(undefined);
  });

  const formData = {
    formData: {
      '200BADDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD': {
        'dd038cc4-1a53-4738-8967-aaa000000030': {
          value: 3500,
          obsDatetime: '2020-07-31T18:13:39.000+0530',
          comment: '{"formField":"dd038cc4-1a53-4738-8967-aaa000000030"}',
          uuid: '0de7be4f-392c-438e-bbe4-6b86c8e0d707',
        },
        'dd038cc4-1a53-4738-8967-aaa000000012': {
          value: true,
          obsDatetime: '2020-07-31T18:13:42.000+0530',
          comment: '{"formField":"dd038cc4-1a53-4738-8967-aaa000000012"}',
          uuid: '90b26814-1d0b-4dce-a59e-58ae184e84fd',
        },

        encounterDatetime: '2020-07-31T18:13:45.000+0530',
        encounterUuid: '109577b3-94c0-4a4f-96d0-bb78e799e943',
        '206c3d9b-a3ff-4b4b-86b8-d72c4897112b': {
          value: {
            set: false,
            display: '4',
            uuid: '146702e4-bf7e-4b53-9108-85baa9c313b3',
          },
          obsDatetime: '2020-07-31T18:12:40.000+0530',
          comment: '{"formField":"206c3d9b-a3ff-4b4b-86b8-d72c4897112b"}',
          uuid: '4e3e491b-bcad-443d-b07c-d2de78aa16d0',
        },
        '82752bd4-6405-4b23-b2c0-62a0a6fb86a2': {
          value: {
            set: false,
            display: '5',
            uuid: 'a9d3ced0-d4fb-43b6-9cbf-2665789d2eeb',
          },
          obsDatetime: '2020-07-31T18:12:44.000+0530',
          comment: '{"formField":"82752bd4-6405-4b23-b2c0-62a0a6fb86a2"}',
          uuid: '9a6da8bb-dcf9-444c-9b8c-39a185464c95',
        },
        '522fec88-20f3-44b7-b6b2-0363edd3e1a0': {
          value: {
            set: false,
            display: '6',
            uuid: '94d5dd74-6c98-4abb-82d6-e3fcdbaf6208',
          },
          obsDatetime: '2020-07-31T18:12:46.000+0530',
          comment: '{"formField":"522fec88-20f3-44b7-b6b2-0363edd3e1a0"}',
          uuid: '1a98c05b-dd2b-4917-8586-4e52baa62f79',
        },
      },
      '71a643f5-63e1-439f-a8a6-cb4f2bced721': {},
    },
  };
  it('should populate APGAR1', () => {
    const formId = '200BADDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD';
    const formFieldId = '206c3d9b-a3ff-4b4b-86b8-d72c4897112b';
    const showAPGAR1 = populateLabourData(formData, formId, formFieldId);
    expect(showAPGAR1).toEqual('');
  });
});
