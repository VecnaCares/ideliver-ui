import { cloneableGenerator } from '@redux-saga/testing-utils';
import { put, call } from 'redux-saga/effects';
import userLoginAction from '../../../../js/features/userLogin/actions/userLoginAction';
import userLoginSaga, {
  getUrlQuery,
} from '../../../../js/features/userLogin/sagas/userLoginSaga';
import { deleteData, postData } from '../../../../js/api';
import userLoginSuccessAction from '../../../../js/features/userLogin/actions/userLoginSuccessAction';

import userLoginFailAction from '../../../../js/features/userLogin/actions/userLoginFailAction';
import userLogoutAction from '../../../../js/features/userLogin/actions/userLogoutAction';
import userLogoutSuccessAction from '../../../../js/features/userLogin/actions/userLogoutSuccessAction';
import userLogoutSaga from '../../../../js/features/userLogin/sagas/userLogoutSaga';
import React from 'react';
import { Redirect } from 'react-router-dom';
import userLogoutFailAction from '../../../../js/features/userLogin/actions/userLogoutFailAction';

describe('User login Saga', () => {
  const url = 'testUrl';
  const apiAuthorization = 'apiToken';
  const action = userLogoutAction(url, { apiAuthorization });

  const generator = cloneableGenerator(userLogoutSaga)(action);

  test('logout success', () => {
    const clone = generator.clone();
    const expected = call(deleteData, getUrlQuery(action), apiAuthorization);
    expect(clone.next().value).toEqual(expected);

    const session = { authenticated: false };
    const user = Object.assign({}, { session }, { apiAuthorization: null });
    const returned = put(userLogoutSuccessAction(user));
    expect(clone.next(session).value).toEqual(returned);
    const redirect = <Redirect to="/" />;
    expect(clone.next().value).toEqual(redirect);
    expect(clone.next().done).toEqual(true);
  });

  test('logout failure', () => {
    const clone = generator.clone();
    clone.next();
    expect(clone.throw(new Error('errorMessage')).value).toEqual(
      put(userLogoutFailAction('errorMessage'))
    );
  });
});
