import { cloneableGenerator } from '@redux-saga/testing-utils';
import { put, call } from 'redux-saga/effects';
import userLoginAction from '../../../../js/features/userLogin/actions/userLoginAction';
import userLoginSaga, {
  getUrlQuery,
} from '../../../../js/features/userLogin/sagas/userLoginSaga';
import { postData } from '../../../../js/api';
import userLoginSuccessAction from '../../../../js/features/userLogin/actions/userLoginSuccessAction';

import { successPayload } from '../actions/userLoginSuccessAction.test';
import userLoginFailAction from '../../../../js/features/userLogin/actions/userLoginFailAction';

describe('User login Saga', () => {
  const url = 'testUrl';
  const location = 'unknown';
  const payload = { username: 'test', password: 'pass', location };
  const action = userLoginAction(url, payload);

  const generator = cloneableGenerator(userLoginSaga)(action);

  test('login success', () => {
    const clone = generator.clone();
    const apiAuthorization = `Basic ${btoa(
      `${payload.username}:${payload.password}`
    )}`;
    const expected = call(
      postData,
      getUrlQuery(action),
      { location },
      apiAuthorization
    );
    expect(clone.next().value).toEqual(expected);

    successPayload.apiAuthorization = apiAuthorization;
    const { session } = successPayload;
    const returned = put(userLoginSuccessAction(successPayload));
    expect(clone.next(session).value).toEqual(returned);
    expect(clone.next().done).toEqual(true);
  });

  test('login wrong credentials', () => {
    const clone = generator.clone();
    const apiAuthorization = `Basic ${btoa(
      `${payload.username}:${payload.password}`
    )}`;
    const expected = call(
      postData,
      getUrlQuery(action),
      { location },
      apiAuthorization
    );
    expect(clone.next().value).toEqual(expected);

    successPayload.apiAuthorization = apiAuthorization;
    successPayload.session.authenticated = false;
    const { session } = successPayload;
    const returned = put(userLoginFailAction('loginFailed'));
    expect(clone.next(session).value).toEqual(returned);
    expect(clone.next().done).toEqual(true);
  });

  test('login failure', () => {
    const clone = generator.clone();
    clone.next();
    expect(clone.throw(new Error()).value).toEqual(
      put(userLoginFailAction('loginFailed'))
    );
  });
});
