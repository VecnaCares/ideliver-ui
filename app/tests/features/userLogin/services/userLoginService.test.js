import {
  API_AUTHORIZATION,
  getApiAuthorization,
  getLocation,
  getLogInUrl,
  isAuthenticated,
  logOut,
  SELECTED_LOCATION,
} from '../../../../js/features/userLogin/services/userLoginService';

describe('userLoginService', () => {
  beforeEach(() => {
    localStorage.removeItem(API_AUTHORIZATION);
    localStorage.removeItem(SELECTED_LOCATION);
  });

  it('should return the login url', () => {
    expect(getLogInUrl('/')).toEqual('/appui/session');
  });

  it('should return the authorization key', () => {
    localStorage.setItem(API_AUTHORIZATION, 'test');
    const apiAuthorization = getApiAuthorization();
    expect(apiAuthorization).toEqual('test');
  });

  it('should return the location uuid', () => {
    localStorage.setItem(SELECTED_LOCATION, 'test');
    const apiAuthorization = getLocation();
    expect(apiAuthorization).toEqual('test');
  });

  it('should check local storage for authentication token', () => {
    expect(isAuthenticated()).toEqual(false);
    localStorage.setItem(API_AUTHORIZATION, 'test');
    expect(isAuthenticated()).toEqual(false);
    localStorage.setItem(SELECTED_LOCATION, 'test');
    expect(isAuthenticated()).toEqual(true);
  });

  it('should remove authorization token from local storage', () => {
    localStorage.setItem(SELECTED_LOCATION, 'test');
    localStorage.setItem(API_AUTHORIZATION, 'test');
    expect(isAuthenticated()).toEqual(true);
    logOut();
    expect(isAuthenticated()).toEqual(false);
  });
});
