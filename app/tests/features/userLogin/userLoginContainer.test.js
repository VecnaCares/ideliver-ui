import { createMockStore } from 'redux-test-utils';
import React from 'react';
import { fromJS } from 'immutable';
import { defaultState as userState } from '../../../js/features/userLogin/userReducer';
import { successPayload as locationsState } from '../locations/sagas/fetchLocationsSaga.test';
import UserLoginContainer from '../../../js/features/userLogin/userLoginContainer';
import UserLogin from '../../../js/features/userLogin/userLogin';
import { shallowWithIntl } from '../../enzyme-test-helpers';
import userLoginAction from '../../../js/features/userLogin/actions/userLoginAction';
import { REST_API_PATHNAME, REST_API_VERSION } from '../../../js/paths';
import fetchLocationsAction from '../../../js/features/locations/actions/fetchLocationsAction';

describe('UserLoginContainer', () => {
  let store, component, testState;

  beforeEach(() => {
    testState = {
      User: fromJS(userState),
      Locations: fromJS(locationsState),
    };
    store = createMockStore(testState);
    component = shallowWithIntl(<UserLoginContainer store={store} />)
      .find(UserLogin)
      .dive();
  });

  it('should render without throwing crashing', () => {
    expect(component).toBeDefined();
  });

  it('should expose props from state', () => {
    expect(component.props().submitting).toEqual(false);
    expect(component.props().isAuthenticated).toEqual(false);
    expect(component.props().loginErrors).toEqual([]);
    expect(component.props().onSubmit).toBeDefined();
    expect(component.props().fetchLocations).toBeDefined();
    expect(component.props().loginLocations).toBeDefined();
  });

  it('should dispatch userLoginAction', () => {
    const data = { username: 'test', password: 'pass' };
    component.props().onSubmit(data);
    const actions = store.getActions();
    const expectedAction = userLoginAction(
      `${REST_API_PATHNAME}${REST_API_VERSION}`,
      data
    );
    expect(actions).toEqual([expectedAction]);
  });

  it('should dispatch fetchLocationsAction', () => {
    const data = { tag: 'Login Location' };
    component.props().fetchLocations(data);
    const actions = store.getActions();
    const expectedAction = fetchLocationsAction(
      `${REST_API_PATHNAME}${REST_API_VERSION}`,
      data
    );
    expect(actions).toEqual([expectedAction]);
  });
});
