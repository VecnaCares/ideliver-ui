import React from 'react';
import { act } from 'react-dom/test-utils';
import { IntlProvider } from 'react-intl';
import { createMockStore } from 'redux-test-utils';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import UserLogin from '../../../js/features/userLogin/userLogin';
import {
  mountWithRouterAndIntl,
  shallowWithIntl,
} from '../../enzyme-test-helpers';

describe('UserLogin', () => {
  const intlProvider = new IntlProvider({ locale: 'en' }, {});
  const { intl } = intlProvider.getChildContext();
  let mockFunc1;
  let mockFunc2;
  const testState = {};
  let store;
  let component;
  beforeEach(() => {
    mockFunc1 = jest.fn();
    mockFunc2 = jest.fn();
    store = createMockStore(testState);
    component = mountWithRouterAndIntl(
      <Provider store={store}>
        <MemoryRouter initialEntries={['/login']}>
          <UserLogin.WrappedComponent
            intl={intl}
            history={{ push: jest.fn() }}
            location={{ pathname: '/login' }}
            onSubmit={mockFunc1}
            fetchLocations={mockFunc2}
            loginLocations={[]}
          />
        </MemoryRouter>
      </Provider>
    );
  });

  it('should render without crashing', () => {
    expect(component).toBeDefined();

    const textFields = component.find('VcTextField');
    const username = textFields.at(0);
    const password = textFields.at(1);
    const location = component.find('VcDropDown').at(0);
    const login = component.find('VcButton');

    expect(username).toBeDefined();
    expect(password).toBeDefined();
    expect(location).toBeDefined();
    expect(login).toBeDefined();
  });

  it('should validate required fields', () => {
    expect(component).toBeDefined();
    const login = component.find('VcButton');
    expect(login).toBeDefined();
    login.simulate('click');

    const textFields = component.find('VcTextField');
    const username = textFields.at(0);
    expect(username).toBeDefined();
    expect(username.props().error).toEqual(true);
    const password = textFields.at(1);
    expect(password).toBeDefined();
    expect(password.props().error).toEqual(true);
    const location = component.find('VcDropDown').at(0);
    expect(location).toBeDefined();
    expect(location.props().error).toEqual(true);

    expect(mockFunc1.mock.calls.length).toEqual(0);
  });

  it('should fetch locations once', () => {
    expect(component).toBeDefined();
    expect(mockFunc2.mock.calls.length).toEqual(1);
  });

  it('should call onSubmit if validation passes', () => {
    expect(component).toBeDefined();

    const textFields = component.find('VcTextField');
    const username = textFields.at(0);
    const password = textFields.at(1);
    const location = component.find('VcDropDown').at(0);
    const login = component.find('VcButton');

    act(() => {
      username.props().onChange('testUsername');
      password.props().onChange('testPassword');
      location.props().onChange('locationUuId');
    });
    login.simulate('click');

    expect(mockFunc1.mock.calls.length).toEqual(1);
  });

  it('should redirect to home page if the user is already logged in', () => {
    const loggedIn = shallowWithIntl(
      <MemoryRouter initialEntries={['/login']}>
        <UserLogin.WrappedComponent
          intl={intl}
          history={{ push: jest.fn() }}
          location={{ pathname: '/login' }}
          onSubmit={mockFunc1}
          isAuthenticated
          loginErrors={[]}
        />
      </MemoryRouter>
    );

    expect(loggedIn).toBeDefined();
    const userLogin = loggedIn.find('UserLogin');
    expect(userLogin.children().length).toEqual(0);
  });
});
