import React from 'react';
import { fromJS } from 'immutable';
import { createMockStore } from 'redux-test-utils';
import PrivateRouteContainer, {
  PrivateRouteRenderer,
} from '../../../js/features/userLogin/privateRoute';
import { shallowWithIntl } from '../../enzyme-test-helpers';
import { REST_API_PATHNAME, REST_API_VERSION } from '../../../js/paths';
import fetchSessionAction from '../../../js/features/main/actions/fetchSessionAction';
import userLogoutAction from '../../../js/features/userLogin/actions/userLogoutAction';
import { shallow } from 'enzyme';
import * as userLoginService from '../../../js/features/userLogin/services/userLoginService';

describe('Private Route test', () => {
  function setup(customProps) {
    const props = {
      component: () => <h1>Test</h1>,
      isAuthenticated: false,
      session: {},
      fetchSession: jest.fn(),
      logout: jest.fn(),
      ...customProps,
    };

    const wrapper = shallow(<PrivateRouteRenderer {...props} />);

    return {
      wrapper,
      props,
    };
  }

  it('passes props to Route component', () => {
    const { wrapper } = setup({ path: '/registration' });
    expect(wrapper.find('Route').prop('path')).toBe('/registration');
  });

  it('redirects unauthenticated users to login', () => {
    const { wrapper } = setup();
    const ComponentToRender = wrapper.prop('render');
    const redirectWrapper = shallow(
      <ComponentToRender location="/registration" />
    );
    expect(redirectWrapper.find('Redirect').props()).toEqual({
      to: {
        pathname: '/login',
        state: { from: '/registration' },
      },
    });
  });

  it('displays passed component to authenticated users', () => {
    userLoginService.isAuthenticated = jest.fn(() => true);
    const { wrapper, props } = setup({ isAuthenticated: true });
    const ComponentToRender = wrapper.prop('render');
    const componentWrapper = shallow(
      <ComponentToRender location="/registration" />
    );
    expect(componentWrapper.is(props.component)).toBe(true);
    expect(componentWrapper.props()).toEqual({
      location: '/registration',
      isAuthenticated: true,
      session: props.session,
      fetchSession: props.fetchSession,
      logout: props.logout,
    });
  });

  it('fetches the user session if user is authenticated', () => {
    userLoginService.isAuthenticated = jest.fn(() => true);
    const { wrapper, props } = setup({ isAuthenticated: true, session: null });
    const ComponentToRender = wrapper.prop('render');
    const componentWrapper = shallow(
      <ComponentToRender location="/registration" />
    );
    expect(componentWrapper.is(props.component)).toBe(true);
    expect(props.fetchSession.mock.calls.length).toEqual(1);
  });
});

describe('Private Route Container test', () => {
  let store, testState;

  function setup(customProps, includeSession = true) {
    const session = includeSession ? { user: { display: 'test' } } : null;
    testState = {
      User: fromJS({
        session,
      }),
    };
    store = createMockStore(testState);
    const props = {
      component: () => <h1>Test</h1>,
      isAuthenticated: false,
      ...customProps,
    };
    const wrapper = shallowWithIntl(
      <PrivateRouteContainer store={store} {...props} />
    );

    return {
      wrapper,
      props,
    };
  }

  it('passes props to Route component', () => {
    const { wrapper } = setup({ path: '/registration' });
    const component = wrapper.find(PrivateRouteRenderer);
    expect(component.props().path).toBe('/registration');
  });

  it('should expose props from state', () => {
    const { wrapper } = setup({ path: '/registration' });
    const component = wrapper.find(PrivateRouteRenderer);
    expect(component.props().session).toEqual(
      testState.User.get('session').toJS()
    );
    expect(component.props().fetchSession).toBeDefined();
    expect(component.props().logout).toBeDefined();

    const { wrapper: wrapper1 } = setup({ path: '/registration' }, false);
    const component1 = wrapper1.find(PrivateRouteRenderer);
    expect(component1.props().session).toEqual(null);
  });

  it('should dispatch userLoginAction', () => {
    const data = { apiAuthorization: null, location: null };
    const { wrapper } = setup({ path: '/registration' });
    const component = wrapper.find(PrivateRouteRenderer);
    component.props().fetchSession();
    const actions = store.getActions();
    const expectedAction = fetchSessionAction(
      `${REST_API_PATHNAME}${REST_API_VERSION}`,
      data
    );
    expect(actions).toEqual([expectedAction]);
  });

  it('should dispatch userLogoutAction', () => {
    const data = { apiAuthorization: null, location: null };
    const { wrapper } = setup({ path: '/registration' });
    const component = wrapper.find(PrivateRouteRenderer);
    component.props().logout();
    const actions = store.getActions();
    const expectedAction = userLogoutAction(
      `${REST_API_PATHNAME}${REST_API_VERSION}`,
      data
    );
    expect(actions).toEqual([expectedAction]);
  });
});
