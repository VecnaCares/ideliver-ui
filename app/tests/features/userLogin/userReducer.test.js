import { fromJS } from 'immutable';
import userReducer, {
  defaultState,
} from '../../../js/features/userLogin/userReducer';
import userLoginSuccessAction from '../../../js/features/userLogin/actions/userLoginSuccessAction';
import { successPayload } from './actions/userLoginSuccessAction.test';
import userLoginFailAction from '../../../js/features/userLogin/actions/userLoginFailAction';
import userLoginAction from '../../../js/features/userLogin/actions/userLoginAction';
import userLogoutSuccessAction from '../../../js/features/userLogin/actions/userLogoutSuccessAction';
import userLogoutFailAction from '../../../js/features/userLogin/actions/userLogoutFailAction';
import fetchSessionSuccessAction from '../../../js/features/main/actions/fetchSessionSuccessAction';
import fetchSessionFailAction from '../../../js/features/main/actions/fetchSessionFailAction';

describe('UserReducer', () => {
  it('should return the initial state', () => {
    const newState = userReducer(undefined, {});
    expect(newState).toEqual(fromJS(defaultState));
  });

  it('should return the state after login success action', () => {
    const newState = userReducer(
      fromJS(defaultState),
      userLoginSuccessAction(successPayload)
    );
    const expectedState = fromJS(successPayload)
      .setIn(['submitting'], false)
      .setIn(['isAuthenticated'], true)
      .setIn(['errors'], fromJS([]));
    expect(newState).toEqual(expectedState);
  });

  it('should return the state after login fail action', () => {
    const newState = userReducer(
      fromJS(defaultState),
      userLoginFailAction('loginFailed')
    );
    const expectedState = fromJS(defaultState)
      .setIn(['isAuthenticated'], false)
      .setIn(['submitting'], false)
      .setIn(['errors'], fromJS(['loginFailed']));
    expect(newState).toEqual(expectedState);
  });

  it('should return the state after login action', () => {
    const newState = userReducer(
      fromJS(defaultState),
      userLoginAction('url', {})
    );
    const expectedState = fromJS(defaultState).setIn(['submitting'], true);
    expect(newState).toEqual(expectedState);
  });

  it('should return the state after logout success action', () => {
    const newState = userReducer(
      fromJS(defaultState),
      userLogoutSuccessAction({})
    );
    const expectedState = fromJS(defaultState)
      .setIn(['session'], null)
      .setIn(['isAuthenticated'], false);
    expect(newState).toEqual(expectedState);
  });

  it('should return the state after logout failed action', () => {
    const newState = userReducer(
      fromJS(defaultState),
      userLogoutFailAction('loginFailed')
    );
    const expectedState = fromJS(defaultState)
      .setIn(['isAuthenticated'], true)
      .setIn(['errors'], fromJS(['loginFailed']));
    expect(newState).toEqual(expectedState);
  });

  it('should return the state after fetch session success action', () => {
    const { session } = successPayload;
    const newState = userReducer(
      fromJS(defaultState),
      fetchSessionSuccessAction(session)
    );
    const expectedState = fromJS(defaultState)
      .setIn(['session'], fromJS(session))
      .setIn(['isAuthenticated'], true);
    expect(newState).toEqual(expectedState);
  });

  it('should return the state after fetch session failed action', () => {
    const newState = userReducer(
      fromJS(defaultState),
      fetchSessionFailAction('error')
    );
    const expectedState = fromJS(defaultState)
      .setIn(['isAuthenticated'], false)
      .setIn(['errors'], fromJS(['error']));
    expect(newState).toEqual(expectedState);
  });
});
