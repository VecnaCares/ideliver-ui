import userLogoutAction, {
  USER_LOGOUT_ACTION,
} from '../../../../js/features/userLogin/actions/userLogoutAction';

describe('userLogoutAction', () => {
  it('should return the correct action', () => {
    const payload = { apiAuthorization: 'apiToke' };
    const url = 'testUrl';
    const result = userLogoutAction(url, payload);
    const expected = Object.assign(
      {},
      { payload, url, type: USER_LOGOUT_ACTION }
    );
    expect(result).toEqual(expected);
  });
});
