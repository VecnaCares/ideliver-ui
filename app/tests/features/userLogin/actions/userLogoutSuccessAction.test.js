import userLogoutSuccessAction, {
  USER_LOGOUT_SUCCEEDED,
} from '../../../../js/features/userLogin/actions/userLogoutSuccessAction';

describe('userLogoutSuccessAction', () => {
  it('should return the correct action', () => {
    const payload = { session: { authenticated: false } };
    const result = userLogoutSuccessAction(payload);
    const expected = Object.assign(
      {},
      { payload, type: USER_LOGOUT_SUCCEEDED }
    );
    expect(result).toEqual(expected);
  });
});
