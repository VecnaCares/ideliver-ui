import userLoginAction, {
  USER_LOGIN_ACTION,
} from '../../../../js/features/userLogin/actions/userLoginAction';

describe('userLoginAction', () => {
  it('should return the correct action', () => {
    const payload = { username: 'test', password: 'pass', location: 'unknown' };
    const url = 'testUrl';
    const result = userLoginAction(url, payload);
    const expected = Object.assign(
      {},
      { payload, url, type: USER_LOGIN_ACTION }
    );
    expect(result).toEqual(expected);
  });
});
