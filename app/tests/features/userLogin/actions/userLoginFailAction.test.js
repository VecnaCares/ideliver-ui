import userLoginFailAction, {
  USER_LOGIN_FAILED,
} from '../../../../js/features/userLogin/actions/userLoginFailAction';

describe('userLoginFailAction', () => {
  it('should return the correct action', () => {
    const payload = 'Unable to log in';
    const result = userLoginFailAction(payload);
    const expected = Object.assign({}, { payload, type: USER_LOGIN_FAILED });
    expect(result).toEqual(expected);
  });
});
