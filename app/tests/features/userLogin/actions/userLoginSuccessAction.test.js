import userLoginSuccessAction, {
  USER_LOGIN_SUCCEEDED,
} from '../../../../js/features/userLogin/actions/userLoginSuccessAction';

export const successPayload = {
  apiAuthorization: 'Basic YWRtaW46QWRtaW4xMjM=',
  session: {
    authenticated: true,
    sessionLocation: {
      display: 'Amani Hospital',
      name: 'Amani Hospital',
      uuid: 'aff27d58-a15c-49a6-9beb-d30dcfc0c66e',
    },
    user: {
      display: 'admin',
      uuid: '1c3db49d-440a-11e6-a65c-00e04c680037',
      systemId: 'admin',
    },
  },
};

describe('userLoginSuccessAction', () => {
  it('should return the correct action', () => {
    const result = userLoginSuccessAction(successPayload);
    const expected = Object.assign(
      {},
      { payload: successPayload, type: USER_LOGIN_SUCCEEDED }
    );
    expect(result).toEqual(expected);
  });
});
