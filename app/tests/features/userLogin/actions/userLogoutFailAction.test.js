import userLogoutFailAction, {
  USER_LOGOUT_FAILED,
} from '../../../../js/features/userLogin/actions/userLogoutFailAction';

describe('userLogoutFailAction', () => {
  it('should return the correct action', () => {
    const payload = 'Unable to log in';
    const result = userLogoutFailAction(payload);
    const expected = Object.assign({}, { payload, type: USER_LOGOUT_FAILED });
    expect(result).toEqual(expected);
  });
});
