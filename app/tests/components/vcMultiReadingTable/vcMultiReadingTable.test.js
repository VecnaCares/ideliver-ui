import React from 'react';
import VcMultiReadingTable from '../../../js/components/vcMultiReadingTable/vcMultiReadingTable';
import { mountWithRouterAndIntl } from '../../enzyme-test-helpers';
import VcDateTime from '../../../js/components/vcDateTime/vcDateTime';
describe('VcmultiForm', () => {
  let mockFunc1;
  let mockFunc2;
  let mockFunc3;
  mockFunc2 = jest.fn();
  mockFunc3 = jest.fn();

  const allData = {
    index: 6,
    data: [
      {
        DDDDDDDDDDDDDDDDDDDDDDDDDD: {
          comment: '{"formField":"ee038cc4-1a53-4738-8967-aaa000000088"}',
          obsDatetime: '2020-08-10T16:39:19.000+0530',
          uuid: '9100cfaa-1f75-410e-86e0-921e6af1d783',
          value: 10,
        },
        encounterDatetime: '2020-08-10T16:39:33.000+0530',
        encounterUuid: '959517a9-8e12-49ff-aaac-419d76c4c7a4',
        lastUpdated: '2020-08-10T16:39:33.000+0530',
      },
      {
        DDDDDDDDDDDDDDDDDDDDDDDDDD: {
          comment: '{"formField":"ee038cc4-1a53-4738-8967-aaa000000088"}',
          obsDatetime: '2020-08-10T16:39:19.000+0530',
          uuid: '9100cfaa-1f75-410e-86e0-921e6af1d783',
          value: 10,
        },
        encounterDatetime: '2020-08-10T16:39:33.000+0530',
        encounterUuid: '959517a9-8e12-49ff-aaac-419d76c4c7a4',
        lastUpdated: '2020-08-10T16:39:33.000+0530',
      },
      {
        DDDDDDDDDDDDDDDDDDDDDDDDDD: {
          comment: '{"formField":"ee038cc4-1a53-4738-8967-aaa000000088"}',
          obsDatetime: '2020-08-10T16:39:19.000+0530',
          uuid: '9100cfaa-1f75-410e-86e0-921e6af1d783',
          value: 10,
        },
        encounterDatetime: '2020-08-10T16:39:33.000+0530',
        encounterUuid: '959517a9-8e12-49ff-aaac-419d76c4c7a4',
        lastUpdated: '2020-08-10T16:39:33.000+0530',
      },
      {
        DDDDDDDDDDDDDDDDDDDDDDDDDD: {
          comment: '{"formField":"ee038cc4-1a53-4738-8967-aaa000000088"}',
          obsDatetime: '2020-08-10T16:39:19.000+0530',
          uuid: '9100cfaa-1f75-410e-86e0-921e6af1d783',
          value: 10,
        },
        encounterDatetime: '2020-08-10T16:39:33.000+0530',
        encounterUuid: '959517a9-8e12-49ff-aaac-419d76c4c7a4',
        lastUpdated: '2020-08-10T16:39:33.000+0530',
      },
      {
        DDDDDDDDDDDDDDDDDDDDDDDDDD: {
          comment: '{"formField":"ee038cc4-1a53-4738-8967-aaa000000088"}',
          obsDatetime: '2020-08-10T16:39:19.000+0530',
          uuid: '9100cfaa-1f75-410e-86e0-921e6af1d783',
          value: 10,
        },
        encounterDatetime: '2020-08-10T16:39:33.000+0530',
        encounterUuid: '959517a9-8e12-49ff-aaac-419d76c4c7a4',
        lastUpdated: '2020-08-10T16:39:33.000+0530',
      },
      {
        DDDDDDDDDDDDDDDDDDDDDDDDDD: {
          comment: '{"formField":"ee038cc4-1a53-4738-8967-aaa000000088"}',
          obsDatetime: '2020-08-10T16:39:19.000+0530',
          uuid: '9100cfaa-1f75-410e-86e0-921e6af1d783',
          value: 10,
        },
        encounterDatetime: '2020-08-10T16:39:33.000+0530',
        encounterUuid: '959517a9-8e12-49ff-aaac-419d76c4c7a4',
        lastUpdated: '2020-08-10T16:39:33.000+0530',
      },
    ],
    onChange: mockFunc2,
  };

  beforeEach(() => {
    mockFunc1 = jest.fn();
  });

  it('should render', () => {
    const component = mountWithRouterAndIntl(
      <VcMultiReadingTable.WrappedComponent
        onChange={mockFunc2}
        allData={allData}
        fieldsHideMap={{}}
        getExtraFieldsToHide={mockFunc1}
        formFieldsMap={{}}
      />
    );

    expect(component).toBeDefined();
    const dateTime = component.find(VcDateTime).at(0);
    dateTime.props().onChange('value');
  });
});
