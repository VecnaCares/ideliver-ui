import React from 'react';
import { IntlProvider } from 'react-intl';
import lolex from 'lolex';
import { mountWithIntl } from '../../enzyme-test-helpers';
import VcTimer from '../../../js/components/vcTimer/vcTimer';

describe('VcTimer', () => {
  // Construct a new `IntlProvider` instance by passing `props` and
  // `context` as React would, then call `getChildContext()` to get the
  // React Intl API, complete with the `format*()` functions.
  let mockFunc;
  beforeEach(() => {
    mockFunc = jest.fn();
  });
  const intlProvider = new IntlProvider({ locale: 'en' }, {});
  const { intl } = intlProvider.getChildContext();

  it('renders with value 30', () => {
    const component = mountWithIntl(
      <VcTimer.WrappedComponent intl={intl} value={30} />
    );
    expect(component).toBeDefined();
    expect(component.state()).toEqual({ next: 30, percentage: 50 });
  });
  it('renders with value 120', () => {
    const component = mountWithIntl(
      <VcTimer.WrappedComponent intl={intl} value={120} />
    );
    expect(component).toBeDefined();
    expect(component.state()).toEqual({ next: 120, percentage: 100 });
  });
  it('moves the state with 30 cycles and puts the percentage at half with value 60', () => {
    const clock = lolex.install();
    const component = mountWithIntl(
      <VcTimer.WrappedComponent
        intl={intl}
        value={60}
        tickLengthInMs={1}
        onOverdue={mockFunc}
      />
    );
    clock.tick(30);
    expect(component.state()).toEqual({ next: 30, percentage: 50 });
    expect(mockFunc.mock.calls.length).toBe(0);
    clock.uninstall();
  });
  it('the timer stops at 0 cycles and 0 percentage state with value 15', () => {
    const clock = lolex.install();
    const component = mountWithIntl(
      <VcTimer.WrappedComponent
        intl={intl}
        value={15}
        tickLengthInMs={1}
        onOverdue={mockFunc}
      />
    );
    clock.tick(1000);
    expect(component.state()).toEqual({ next: 0, percentage: 0 });
    expect(mockFunc.mock.calls.length).toBe(1);
    component.unmount();
    clock.uninstall();
  });
});
