import React from 'react';
import { mount } from 'enzyme';
import { mountWithIntl } from '../../enzyme-test-helpers';
import VcUserProfile from '../../../js/components/vcUserProfile/vcUserProfile';

describe('VcUserProfile', () => {
  it('should render without crashing', () => {
    const component = mountWithIntl(<VcUserProfile />);
    expect(component).toBeDefined();
    const menu = component.find('Menu').get(0);
    expect(menu).toBeUndefined();
    expect(component.html()).toBe('');
  });

  it('should render user profile and menu', () => {
    const component = mountWithIntl(
      <VcUserProfile user={{ display: 'test' }} />
    );
    expect(component).toBeDefined();
    const menu = component.find('Menu').get(0);
    expect(menu).toBeDefined();
    expect(menu.props.children.length).toEqual(3);
    const name = component.find('Typography').get(0);
    const temp = component.find('FormattedMessage').get(0);
    expect(temp).toBeDefined();
    //expect(name.props.children).toEqual('test');
    const icon = component.find('IconButton').get(0);
    expect(icon).toBeDefined();
  });

  it('should open menu on click', () => {
    const component = mountWithIntl(
      <VcUserProfile user={{ display: 'test' }} />
    );
    expect(component).toBeDefined();
    const userIcon = component.find('IconButton').at(0);
    userIcon.simulate('click');
    const menu = component.find('Menu');
    expect(menu.props().open).toBeTruthy();
    const menuItems = component.find('MenuItem');
    expect(menuItems.length).toEqual(3);
  });

  it('should close menu on menu click', () => {
    const component = mountWithIntl(
      <VcUserProfile user={{ display: 'test' }} />
    );
    expect(component).toBeDefined();
    let menu = component.find('Menu');
    expect(menu.props().anchorEl).toBeNull();
    expect(menu.props().open).toBeFalsy();
    const userIcon = component.find('IconButton').at(0);
    userIcon.simulate('click');
    // clicking on the user icon should open the menu
    menu = component.find('Menu');
    expect(menu.props().anchorEl).not.toBeNull();
    expect(menu.props().open).toBeTruthy();
    const menuItem = component.find('MenuItem').at(0);
    menuItem.simulate('click');
    // clicking on the menu item, should close the menu
    menu = component.find('Menu');
    expect(menu.props().anchorEl).toBeNull();
    expect(menu.props().open).toBeFalsy();
  });

  it('should call logout and close menu', () => {
    const logoutMock = jest.fn();
    const component = mountWithIntl(
      <VcUserProfile user={{ display: 'test' }} logout={logoutMock} />
    );
    expect(component).toBeDefined();
    let menu = component.find('Menu');
    // menu items should be closed initially
    expect(menu.props().open).toBeFalsy();
    const userIcon = component.find('IconButton').at(0);
    userIcon.simulate('click');
    // clicking on the user icon should open the menu
    menu = component.find('Menu');
    expect(menu.props().open).toBeTruthy();
    const logout = component.find('MenuItem').at(2);
    logout.simulate('click');
    // clicking on logout, should call the logout function and close the menu
    expect(logoutMock.mock.calls.length).toEqual(1);
    menu = component.find('Menu');
    expect(menu.props().open).toBeFalsy();
  });

  it('should not call logout if not defined', () => {
    const logoutMock = jest.fn();
    const component = mountWithIntl(
      <VcUserProfile user={{ display: 'test' }} />
    );
    expect(component).toBeDefined();
    let menu = component.find('Menu');
    // menu items should be closed initially
    expect(menu.props().open).toBeFalsy();
    const userIcon = component.find('IconButton').at(0);
    userIcon.simulate('click');
    // clicking on the user icon should open the menu
    menu = component.find('Menu');
    expect(menu.props().open).toBeTruthy();
    const logout = component.find('MenuItem').at(2);
    logout.simulate('click');
    // clicking on logout, just close the menu
    expect(logoutMock.mock.calls.length).toEqual(0);
    menu = component.find('Menu');
    expect(menu.props().open).toBeFalsy();
  });
});
